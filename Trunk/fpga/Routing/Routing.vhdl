-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          Routing.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'Routing' is:
-- to route the samples, coming from the ADC
--  to the FOUR PSI5 sensors.
-- The routing can be changed 
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity Routing is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- ADC Interface
   ADC_CHL_I      : in     std_logic_vector( 2 downto 0 );
   ADC_DAT_I      : in     std_logic_vector( 15 downto 0 );
   ADC_VAL_I      : in     std_logic;
   --
   -- GEN Interface
   GEN_CHL_I      : in     std_logic_vector( 2 downto 0 );
   GEN_DAT_I      : in     std_logic_vector( 15 downto 0 );
   GEN_VAL_I      : in     std_logic;
   --
   -- Selects the sensor input source
   -- "0000"   ADC CHL#0 -> DATA1, ADC CHL#1 -> DATA2
   -- "0001"   ADC CHL#2 -> DATA1, ADC CHL#3 -> DATA2
   -- "0010"   ADC CHL#4 -> DATA1, ADC CHL#5 -> DATA2
   -- "0011"   ADC CHL#6 -> DATA1, ADC CHL#7 -> DATA2
   -- "0100"   GEN CHL#0 -> DATA1, GEN CHL#0 -> DATA2
   -- "0101"   GEN CHL#0 -> DATA1, GEN CHL#0 -> DATA2
   -- "0110"   GEN CHL#0 -> DATA1, GEN CHL#0 -> DATA2
   -- "0111"   GEN CHL#0 -> DATA1, GEN CHL#0 -> DATA2
   -- "1xxx"   CONST_1   -> DATA1, CONST_2   -> DATA2
   SENS_SEL_I     : in     std_logic_vector(  3 downto 0 );
   --
   -- Constant Values
   SENS_CONST1_I  : in     std_logic_vector( 15 downto 0 );
   SENS_CONST2_I  : in     std_logic_vector( 15 downto 0 );
   --
   -- Sensor Data
   SENS_DATA1_O   : out    std_logic_vector( 15 downto 0 );
   SENS_DATA2_O   : out    std_logic_vector( 15 downto 0 );
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end Routing;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of Routing is 
   --
   -- signals
   signal data1         : std_logic_vector( 15 downto 0 )   := X"0000";
   signal data2         : std_logic_vector( 15 downto 0 )   := X"0000";
   --
begin
   --
   -- ------------------------------------------------------
   -- This process does calculates the 
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            data1    <= ( others => '0' );
            data2    <= ( others => '0' );
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            if( SENS_SEL_I( 3 downto 2 ) = "00" )then       --  ADC data used?
               --                                           --   yes, then...
               if( ADC_VAL_I = '1' )then                    --   ADC inputs valid?
                  --                                        --    yes, then...
                  if(   ADC_CHL_I( 2 downto 1 )  =          --    channel
                        SENS_SEL_I( 1 downto 0 )    )then   --     found?
                     --                                     --     yes, then...
                     if( ADC_CHL_I( 0 ) = '0' )then         --      first channel?
                        data1 <= ADC_DAT_I;                 --      yes, then store it as data1
                     else                                   --      otherwise
                        data2 <= ADC_DAT_I;                 --       store it as data2
                     end if;
                     --
                  end if;  --if( ADC_CHL_I()... )
                  --
               end if;  --if( ADC_VAL_I = '1' )then
               --
            elsif( SENS_SEL_I( 3 downto 2 ) = "01" )then    --  GENerator data used?
               --                                           --   yes, then...
               if( GEN_VAL_I = '1' )then                    --   GEN inputs valid?
                  --                                        --    yes, then...
                  data1 <= GEN_DAT_I;                       --    store generator data as data1
                  data2 <= GEN_DAT_I;                       --    store generator data as data2
                  --
               end if;  --if( GEN_VAL_I = '1' )then
               --
            else     --if( SENS_SEL_I( ... )... )then
               --
               data1 <= SENS_CONST1_I;
               data2 <= SENS_CONST2_I;
               --
            end if;  --if( SENS_SEL_I( ... )... )then
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK... ) )
      --
   end process;
   --
   -- concurrent statements
   SENS_DATA1_O   <= data1;
   SENS_DATA2_O   <= data2;
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
