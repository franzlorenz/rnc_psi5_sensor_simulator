onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_routing/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_routing/DUT/RST_I
add wave -noupdate -divider {ADC INTERFACE}
add wave -noupdate -radix hexadecimal /tb_routing/DUT/ADC_CHL_I
add wave -noupdate -radix hexadecimal /tb_routing/DUT/ADC_DAT_I
add wave -noupdate -radix hexadecimal /tb_routing/DUT/ADC_VAL_I
add wave -noupdate -divider SELECTOR
add wave -noupdate -radix hexadecimal /tb_routing/DUT/SENS_SEL_I
add wave -noupdate -radix hexadecimal /tb_routing/DUT/SENS_CONST1_I
add wave -noupdate -radix hexadecimal /tb_routing/DUT/SENS_CONST2_I
add wave -noupdate -divider {SENSOR DATA}
add wave -noupdate -radix hexadecimal /tb_routing/DUT/SENS_DATA1_O
add wave -noupdate -radix hexadecimal /tb_routing/DUT/SENS_DATA2_O
add wave -noupdate -divider {DEBUG SIGNALS}
add wave -noupdate -radix hexadecimal /tb_routing/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_routing/DUT/data1
add wave -noupdate -radix hexadecimal /tb_routing/DUT/data2
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {68752278 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {945052500 ps}
