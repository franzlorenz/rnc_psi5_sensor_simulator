-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_Routing.vhdl
-- @ingroup       Routing
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'Routing'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_Routing is
end tb_Routing;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_Routing is
   --
   -- component declaration for the DUT
   component Routing
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      ADC_CHL_I                     : in     std_logic_vector( 2 downto 0 );
      ADC_DAT_I                     : in     std_logic_vector( 15 downto 0 );
      ADC_VAL_I                     : in     std_logic;
      SENS_SEL_I                    : in     std_logic_vector(  2 downto 0 );
      SENS_CONST1_I                 : in     std_logic_vector( 15 downto 0 );
      SENS_CONST2_I                 : in     std_logic_vector( 15 downto 0 );
      SENS_DATA1_O                  : out    std_logic_vector( 15 downto 0 );
      SENS_DATA2_O                  : out    std_logic_vector( 15 downto 0 );
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal ADC_CHL_I                 : std_logic_vector( 2 downto 0 ) := "000";
   signal ADC_DAT_I                 : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   signal ADC_VAL_I                 : std_logic := '0';
   signal SENS_SEL_I                : std_logic_vector(  2 downto 0 ) := "000";
   signal SENS_CONST1_I             : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   signal SENS_CONST2_I             : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   --
   -- ----------------------
   -- output signals
   --
   signal SENS_DATA1_O              : std_logic_vector( 15 downto 0 );
   signal SENS_DATA2_O              : std_logic_vector( 15 downto 0 );
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   signal testChl                   : std_logic_vector( 2 downto 0 ) := "000"; 
   signal testDat                   : std_logic_vector( 15 downto 0 ) := "0000000000000000";   
   --
begin
   --
   DUT   : Routing
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      ADC_CHL_I                  => ADC_CHL_I,
      ADC_DAT_I                  => ADC_DAT_I,
      ADC_VAL_I                  => ADC_VAL_I,
      SENS_SEL_I                 => SENS_SEL_I,
      SENS_CONST1_I              => SENS_CONST1_I,
      SENS_CONST2_I              => SENS_CONST2_I,
      SENS_DATA1_O               => SENS_DATA1_O,
      SENS_DATA2_O               => SENS_DATA2_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 500 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 1 ns;
      CLK_50MHZ_I <= '1';
      wait for 1 ns;
      --
   end process;

   ADC   : process
   begin
      --
      wait for 14 ns;
      ADC_CHL_I   <= testChl;
      ADC_DAT_I   <= testDat;
      ADC_VAL_I   <= '1';
      wait for 5 ns;
      ADC_VAL_I   <= '0';
      wait for 1 ns;
      testChl     <= testChl + 1;
      testDat     <= testDat + X"1001";
      --
   end process;
   
   
   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 10 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      SENS_CONST1_I  <= X"CC11";
      SENS_CONST2_I  <= X"CC22";     
      --
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;      
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;
      wait for 300 ns;
      SENS_SEL_I  <= SENS_SEL_I + 1;      
      wait for 300 ns;
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
