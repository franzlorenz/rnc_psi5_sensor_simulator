@echo off
REM --------------------------------------------------------
REM This batch file will run the vhdl code checker
REM over the project VHDL source file and generates
REM an output
REM --------------------------------------------------------
REM
%HDLTOOLS_PATH%\chkvhdl\v1.6\chkvhdl -o UnitTestVhdl.log -cpl 100 BusVersionNr.vhd
%HDLTOOLS_PATH%\chkvhdl\v1.6\chkvhdl -a UnitTestVhdl.log -cpl 100 tb_BusVersionNr.vhd
REM
ECHO "UnitTest Vhdl done....";

