onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_busversionnr/MOD_ADDR_I
add wave -noupdate -radix hexadecimal /tb_busversionnr/BUS_ADDR_I
add wave -noupdate -radix hexadecimal /tb_busversionnr/BUS_READ_I
add wave -noupdate -radix hexadecimal /tb_busversionnr/BUS_DATA_O
add wave -noupdate -divider INTERNAL
add wave -noupdate -radix hexadecimal /tb_busversionnr/DUT/c_VERSION
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {126 ns}
