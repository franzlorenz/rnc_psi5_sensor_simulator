-- ----------------------------------------------------------------------------
-- Project        PH3 FPGA
-- (c) copyright  2016
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_BusVersionNr.vhd
-- @ingroup       BusVersionNr
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'BusVersionNr'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_BusVersionNr is
end tb_BusVersionNr;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_BusVersionNr is
   --
   -- component declaration for the DUT
   component BusVersionNr
   generic
   (
      g_VERSIONMAJOR    : std_logic_vector( 7 downto 0 );
      g_VERSIONMINOR    : std_logic_vector( 7 downto 0 );
      g_VERSIONBUGFIX   : std_logic_vector( 7 downto 0 );
      g_VERSIONHARDWARE : std_logic_vector( 7 downto 0 )
   );
   port
   (
      MOD_ADDR_I                : in    std_logic_vector( 3 downto 0 );
      BUS_ADDR_I                : in    std_logic_vector( 6 downto 0 );
      BUS_READ_I                : in    std_logic;
      BUS_DATA_O                : out   std_logic_vector( 7 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal MOD_ADDR_I            : std_logic_vector( 3 downto 0 ) := X"0";
   signal BUS_ADDR_I            : std_logic_vector( 6 downto 0 ) := "0000000";
   signal BUS_READ_I            : std_logic := '0';
   --
   -- ----------------------
   -- output signals
   --
   signal BUS_DATA_O            : std_logic_vector( 7 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   signal tstCounter            : integer;
   --
begin
   --
   DUT   : BusVersionNr
   generic map
   (
      g_VERSIONMAJOR    => X"12",
      g_VERSIONMINOR    => X"34",
      g_VERSIONBUGFIX   => X"BF",
      g_VERSIONHARDWARE => X"C1"
   )
   port map
   (
      MOD_ADDR_I        => MOD_ADDR_I,
      BUS_ADDR_I        => BUS_ADDR_I,
      BUS_READ_I        => BUS_READ_I,
      BUS_DATA_O        => BUS_DATA_O
   );
   --
   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      wait for 10 ns;
      --
      -- TEST 
      --
      BUS_ADDR_I  <= MOD_ADDR_I & "000";                    --select the module
      BUS_READ_I  <= '1';                                   --activate read access
      --
      for tstCounter in 0 to 10 loop                        --read up to 11 bytes
         --                                                 -- then...
         wait for 10 ns;                                    -- wait 
         BUS_ADDR_I  <= BUS_ADDR_I + 1;                     -- increment address (next version byte)
         --
      end loop;
      --
      -- stops the testbench execution
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
