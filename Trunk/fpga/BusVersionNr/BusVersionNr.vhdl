-- ----------------------------------------------------------------------------
-- Project        PH3
-- (c) copyright  2016
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          BusVersionNr.vhdl
-- @ingroup       BusVersionNr
-- @author        Franz Lorenz
--
-- This module is a bus module that support the reading of the 
-- device version. The version consists of 8 bytes.
-- NOTE: The module takes addresses starting from
--       MOD_ADDR_I*8 up to MOD_ADDR_I*8+7 (included address!)
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity BusVersionNr is
generic
(
   -- The major version number
   g_VERSIONMAJOR    : std_logic_vector(  7 downto 0 ) := X"FF";
   -- The minor version number
   g_VERSIONMINOR    : std_logic_vector(  7 downto 0 ) := X"FF";
   -- The bugfix version number
   g_VERSIONBUGFIX   : std_logic_vector(  7 downto 0 ) := X"FF";
   -- The hardware version number
   g_VERSIONHARDWARE : std_logic_vector(  7 downto 0 ) := X"A0"
);
port 
(
   --
   -- CONFIGURATION interface
   MOD_ADDR_I  : in     std_logic_vector( 3 downto 0 );     --module address
         -- NOTE: after that address 
   --
   -- BUS interface
   BUS_ADDR_I  : in     std_logic_vector( 6 downto 0 );     --bus address
   BUS_DATA_O  : out    std_logic_vector( 7 downto 0 );     --bus data-out
   BUS_READ_I  : in     std_logic                           --bus read signal
);
end BusVersionNr;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of BusVersionNr is
   --
   subtype  BYTE is   std_logic_vector( 7 downto 0 );
   type     VROM is array( 7 downto 0 ) of BYTE;
   constant c_VERSION   : VROM := 
            ( 
               0 => CONV_STD_LOGIC_VECTOR( character'pos( 'R' ), 8 ), 
               1 => CONV_STD_LOGIC_VECTOR( character'pos( 'S' ), 8 ), 
               2 => CONV_STD_LOGIC_VECTOR( character'pos( 'S' ), 8 ), 
               3 => CONV_STD_LOGIC_VECTOR( character'pos( 'I' ), 8 ), 
               4 => g_VERSIONMAJOR,
               5 => g_VERSIONMINOR,
               6 => g_VERSIONBUGFIX,
               7 => g_VERSIONHARDWARE
            );
   --
begin
   --
   BUS_DATA_O  <= X"00" when( BUS_ADDR_I( 6 downto 3 ) /= MOD_ADDR_I ) else
                  c_VERSION( CONV_INTEGER( BUS_ADDR_I( 2 downto 0 ) ) );
   --
end behavioral;
