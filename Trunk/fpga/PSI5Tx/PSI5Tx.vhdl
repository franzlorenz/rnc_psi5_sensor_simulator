-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          PSI5Tx.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'PSI5Tx' is:
--  - Generates a PSI5 manchester coded serial stream.
--  - Generates manchester coding error
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity PSI5Tx is
port
(
   CLK_I          : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- Transmit clock divider, divides the CLK_I 
   -- input by this value to generate the DOUBLED
   -- PSI5 clock rate
   TX_CLK_DIV_I   : in     std_logic_vector( 7 downto 0 );
   --
   -- Transmit data
   TX_DATA_I      : in     std_logic_vector( 15 downto 0 );
   --
   -- Number of bits transmitted-1 (1...16)
   -- NOTE: If TX_BITS_I = "0000", then  1 bit  will be transmitted
   --       If TX_BITS_I = "1111", then 16 bits will be transmitted
   TX_BITS_I      : in     std_logic_vector( 3 downto 0 );
   --
   -- CRC3 or PARITY
   -- 00 = CRC3 (only 16 bit)
   -- 10 = Parity even
   -- 11 = Parity odd
   TX_CRCPAR_I    : in     std_logic_vector( 1 downto 0 );
   --
   -- Triggers the transmission
   -- '1' triggers the transmission
   TX_TRIG_I      : in     std_logic;
   --
   -- PSI5 data output (manchester coded)
   PSI5_DATA_O    : out    std_logic;
   --
   -- PSI5 data valid signal
   -- '1' means the TX_DATA_O is valid
   PSI5_VALID_O   : out    std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end PSI5Tx;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of PSI5Tx is 
   --
   -- types
   type tStates is ( IDLE,
                     S0_0, S0_1, 
                     S1_0, S1_1, 
                     Dn_0, Dn_1, 
                     PA_0, PA_1, 
                     C2_0, C2_1, 
                     C1_0, C1_1, 
                     C0_0, C0_1,
                     STOP
                  );   
   --
   -- signals
   signal state         :  tStates                          := IDLE;
   signal clkdiv        :  std_logic_vector( 7 downto 0 )   := X"00";
   signal bitctr        :  std_logic_vector( 3 downto 0 )   := X"0";
   signal shiftreg      :  std_logic_vector( 15 downto 0 )  := X"0000";
   signal crcmode       :  std_logic                        := '0';
   signal crc3c         :  std_logic_vector( 2 downto 0 )   := "111";
   signal parity        :  std_logic                        := '0';
   signal psi5dat       :  std_logic                        := '0';
   signal psi5val       :  std_logic                        := '0';
   --
begin
   -- ------------------------------------------------------
   -- This process does calculates the 
   --  - the CRC3 checksum
   --  - the parity bit
   -- ------------------------------------------------------
   process( CLK_I )
   begin
      --
      if( rising_edge( CLK_I ) )then                        --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            clkdiv   <= ( others => '0' );                  --  init clock divider
            psi5dat  <= '0';                                --  init psi5 output bit
            psi5val  <= '0';                                --  init psi5 valid bit
            state    <= IDLE;                               --  init state machine
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            --
            if( state = IDLE )then                          --  IDLE state?
               --                                           --   yes, then...
               if( TX_TRIG_I = '1' )then                    --   trigger of transmission detected?
                  --                                        --    yes, then...
                  state    <= S0_0;                         --    setup next state
                  clkdiv   <= X"00";                        --    init clockdivider for PSI5 baudrate
                  bitctr   <= TX_BITS_I;                    --    init bitcounter
                  crc3c    <= "111";                        --    init crc3 (c2,c1,c0) bits
                  shiftreg <= TX_DATA_I;                    --    init shift reg
                  crcmode  <= TX_CRCPAR_I(1);               --    store mode
                  parity   <= TX_CRCPAR_I(0);               --    init parity
                  --
               end if;  --if( TX_TRIG_I = '1' )then
               --
            else     --if( state = IDLE )then               -- not in IDLE state - means running!
               --                                           --  then...
               --
               -- NOTE: The PSI5 specification said, that
               --       After all bits shifted in the CRC3
               --       calculation, THREE additional '0'
               --       bits must be shifted in. 
               --       The following code does this...
               if(   ( state = C2_0             ) and       --   before output the
                     (  ( clkdiv = X"03"  ) or              --   first CRC bit
                        ( clkdiv = X"02"  ) or
                        ( clkdiv = X"01"  )     )     )then 
                  --                                        --   then...
                  crc3c(2) <= crc3c(1);                     --   calculate
                  crc3c(1) <= crc3c(0) xor crc3c(2);        --    the CRC3 C2,C1,C0
                  crc3c(0) <= '0' xor crc3c(2);             --    bits with input '0'
                  --
               end if;
               --
               if( clkdiv = X"00" )then                     --  time for next psi5 signal?
                  --                                        --   yes, then...
                  clkdiv   <= TX_CLK_DIV_I-1;               --   reload baudrate divider
                  --
                  case state is                             --   handle the states
                  
                  when S0_0 =>                              --   STATE: BIT S1 SECOND TIMESLICE
                     psi5dat  <= '0';                       --   update PSI5 data output
                     psi5val  <= '1';                       --   activate PSI5 data output
                     state    <= S0_1;                      --   set next state
                     --
                  when S0_1 =>                              --   STATE: BIT S0 SECOND TIMESLICE
                     psi5dat  <= '1';                       --   update PSI5 data output
                     state    <= S1_0;                      --   set next state
                     --
                  when S1_0 =>                              --   STATE: BIT S1 FIRST TIMESLICE
                     psi5dat  <= '0';                       --   update PSI5 data output
                     state    <= S1_1;                      --   set next state
                     --
                  when S1_1 =>                              --   STATE: BIT S1 SECOND TIMESLICE
                     psi5dat  <= '1';                       --   update PSI5 data output
                     state    <= Dn_0;                      --   set next state
                     --
                  when Dn_0 =>                              --   STATE: DATA FIRST TIMESLICE
                     psi5dat  <= shiftreg(0) xor '0';       --   update PSI5 data output
                     parity   <= parity xor shiftreg(0);    --   update parity bit
                     crc3c(2) <= crc3c(1);                  --   calculate
                     crc3c(1) <= crc3c(0) xor crc3c(2);     --    the CRC C2,C1,C0
                     crc3c(0) <= crc3c(2) xor shiftreg(0);  --    bits
                     state    <= Dn_1;                      --   set next state
                     --
                  when Dn_1 =>                              --   STATE: DATA SECOND TIMESLICE
                     psi5dat  <= shiftreg(0) xor '1';       --   update PSI5 data output
                     shiftreg <= '0' & shiftreg( 15 downto 1 ); -- update data shift register for the next data bit
                     bitctr   <= bitctr - 1;                --   decrement bitcounter
                     if( bitctr = "0000" )then              --   last bit?
                        if( crcmode = '1' )then             --    yes, parity output?
                           state <= PA_0;                   --     yes, set next state
                        else                                --    not parity output then CRC3
                           state <= C2_0;                   --     set next state
                        end if;  --if( crcmode... )then
                     else     --if( bitctr... )then         --   not the last bit
                        state <= Dn_0;                      --    then output next data bit
                     end if;  --if( bitctr... )then
                     --
                  when PA_0 =>                              --   STATE: PARITY BIT FIRST TIMESLICE
                     psi5dat  <= parity xor '0';            --   update PSI5 data output
                     state    <= PA_1;                      --   set next state
                     --
                  when PA_1 =>                              --   STATE: PARITY BIT SECOND TIMESLICE
                     psi5dat  <= parity xor '1';            --   update PSI5 data output
                     state    <= STOP;                      --   set next state
                     --
                  when C2_0 => 
                     psi5dat  <= crc3c(2) xor '0';          --   update PSI5 data output
                     state    <= C2_1;                      --   set next state
                     --
                  when C2_1 => 
                     psi5dat  <= crc3c(2) xor '1';          --   update PSI5 data output
                     state    <= C1_0;                      --   set next state
                     --
                  when C1_0 => 
                     psi5dat  <= crc3c(1) xor '0';          --   update PSI5 data output
                     state    <= C1_1;                      --   set next state
                     --
                  when C1_1 => 
                     psi5dat  <= crc3c(1) xor '1';          --   update PSI5 data output
                     state    <= C0_0;                      --   set next state
                     --
                  when C0_0 => 
                     psi5dat  <= crc3c(0) xor '0';          --   update PSI5 data output
                     state    <= C0_1;                      --   set next state
                     --
                  when C0_1 => 
                     psi5dat  <= crc3c(0) xor '1';          --   update PSI5 data output
                     state    <= STOP;                      --   set next state
                     --
                  when STOP =>                              --   STATE: STOP
                     psi5dat  <= '0';                       --   init PSI5 data output
                     psi5val  <= '0';                       --   disable PSI5 data output
                     state    <= IDLE;                      --   back to IDLE state
                     --
                  when others =>                            --   STATE: UNKNOWN!
                     state <= IDLE;                         --   resync to IDLE state
                  end case;
                  --
               else     --if( clkdiv... )then
                  --
                  clkdiv   <= clkdiv - 1;
                  --
               end if;  --if( clkdiv = X"00" )then
               --
            end if;  --if( state = IDLE )then
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_I ) )
      --
   end process;
   --
   -- concurrent statements
   PSI5_DATA_O    <= psi5dat;
   PSI5_VALID_O   <= psi5val;
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
