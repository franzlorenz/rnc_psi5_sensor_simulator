-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_PSI5Tx.vhdl
-- @ingroup       PSI5Tx
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'PSI5Tx'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_PSI5Tx is
end tb_PSI5Tx;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_PSI5Tx is
   --
   -- component declaration for the DUT
   component PSI5Tx
   port
   (
      CLK_I                         : in     std_logic;
      RST_I                         : in     std_logic;
      TX_CLK_DIV_I                  : in     std_logic_vector( 7 downto 0 );
      TX_DATA_I                     : in     std_logic_vector( 15 downto 0 );
      TX_BITS_I                     : in     std_logic_vector( 3 downto 0 );
      TX_CRCPAR_I                   : in     std_logic_vector( 1 downto 0 );
      TX_TRIG_I                     : in     std_logic;
      PSI5_DATA_O                   : out    std_logic;
      PSI5_VALID_O                  : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_I                     : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal TX_CLK_DIV_I              : std_logic_vector( 7 downto 0 ) := "00000000";
   signal TX_DATA_I                 : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   signal TX_BITS_I                 : std_logic_vector( 3 downto 0 ) := "0000";
   signal TX_CRCPAR_I               : std_logic_vector( 1 downto 0 ) := "00";
   signal TX_TRIG_I                 : std_logic := '0';
   --
   -- ----------------------
   -- output signals
   --
   signal PSI5_DATA_O               : std_logic;
   signal PSI5_VALID_O              : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   --
begin
   --
   DUT   : PSI5Tx
   port map
   (
      CLK_I                      => CLK_I,
      RST_I                      => RST_I,
      TX_CLK_DIV_I               => TX_CLK_DIV_I,
      TX_DATA_I                  => TX_DATA_I,
      TX_BITS_I                  => TX_BITS_I,
      TX_CRCPAR_I                => TX_CRCPAR_I,
      TX_TRIG_I                  => TX_TRIG_I,
      PSI5_DATA_O                => PSI5_DATA_O,
      PSI5_VALID_O               => PSI5_VALID_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLKI  : process
   begin
      --
      CLK_I <= '0';
      wait for 10 ns;
      CLK_I <= '1';
      wait for 10 ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      
      -- setup baudrate to 189 kHz
      -- divider = 50 MHz / 189 kHz / 2 = 132 (0x84)
      TX_CLK_DIV_I   <= X"84";                              --setup baudrate
      TX_DATA_I      <= X"01E7";                            --setup data
      TX_BITS_I      <= X"9";                               --set datalength to 10 bits
      TX_CRCPAR_I    <= "10";                               --set parity mode, even
      --
      TX_TRIG_I      <= '1';                                --trigger output
      wait for 50 ns;
      TX_TRIG_I      <= '0';                                --release trigger
      --
      wait for 150 us;
      --
      --
      TX_CLK_DIV_I   <= X"84";                              --setup baudrate
      TX_DATA_I      <= X"AD2C";                            --setup data
      TX_BITS_I      <= X"F";                               --set datalength to 10 bits
      TX_CRCPAR_I    <= "01";                               --set CRC3 mode
      --
      TX_TRIG_I      <= '1';                                --trigger output
      wait for 50 ns;
      TX_TRIG_I      <= '0';                                --release trigger
      --
      wait for 150 us;
      --
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
