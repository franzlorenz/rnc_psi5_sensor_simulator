onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/CLK_I
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/RST_I
add wave -noupdate -divider {TX DATA}
add wave -noupdate -radix unsigned /tb_psi5tx/DUT/TX_CLK_DIV_I
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/TX_DATA_I
add wave -noupdate -radix unsigned /tb_psi5tx/DUT/TX_BITS_I
add wave -noupdate -radix binary /tb_psi5tx/DUT/TX_CRCPAR_I
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/TX_TRIG_I
add wave -noupdate -divider {PSI5 DATA}
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/PSI5_DATA_O
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/PSI5_VALID_O
add wave -noupdate -divider {DEBUG SIGNALS}
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/state
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/clkdiv
add wave -noupdate -radix unsigned /tb_psi5tx/DUT/bitctr
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/shiftreg
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/crcmode
add wave -noupdate -radix binary -childformat {{/tb_psi5tx/DUT/crc3c(2) -radix hexadecimal} {/tb_psi5tx/DUT/crc3c(1) -radix hexadecimal} {/tb_psi5tx/DUT/crc3c(0) -radix hexadecimal}} -expand -subitemconfig {/tb_psi5tx/DUT/crc3c(2) {-radix hexadecimal} /tb_psi5tx/DUT/crc3c(1) {-radix hexadecimal} /tb_psi5tx/DUT/crc3c(0) {-radix hexadecimal}} /tb_psi5tx/DUT/crc3c
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/parity
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/psi5dat
add wave -noupdate -radix hexadecimal /tb_psi5tx/DUT/psi5val
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {252888702 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {219282518 ps} {267223794 ps}
