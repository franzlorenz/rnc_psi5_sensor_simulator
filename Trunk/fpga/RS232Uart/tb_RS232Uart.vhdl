-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_RS232Uart.vhdl
-- @ingroup       RS232Uart
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'RS232Uart'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_RS232Uart is
end tb_RS232Uart;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_RS232Uart is
   --
   -- component declaration for the DUT
   component RS232Uart
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      TX_DATA_I                     : in     std_logic_vector( 7 downto 0 );
      TX_VAL_I                      : in     std_logic;
      UART_RX_I                     : in     std_logic;
      TX_BUSY_O                     : out    std_logic;
      RX_DATA_O                     : out    std_logic_vector( 7 downto 0 );
      RX_VAL_O                      : out    std_logic;
      UART_TX_O                     : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal TX_DATA_I                 : std_logic_vector( 7 downto 0 ) := "00000000";
   signal TX_VAL_I                  : std_logic := '0';
   signal UART_RX_I                 : std_logic := '0';
   --
   -- ----------------------
   -- output signals
   --
   signal TX_BUSY_O                 : std_logic;
   signal RX_DATA_O                 : std_logic_vector( 7 downto 0 );
   signal RX_VAL_O                  : std_logic;
   signal UART_TX_O                 : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   signal tstCtr                    : integer := 0;
   --
begin
   --
   DUT   : RS232Uart
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      TX_DATA_I                  => TX_DATA_I,
      TX_VAL_I                   => TX_VAL_I,
      UART_RX_I                  => UART_TX_O,
      TX_BUSY_O                  => TX_BUSY_O,
      RX_DATA_O                  => RX_DATA_O,
      RX_VAL_O                   => RX_VAL_O,
      UART_TX_O                  => UART_TX_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 10 ns;
      CLK_50MHZ_I <= '1';
      wait for 10 ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      TX_DATA_I   <= X"00";
      --
      for tstCtr in 0 to 16 loop
         TX_VAL_I    <= '1';
         wait for 20 ns;
         TX_VAL_I    <= '0';
         wait for 21 us;
         TX_DATA_I   <= TX_DATA_I + X"12";
      end loop;
      
      
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
      UART_RX_I   <= UART_TX_O;
      --
   end process;
   --
end test;
