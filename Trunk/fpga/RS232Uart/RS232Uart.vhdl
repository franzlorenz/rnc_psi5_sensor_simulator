-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
--! @file         RS232Uart.vhdl
--! @ingroup      20170213_RNC_SensorSimulator
--! @author       Franz Lorenz
--
--! The functionality of the module 'RS232Uart' is:
--! A serial receiver/transmitter with a fixed baurate
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity RS232Uart is
generic
(
   g_BAUDRATE_DIV :  std_logic_vector( 8 downto 0 ) := '0' & X"35"
);
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   --! data to transmit
   TX_DATA_I      : in     std_logic_vector( 7 downto 0 );
   TX_VAL_I       : in     std_logic;
   TX_BUSY_O      : out    std_logic;
   --
   RX_DATA_O      : out    std_logic_vector( 7 downto 0 );
   RX_VAL_O       : out    std_logic;
   --
   UART_TX_O      : out    std_logic;
   UART_RX_I      : in     std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end RS232Uart;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of RS232Uart is 
   --
   -- signals
   signal tx_state         :  std_logic   := '0';
   signal tx_divider       :  std_logic_vector( 8 downto 0 );
   signal tx_data          :  std_logic_vector( 10 downto 0 );
   --
   signal rx_state         :  std_logic   := '0';
   signal rx_divider       :  std_logic_vector( 8 downto 0 );
   signal rx_data          :  std_logic_vector( 9 downto 0 );
   signal rx_val           :  std_logic   := '0';
   signal rx_edge          :  std_logic_vector( 1 downto 0 );
   --
begin
   --
   -- ------------------------------------------------------
   -- This process handles the data transmission
   -- ------------------------------------------------------
   TX : process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            tx_state    <= '0';                             --  set state to "IDLE"
            tx_data     <= ( others => '1' );               --  init tx data register
            tx_divider  <= g_BAUDRATE_DIV;                  --  init divider
            --
         else    --if( RST_I = '1' )then                    -- reset is NOT active
            --
            if( tx_state = '0' )then                        --  TX IDLE mode?
               --                                           --   yes, then...
               if( TX_VAL_I = '1' )then                     --   transmission trigger?
                  --                                        --    yes, then...
                  tx_data     <= "11" &                     --    load stop bit
                                 TX_DATA_I &                --    load data
                                 '0';                       --    load start bit
                  tx_divider  <= g_BAUDRATE_DIV;            --    init divider
                  tx_state    <= '1';                       --    change the state
                  --
               end if;
               --
            else     --if( tx_state = '0' )then             --  TX TRANSMISSION mode
               --                                           --   then...
               if( tx_divider = "0000000000" )then          --   one bit time over?
                  --                                        --    yes, then...
                  tx_divider  <= g_BAUDRATE_DIV;            --    reload divider
                  tx_data     <= '0' & tx_data( 10 downto 1 );
                  --
                  if( tx_data = "00000000011" )then         --    all data shifted out?
                     tx_state <= '0';                       --     yes, then back to IDLE state
                  end if;
                  --
               else     --if( tx_divider = ... )            --   bit time runs...
                  --                                        --    then...
                  tx_divider  <= tx_divider - 1;            --    decrement divider
                  --
               end if;  --if( tx_divider = ... )
               --
            end if;  --if( tx_state = '0' )then
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_50MHZ_I ) )
      --
   end process;
   --
   -- ------------------------------------------------------
   -- This process handles the data receiption
   -- ------------------------------------------------------
   RX : process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            rx_state    <= '0';                             --  set state to "IDLE"
            rx_data     <= ( others => '1' );               --  init tx data register
            rx_divider  <= g_BAUDRATE_DIV;                  --  init divider
            rx_edge     <= UART_RX_I & UART_RX_I;           --  init edge detector
            rx_val      <= '0';                             --  init valid signal
            --
         else    --if( RST_I = '1' )then                    -- reset is NOT active
            --
            rx_val      <= '0';                             --  reset valid bit
            rx_edge     <= rx_edge( 0 ) & UART_RX_I;        --  get the receive line
            --
            if( rx_state = '0' )then                        --  RX IDLE state?
               --                                           --   yes, then...
               if( rx_edge = "10" )then                     --   falling edge of the start bit detected?
                  --                                        --    yes, then...
                  rx_divider  <= '0' & g_BAUDRATE_DIV( 8 downto 1 );--  init divider with 1/2 bit time
                  rx_state    <= '1';                       --    change the state
                  rx_data     <= ( 9 => '1',others => '0' );--    init receive data
                  rx_val      <= '0';
                  --
               end if;
               --
            else     --if( tx_state = '0' )then             --  TX TRANSMISSION mode
               --                                           --   then...
               if( rx_divider = "0000000000" )then          --   one bit time over?
                  --                                        --    yes, then...
                  rx_divider  <= g_BAUDRATE_DIV;            --    reload divider
                  rx_data     <= rx_edge( 0 ) & rx_data( 9 downto 1 );
                  --
                  if( rx_data( 0 ) = '1' )then              --    all data shifted in?
                     rx_val   <= '1';                       --     yes, then set valid flag
                     rx_state <= '0';                       --     go back to IDLE state
                  end if;
                  --
               else     --if( tx_divider = ... )            --   bit time runs...
                  --                                        --    then...
                  rx_divider  <= rx_divider - 1;            --    decrement divider
                  --
               end if;  --if( tx_divider = ... )
               --
            end if;  --if( tx_state = '0' )then
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_50MHZ_I ) )
      --
   end process;
   --
   -- concurrent statements
   --
   UART_TX_O      <= tx_data( 0 );
   TX_BUSY_O      <= tx_state;
   --
   RX_DATA_O      <= X"00"                 when( RST_I  = '1' ) else
                     rx_data( 8 downto 1 ) when( rx_val = '1' );
   RX_VAL_O       <= rx_val;
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
