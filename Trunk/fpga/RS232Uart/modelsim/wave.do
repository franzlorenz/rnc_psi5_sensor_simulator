onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/RST_I
add wave -noupdate -divider {TRANSMIT SIGNALS}
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/TX_DATA_I
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/TX_VAL_I
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/TX_BUSY_O
add wave -noupdate -divider RECEIVER
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/RX_DATA_O
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/RX_VAL_O
add wave -noupdate -divider {RXTX SIGNALS}
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/UART_TX_O
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/UART_RX_I
add wave -noupdate -divider DEBUG
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/DEBUG_O
add wave -noupdate -divider INTERNALS
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/tx_state
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/tx_divider
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/tx_data
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/rx_state
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/rx_divider
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/rx_data
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/rx_val
add wave -noupdate -radix hexadecimal /tb_rs232uart/DUT/rx_edge
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {68198 ps} 0} {{Cursor 2} {10843516 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {22123500 ps}
