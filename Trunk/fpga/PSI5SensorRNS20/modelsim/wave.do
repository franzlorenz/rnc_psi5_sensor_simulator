onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/RST_I
add wave -noupdate -divider {TX DATA}
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/SEN_TRIGGER_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/SEN_DATA_1_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/SEN_DATA_2_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/SEN_BUSY_O
add wave -noupdate -divider {PSI5 DATA}
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI5_DAT_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI5_VAL_O
add wave -noupdate -divider {DEBUG SIGNALS}
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/state
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/timTimeUs
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/timTrigger
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/timBusy
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/timStop
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiClkDiv
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiData
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiBits
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiCrcPar
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiTrigger
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiSDat
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/psiSVal
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/RST_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/TIM_TIMEUS_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/TIM_TRIG_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/TIM_BUSY_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/TIM_STOP_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/DEBUG_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/state
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/expired
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/divider
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/TIM/prediv
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/CLK_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/RST_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/TX_CLK_DIV_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/TX_DATA_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/TX_BITS_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/TX_CRCPAR_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/TX_TRIG_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/PSI5_DATA_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/PSI5_VALID_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/DEBUG_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/state
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/clkdiv
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/bitctr
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/shiftreg
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/crcmode
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/crc3c
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/parity
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/psi5dat
add wave -noupdate -radix hexadecimal /tb_psi5sensorrns20/DUT/PSI/psi5val
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {501050000 ps} 0} {{Cursor 2} {693230000 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {104250125 ps} {1049302625 ps}
