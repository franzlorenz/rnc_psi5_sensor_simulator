-- ----------------------------------------------------------------------------
-- Project        PH3
-- (c) copyright  2016
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          IOReg8bit.vhdl
-- @ingroup       IOReg8bit
-- @author        Franz Lorenz
--
-- This is a generic 8 bit register with read and write 
-- functionality. The generic g_BUSWRITEMASK is a "AND"-mask
-- when the controller writes a value to the data register.
-- With this functionality, you can prevent the setting
-- of special bits.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity IOReg8bit is
generic
(
   g_RESETVALUE   :  std_logic_vector( 7 downto 0 )   := X"00"
);
port 
(
   CLK_I       : in     std_logic;
   RST_I       : in     std_logic;
   --
   -- CONFIGURATION interface
   MOD_ADDR_I  : in     std_logic_vector( 6 downto 0 );     --module address
   --
   -- BUS interface
   BUS_ADDR_I  : in     std_logic_vector( 6 downto 0 );     --bus address
   BUS_DATA_O  : out    std_logic_vector( 7 downto 0 );     --bus data-out
   BUS_DATA_I  : in     std_logic_vector( 7 downto 0 );     --bus data-in
   BUS_WRITE_I : in     std_logic;
   --
   -- OUTPUT interface
   DATA_O      : out    std_logic_vector( 7 downto 0 );
   DATAWR_O    : out    std_logic;
   DATA_I      : in     std_logic_vector( 7 downto 0 )
);
end IOReg8bit;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of IOReg8bit is
   --
   signal data    :  std_logic_vector( 7 downto 0 );
   signal sel     :  std_logic;
   --
begin
   --
   process( CLK_I )
   begin
      --
      if( rising_edge( CLK_I ) )then                        --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- reset active?
            --                                              --  yes, then...
            data     <= g_RESETVALUE;                       --  reset the output register
            DATAWR_O <= '0';                                --  reset data-write signal
            --
         else     --if( RST_I = '1' )then                   -- reset released
            --                                              --  then...
            if( ( BUS_WRITE_I='1' ) and ( sel='1' ) )then   --  bus write to the register?
               --                                           --   yes, then...
               data     <= BUS_DATA_I;                      --   store data
               DATAWR_O <= '1';                             --   set data-write
               --
            else     --if( BUS_WRITE_I..sel... )then
               --
               DATAWR_O <= '0';                             --   set data-write
               --
            end if;  --if( BUS_WRITE_I..sel... )then
            --
         end if;  --if( RST_I = '1' )then
         --
      end if;  --if( rising_edge( CLK_I ) )then
      --
   end process;
   --
   sel         <= '1'   when( BUS_ADDR_I = MOD_ADDR_I ) else '0';
   BUS_DATA_O  <= DATA_I   when( sel = '1' ) else X"00";
   DATA_O      <= data;
   --
end behavioral;
