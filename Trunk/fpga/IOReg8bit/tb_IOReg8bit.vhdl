-- ----------------------------------------------------------------------------
-- Project        PH3
-- (c) copyright  2016
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_IOReg8bit.vhdl
-- @ingroup       IOReg8bit
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'IOReg8bit'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_IOReg8bit is
end tb_IOReg8bit;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_IOReg8bit is
   --
   -- component declaration for the DUT
   component IOReg8bit
   port
   (
      CLK_I                         : in     std_logic;
      RST_I                         : in     std_logic;
      MOD_ADDR_I                    : in     std_logic_vector( 6 downto 0 );
      BUS_ADDR_I                    : in     std_logic_vector( 6 downto 0 );
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      BUS_WRITE_I                   : in     std_logic;
      DATA_I                        : in     std_logic_vector( 7 downto 0 );
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      DATA_O                        : out    std_logic_vector( 7 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_I                     : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal MOD_ADDR_I                : std_logic_vector( 6 downto 0 ) := "0000000";
   signal BUS_ADDR_I                : std_logic_vector( 6 downto 0 ) := "0000000";
   signal BUS_DATA_I                : std_logic_vector( 7 downto 0 ) := "00000000";
   signal BUS_WRITE_I               : std_logic := '0';
   signal DATA_I                    : std_logic_vector( 7 downto 0 ) := "00000000";
   --
   -- ----------------------
   -- output signals
   --
   signal BUS_DATA_O                : std_logic_vector( 7 downto 0 );
   signal DATA_O                    : std_logic_vector( 7 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   --
begin
   --
   DUT   : IOReg8bit
   port map
   (
      CLK_I                      => CLK_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => MOD_ADDR_I,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      DATA_I                     => DATA_I,
      BUS_DATA_O                 => BUS_DATA_O,
      DATA_O                     => DATA_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_I frequency is XXX MHz.
   -- ------------------------------------------------------
   CLKI  : process
   begin
      --
      CLK_I <= '0';
      wait for XXXX ns;
      CLK_I <= '1';
      wait for XXXX ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --

      --
      -- PUT YOUR TESTS HERE
      --

      -- output a testbench result
      if( SIGNAL = '?' )then
         report "Test of XXXXX: PASS"
         severity note;
      else
         report "Test of XXXXX: FAIL"
         severity note;
      end if;
      --
      -- output a testbench result
      assert( xxxxx )
         report "Test of XXXXX: PASS"
         severity note;
      assert( xxxxx )
         report "Test of XXXXX: FAIL"
         severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
