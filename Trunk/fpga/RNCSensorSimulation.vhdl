-- ----------------------------------------------------------------------------
-- Project        RNC SENSOR SIMULATION
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
--!@file          RNCSensorSimulation.vhdl
--!@ingroup       DE0Nano
--!@author        Franz Lorenz
--
--!The functionality of the module 'RNCSensorSimulation' is:
--! - ....
--! - ....
--! - ....
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity RNCSensorSimulation is
generic
(
   g_VERSIONMAJOR    : std_logic_vector( 7 downto 0 ) := X"03";
   g_VERSIONMINOR    : std_logic_vector( 7 downto 0 ) := X"02";
   g_VERSIONBUGFIX   : std_logic_vector( 7 downto 0 ) := X"00";
   g_VERSIONHARDWARE : std_logic_vector( 7 downto 0 ) := X"A1"
);
port
(
   --
   -- ------------------------------------------------------
   -- OSCILLATOR INPUT
   OSC_50MHZ_I       : in  std_logic;                       --pin=R8
   --
   -- ------------------------------------------------------
   -- KEY INPUTS
   KEY_I             : in  std_logic_vector( 1 downto 0 );  --pin=J15,E1
   DIP_I             : in  std_logic_vector( 3 downto 0 );  --pin=M1,T8,B9,M15
   --
   -- ------------------------------------------------------
   -- LED OUTPUTS
   LED_O             : out std_logic_vector( 7 downto 0 );  --pin=A15,A13,B13,A11,D1,F3,B1,L3
   --
   -- ------------------------------------------------------
   -- JP1 - PINS
   JP1_PIN04         : out std_logic;                       --pin=C3 JP1-pin4
   JP1_PIN05         : out std_logic;                       --pin=A2 JP1-pin5
   JP1_PIN06         : out std_logic;                       --pin=A3 JP1-pin6
   -- ------------------------------------------------------
   -- WATCHDOG PORT
   WDG_RESET_NI      : in  std_logic;                       --pin=B11 JP1-pin36
   WDG_TRIG_O        : out std_logic;                       --pin=D11 JP1-pin38
   --
   -- ------------------------------------------------------
   -- ADS8332 PORT
   ADS_RST_NO        : out std_logic;                       --pin=E7  JP1-pin23
   ADS_SDO_I         : in  std_logic;                       --pin=A12 JP1-pin37
   ADS_EOC_I         : in  std_logic;                       --pin=E8  JP1-pin25
   ADS_SCLK_O        : out std_logic;                       --pin=F9  JP1-pin27
   ADS_SDI_O         : out std_logic;                       --pin=C11 JP1-pin35
   ADS_CS_NO         : out std_logic;                       --pin=E11 JP1-pin33
   ADS_CONVST_NO     : out std_logic;                       --pin=C9  JP1-pin31
   --  
   -- ------------------------------------------------------
   -- PSI5 SENSOR 1 PORT
   PSI5_S1_DATA_O    : out std_logic;                       --pin=A5 JP1-pin13
   PSI5_S1_SYNC_NI   : in  std_logic;                       --pin=D5 JP1-pin14
   PSI5_S1_RDY_NI    : in  std_logic;                       --pin=E6 JP1-pin22
   --
   -- ------------------------------------------------------
   -- PSI5 SENSOR 2 PORT
   PSI5_S2_DATA_O    : out std_logic;                       --pin=B7 JP1-pin17
   PSI5_S2_SYNC_NI   : in  std_logic;                       --pin=D6 JP1-pin18
   PSI5_S2_RDY_NI    : in  std_logic;                       --pin=D8 JP1-pin24
   --
   -- ------------------------------------------------------
   -- PSI5 SENSOR 3 PORT
   PSI5_S3_DATA_O    : out std_logic;                       --pin=B6 JP1-pin15
   PSI5_S3_SYNC_NI   : in  std_logic;                       --pin=A6 JP1-pin16
   PSI5_S3_RDY_NI    : in  std_logic;                       --pin=F8 JP1-pin26
   --
   -- ------------------------------------------------------
   -- PSI5 SENSOR 4 PORT
   PSI5_S4_DATA_O    : out std_logic;                       --pin=A7 JP1-pin19
   PSI5_S4_SYNC_NI   : in  std_logic;                       --pin=C6 JP1-pin20
   PSI5_S4_RDY_NI    : in  std_logic;                       --pin=E9 JP1-pin28
   --
   --
   -- ------------------------------------------------------
   -- UART PORT
   RS232_TX_O        : out std_logic;                       --pin=B12 JP1-pin40
   RS232_RX_I        : in  std_logic                        --pin=D12 JP1-pin39
);
end RNCSensorSimulation;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of RNCSensorSimulation is 
   --
   component ADS8332
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      ADS_SDO_I                     : in     std_logic;
      ADS_EOC_I                     : in     std_logic;
      ADS_SCLK_O                    : out    std_logic;
      ADS_SDI_O                     : out    std_logic;
      ADS_CS_NO                     : out    std_logic;
      ADS_CONVST_NO                 : out    std_logic;
      ADC_CHL_O                     : out    std_logic_vector( 2 downto 0 );
      ADC_DATA_O                    : out    std_logic_vector( 15 downto 0 );
      ADC_VALID_O                   : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component DigSignalGenerator
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      GEN_CHL_O                     : out    std_logic_vector( 2 downto 0 );
      GEN_DAT_O                     : out    std_logic_vector( 15 downto 0 );
      GEN_VAL_O                     : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component RS232Controller
   port
   (
      CLK_I                         : in     std_logic;
      RST_I                         : in     std_logic;
      UART_RX_I                     : in     std_logic;
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      UART_TX_O                     : out    std_logic;
      BUS_ADDR_O                    : out    std_logic_vector( 6 downto 0 );
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      BUS_READ_O                    : out    std_logic;
      BUS_WRITE_O                   : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component BusVersionNr
   generic
   (
      g_VERSIONMAJOR                : std_logic_vector( 7 downto 0 );
      g_VERSIONMINOR                : std_logic_vector( 7 downto 0 );
      g_VERSIONBUGFIX               : std_logic_vector( 7 downto 0 );
      g_VERSIONHARDWARE             : std_logic_vector( 7 downto 0 )
   );
   port
   (
      MOD_ADDR_I                    : in    std_logic_vector( 3 downto 0 );
      BUS_ADDR_I                    : in    std_logic_vector( 6 downto 0 );
      BUS_READ_I                    : in    std_logic;
      BUS_DATA_O                    : out   std_logic_vector( 7 downto 0 )
   );
   end component;
   --
   component PSI5SensorBlock
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      MOD_ADDR_I                    : in     std_logic_vector( 3 downto 0 );
      BUS_ADDR_I                    : in     std_logic_vector( 6 downto 0 );
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      BUS_WRITE_I                   : in     std_logic;
      BUS_READ_I                    : in     std_logic;
      ADC_CHL_I                     : in     std_logic_vector( 2 downto 0 );
      ADC_DAT_I                     : in     std_logic_vector( 15 downto 0 );
      ADC_VAL_I                     : in     std_logic;
      GEN_CHL_I                     : in     std_logic_vector( 2 downto 0 );
      GEN_DAT_I                     : in     std_logic_vector( 15 downto 0 );
      GEN_VAL_I                     : in     std_logic;
      PSI5_READY_I                  : in     std_logic;
      PSI5_SYNC_NI                  : in     std_logic;
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      PSI5_DAT_O                    : out    std_logic;
      PSI5_VAL_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;   
   --
   --
   -- signals
   signal sys_clk          :  std_logic;                    --system clock
   signal sys_rst          :  std_logic;                    --system reset (active '1')
   --
   -- signals of the I2C controller interface
   --
   signal i2cscl           :  std_logic;                    --i2c serial clock
   signal i2csdai          :  std_logic;                    --i2c serial data - input  of the FPGA
   signal i2csdao          :  std_logic;                    --i2c serial data - output of the FPGA
   --
   signal busaddr          :  std_logic_vector( 6 downto 0 );--internal bus address
   signal busdatao         :  std_logic_vector( 7 downto 0 );--internal bus data for write access
   signal busdatai         :  std_logic_vector( 7 downto 0 );--internal bus data for read  access
   signal busread          :  std_logic;                    --internal bus read  signal, active '1'
   signal buswrite         :  std_logic;                    --internal bus write signal, active '1'
   --
   -- signal of the version register
   signal reg0x00busdata   :  std_logic_vector( 7 downto 0 );
   --
   -- signal of the bus data
   signal sens1ready       :  std_logic;
   signal sens1busdata     :  std_logic_vector( 7 downto 0 );
   signal sens1debug       :  std_logic_vector( 15 downto 0 );
   signal sens2ready       :  std_logic;
   signal sens2busdata     :  std_logic_vector( 7 downto 0 );
   signal sens2debug       :  std_logic_vector( 15 downto 0 );
   signal sens3ready       :  std_logic;
   signal sens3busdata     :  std_logic_vector( 7 downto 0 );
   signal sens3debug       :  std_logic_vector( 15 downto 0 );
   signal sens4ready       :  std_logic;
   signal sens4busdata     :  std_logic_vector( 7 downto 0 );
   signal sens4debug       :  std_logic_vector( 15 downto 0 );
   --
   -- signals of the ADC interface
   signal adc_chl          :  std_logic_vector( 2 downto 0 );
   signal adc_dat          :  std_logic_vector( 15 downto 0 );
   signal adc_val          :  std_logic;
   signal adc_rst          :  std_logic;
   --
   -- signals of the test signal interface
   signal gen_chl          :  std_logic_vector( 2 downto 0 );
   signal gen_dat          :  std_logic_vector( 15 downto 0 );
   signal gen_val          :  std_logic;
   --
   -- signals of the watchdog
   signal wdg_timer        :  std_logic_vector( 8 downto 0 );
   signal wdg_stop         :  std_logic;
   --
   -- common signals
   signal divider          :  std_logic_vector( 23 downto 0 );
   signal counter          :  std_logic_vector( 15 downto 0 );
   signal triggerdiv       :  std_logic_vector( 15 downto 0 );
   --
begin
   --
   UARTCTRL    : RS232Controller
   port map
   (
      CLK_I                      => sys_clk,
      RST_I                      => sys_rst,
      UART_RX_I                  => RS232_RX_I,
      BUS_DATA_I                 => busdatai,
      UART_TX_O                  => RS232_TX_O,
      BUS_ADDR_O                 => busaddr,
      BUS_DATA_O                 => busdatao,
      BUS_READ_O                 => busread,
      BUS_WRITE_O                => buswrite,
      DEBUG_O                    => open
   );   
   --
   I2C_0X00_0X07  :  BusVersionNr
   generic map
   (
      g_VERSIONMAJOR             => g_VERSIONMAJOR,
      g_VERSIONMINOR             => g_VERSIONMINOR,
      g_VERSIONBUGFIX            => g_VERSIONBUGFIX,
      g_VERSIONHARDWARE          => g_VERSIONHARDWARE
   )
   port map
   (
      MOD_ADDR_I                 => X"0",
      BUS_ADDR_I                 => busaddr,
      BUS_READ_I                 => busread,
      BUS_DATA_O                 => reg0x00busdata
   );
   --
   ADC            : ADS8332
   port map
   (
      CLK_50MHZ_I                => sys_clk,
      RST_I                      => sys_rst,
      ADS_SDO_I                  => ADS_SDO_I,
      ADS_EOC_I                  => ADS_EOC_I,
      ADS_SCLK_O                 => ADS_SCLK_O,
      ADS_SDI_O                  => ADS_SDI_O,
      ADS_CS_NO                  => ADS_CS_NO,
      ADS_CONVST_NO              => ADS_CONVST_NO,
      ADC_CHL_O                  => adc_chl,
      ADC_DATA_O                 => adc_dat,
      ADC_VALID_O                => adc_val,
      DEBUG_O                    => open
   );
   --
   GEN            : DigSignalGenerator
   port map
   (
      CLK_50MHZ_I                => sys_clk,
      RST_I                      => sys_rst,
      GEN_CHL_O                  => gen_chl,
      GEN_DAT_O                  => gen_dat,
      GEN_VAL_O                  => gen_val,
      DEBUG_O                    => open
   );
   --
   SENS1_0X08_0X0F   : PSI5SensorBlock
   port map
   (
      CLK_50MHZ_I                => sys_clk,
      RST_I                      => sys_rst,
      MOD_ADDR_I                 => X"1",
      BUS_ADDR_I                 => busaddr,
      BUS_DATA_I                 => busdatao,
      BUS_WRITE_I                => buswrite,
      BUS_READ_I                 => busread,
      ADC_CHL_I                  => adc_chl,
      ADC_DAT_I                  => adc_dat,
      ADC_VAL_I                  => adc_val,
      GEN_CHL_I                  => gen_chl,
      GEN_DAT_I                  => gen_dat,
      GEN_VAL_I                  => gen_val,
      PSI5_READY_I               => sens1ready,
      PSI5_SYNC_NI               => PSI5_S1_SYNC_NI,
      BUS_DATA_O                 => sens1busdata,
      PSI5_DAT_O                 => PSI5_S1_DATA_O,
      PSI5_VAL_O                 => open,
      DEBUG_O                    => sens1debug
   );
   --
   SENS2_0X10_0X17   : PSI5SensorBlock
   port map
   (
      CLK_50MHZ_I                => sys_clk,
      RST_I                      => sys_rst,
      MOD_ADDR_I                 => X"2",
      BUS_ADDR_I                 => busaddr,
      BUS_DATA_I                 => busdatao,
      BUS_WRITE_I                => buswrite,
      BUS_READ_I                 => busread,
      ADC_CHL_I                  => adc_chl,
      ADC_DAT_I                  => adc_dat,
      ADC_VAL_I                  => adc_val,
      GEN_CHL_I                  => gen_chl,
      GEN_DAT_I                  => gen_dat,
      GEN_VAL_I                  => gen_val,
      PSI5_READY_I               => sens2ready,
      PSI5_SYNC_NI               => PSI5_S2_SYNC_NI,
      BUS_DATA_O                 => sens2busdata,
      PSI5_DAT_O                 => PSI5_S2_DATA_O,
      PSI5_VAL_O                 => open,
      DEBUG_O                    => sens2debug
   );
   --
   SENS3_0X18_0X1F   : PSI5SensorBlock
   port map
   (
      CLK_50MHZ_I                => sys_clk,
      RST_I                      => sys_rst,
      MOD_ADDR_I                 => X"3",
      BUS_ADDR_I                 => busaddr,
      BUS_DATA_I                 => busdatao,
      BUS_WRITE_I                => buswrite,
      BUS_READ_I                 => busread,
      ADC_CHL_I                  => adc_chl,
      ADC_DAT_I                  => adc_dat,
      ADC_VAL_I                  => adc_val,
      GEN_CHL_I                  => gen_chl,
      GEN_DAT_I                  => gen_dat,
      GEN_VAL_I                  => gen_val,
      PSI5_READY_I               => sens3ready,
      PSI5_SYNC_NI               => PSI5_S3_SYNC_NI,
      BUS_DATA_O                 => sens3busdata,
      PSI5_DAT_O                 => PSI5_S3_DATA_O,
      PSI5_VAL_O                 => open,
      DEBUG_O                    => sens3debug
   );
   --
   SENS4_0X20_0X27   : PSI5SensorBlock
   port map
   (
      CLK_50MHZ_I                => sys_clk,
      RST_I                      => sys_rst,
      MOD_ADDR_I                 => X"4",
      BUS_ADDR_I                 => busaddr,
      BUS_DATA_I                 => busdatao,
      BUS_WRITE_I                => buswrite,
      BUS_READ_I                 => busread,
      ADC_CHL_I                  => adc_chl,
      ADC_DAT_I                  => adc_dat,
      ADC_VAL_I                  => adc_val,
      GEN_CHL_I                  => gen_chl,
      GEN_DAT_I                  => gen_dat,
      GEN_VAL_I                  => gen_val,
      PSI5_READY_I               => sens4ready,
      PSI5_SYNC_NI               => PSI5_S4_SYNC_NI,
      BUS_DATA_O                 => sens4busdata,
      PSI5_DAT_O                 => PSI5_S4_DATA_O,
      PSI5_VAL_O                 => open,
      DEBUG_O                    => sens4debug
   );
   -- ------------------------------------------------------
   -- TEST PROCESS
   process( OSC_50MHZ_I )
   begin
      --
      if( rising_edge( OSC_50MHZ_I ) )then
         counter  <= counter + 1;
         adc_rst  <= counter(15);
      end if;
      --
   end process;
   --
   -- ------------------------------------------------------
   -- This process handles the reset signal
   -- ------------------------------------------------------
   RESET : process( OSC_50MHZ_I )
   begin
      --
      if( rising_edge( OSC_50MHZ_I ) )then                  --activate clock edge
         --                                                 -- then...
         if( ( KEY_I( 0 )   = '0' ) or                      -- reset key on eval-board pressed
             ( WDG_RESET_NI = '0' )    )then                --  or reset from the watchdog/reset ic?
            --                                              --  then...
            sys_rst  <= '1';                                --  activate internal reset signal
            --
         else                                               -- no reset source active
            --                                              --  then...
            sys_rst  <= '0';                                --  release internal reset signal
            --
         end if;
         --
      end if;  --if( rising_edge( OSC_50MHZ_I ) )then
      --
   end process;
   --
   -- ------------------------------------------------------
   -- This process handles the watchdog functionality
   -- ------------------------------------------------------
   WDG : process( OSC_50MHZ_I )
   begin
      --
      if( rising_edge( OSC_50MHZ_I ) )then                  --activate clock edge
         --                                                 -- then...
         if( sys_rst = '1' )then                            -- reset active?
            --                                              --  yes, then...
            wdg_stop    <= '0';                             --  clear stop flag
            wdg_timer   <= ( others => '0' );               --  restart timer
            --
         else     --if( sys_rst = '1' )then                 -- reset NOT active
            --                                              --  then...
            --
            if( ( busaddr  = "0000000" ) and                --  WRITE ACCESS to the
                ( buswrite = '1'       )        )then       --   VERSION register 0?
               --                                           --   yes, then...
               wdg_stop <= '1';                             --   set watchdog stop flag
               --
            end if;
            --
            if( wdg_stop = '0' )then                        --  watchdog running?
               --                                           --   yes, then...
               wdg_timer   <= wdg_timer + 1;                --   increment timer
               --
            end if;
            --
         end if;  --if( sys_rst = '1' )then
         --
      end if;  --if( rising_edge( OSC_50MHZ_I ) )then
      --
   end process;
   --
   -- concurrent statements
   --
   sys_clk  <= OSC_50MHZ_I;                                 --setup system clock
   ADS_RST_NO  <= not( sys_rst );                           --reflect system reset signal
   WDG_TRIG_O  <= wdg_timer( wdg_timer'high );              --output watchdog trigger signal
-- was used for debugging  ADS_RST_NO  <= not( adc_rst );
   --
   sens1ready  <= not( PSI5_S1_RDY_NI );                    --ready signals are inverted
   sens2ready  <= not( PSI5_S2_RDY_NI );                    -- by the external hardware
   sens3ready  <= not( PSI5_S3_RDY_NI );                    -- so we have to invert these
   sens4ready  <= not( PSI5_S4_RDY_NI );                    -- signals to get a '1' active signal
   --
   --
   -- route signal for internal address/data bus
   --
   busdatai       <= reg0x00busdata or                      --combine all data outputs of the modules
                     sens1busdata   or
                     sens2busdata   or
                     sens3busdata   or
                     sens4busdata   or
                     X"00";
   --
   -- output data bit
   JP1_PIN04   <= sens1debug(0);
   JP1_PIN05   <= sens1debug(1);
   JP1_PIN06   <= sens1debug(2);
--   JP1_PIN06   <= gen_val;
   --
   LED_O(0)    <= sens1debug(2);                            --output sensor1 ready signal
   LED_O(1)    <= sens1debug(1);                            --output sensor1 data signal
   LED_O(2)    <= sens2debug(2);                            --output sensor2 ready signal
   LED_O(3)    <= sens2debug(1);                            --output sensor2 data signal
   LED_O(4)    <= sens3debug(2);                            --output sensor3 ready signal
   LED_O(5)    <= sens3debug(1);                            --output sensor3 data signal
   LED_O(6)    <= sens4debug(2);                            --output sensor4 ready signal
   LED_O(7)    <= sens4debug(1);                            --output sensor4 data signal
   --
end behavioral;
