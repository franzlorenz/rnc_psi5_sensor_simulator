-- ----------------------------------------------------------------------------
-- Project        <PROJECTNAME>
-- (c) copyright  2015
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          DE0Nano.vhdl
-- @ingroup       DE0Nano
-- @author        Franz Lorenz
--
-- The functionality of the module 'DE0Nano' is:
--  - ....
--  - ....
--  - ....
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity DE0Nano is
generic
(
   g_VERSIONMAJOR    : std_logic_vector( 7 downto 0 ) := X"DB";
   g_VERSIONMINOR    : std_logic_vector( 7 downto 0 ) := X"00";
   g_VERSIONBUGFIX   : std_logic_vector( 7 downto 0 ) := X"02";
   g_VERSIONHARDWARE : std_logic_vector( 7 downto 0 ) := X"C1"
);
port
(
	--
   -- ------------------------------------------------------
   -- OSCILLATOR INPUT
   OSC_50MHZ_I       : in  std_logic;                       --pin=R8
	--
   -- ------------------------------------------------------
   -- KEY INPUTS
   KEY_I             : in  std_logic_vector( 1 downto 0 );  --pin=J15,E1
   DIP_I             : in  std_logic_vector( 3 downto 0 );  --pin=M1,T8,B9,M15
	--
   -- ------------------------------------------------------
   -- LED OUTPUTS
   LED_O             : out std_logic_vector( 7 downto 0 );  --pin=A15,A13,B13,A11,D1,F3,B1,L3
	--
   -- ------------------------------------------------------
   -- JP1 CONNECTOR
   JP1_PIN01         : in  std_logic;                       --pin=A8 JP1-pin1
   JP1_PIN02         : in  std_logic;                       --pin=D3 JP1-pin2
   JP1_PIN03         : in  std_logic;                       --pin=B8 JP1-pin3
   JP1_PIN04         : in  std_logic;                       --pin=C3 JP1-pin4
   JP1_PIN05         : in  std_logic;                       --pin=A2 JP1-pin5
   JP1_PIN06         : in  std_logic;                       --pin=A3 JP1-pin6
   JP1_PIN07         : in  std_logic;                       --pin=B3 JP1-pin7
   JP1_PIN08         : in  std_logic;                       --pin=B4 JP1-pin8
   JP1_PIN09         : in  std_logic;                       --pin=A4 JP1-pin9
   JP1_PIN10         : in  std_logic;                       --pin=B5 JP1-pin10
   JP1_PIN13         : in  std_logic;                       --pin=A5 JP1-pin13
   JP1_PIN14         : in  std_logic;                       --pin=D5 JP1-pin14
   JP1_PIN15         : in  std_logic;                       --pin=B6 JP1-pin15
   JP1_PIN16         : in  std_logic;                       --pin=A6 JP1-pin16
   JP1_PIN17         : in  std_logic;                       --pin=B7 JP1-pin17
   JP1_PIN18         : in  std_logic;                       --pin=D6 JP1-pin18
   JP1_PIN19         : in  std_logic;                       --pin=A7 JP1-pin19
   JP1_PIN20         : in  std_logic;                       --pin=C6 JP1-pin20
   JP1_PIN21         : in  std_logic;                       --pin=C8 JP1-pin21
   JP1_PIN22         : in  std_logic;                       --pin=E6 JP1-pin22
   JP1_PIN23         : in  std_logic;                       --pin=E7 JP1-pin23
   JP1_PIN24         : in  std_logic;                       --pin=D8 JP1-pin24
   JP1_PIN25         : in  std_logic;                       --pin=E8 JP1-pin25
   JP1_PIN26         : in  std_logic;                       --pin=F8 JP1-pin26
   JP1_PIN27         : in  std_logic;                       --pin=F9 JP1-pin27
   JP1_PIN28         : in  std_logic;                       --pin=E9 JP1-pin28
   JP1_PIN31         : in  std_logic;                       --pin=C9 JP1-pin31
   JP1_PIN32         : in  std_logic;                       --pin=D9 JP1-pin32
   JP1_PIN33         : in  std_logic;                       --pin=E11 JP1-pin33
   JP1_PIN34         : in  std_logic;                       --pin=E10 JP1-pin34
   JP1_PIN35         : in  std_logic;                       --pin=C11 JP1-pin35
   JP1_PIN36         : in  std_logic;                       --pin=B11 JP1-pin36
   JP1_PIN37         : in  std_logic;                       --pin=A12 JP1-pin37
   JP1_PIN38         : in  std_logic;                       --pin=D11 JP1-pin38
   JP1_PIN39         : in  std_logic;                       --pin=D12 JP1-pin39
   JP1_PIN40         : in  std_logic;                       --pin=B12 JP1-pin40
	--
   -- ------------------------------------------------------
   -- JP2 CONNECTOR
   JP2_PIN01         : in  std_logic;                       --pin=T9 JP2-pin1
   JP2_PIN02         : in  std_logic;                       --pin=F13 JP2-pin2
   JP2_PIN03         : in  std_logic;                       --pin=R9 JP2-pin3
   JP2_PIN04         : in  std_logic;                       --pin=T15 JP2-pin4
   JP2_PIN05         : in  std_logic;                       --pin=T14 JP2-pin5
   JP2_PIN06         : in  std_logic;                       --pin=T13 JP2-pin6
   JP2_PIN07         : in  std_logic;                       --pin=R13 JP2-pin7
   JP2_PIN08         : in  std_logic;                       --pin=T12 JP2-pin8
   JP2_PIN09         : in  std_logic;                       --pin=R12 JP2-pin9
   JP2_PIN10         : in  std_logic;                       --pin=T11 JP2-pin10
   -- VCC_SYS
   -- GND
   JP2_PIN13         : in  std_logic;                       --pin=T10 JP2-pin13
   JP2_PIN14         : in  std_logic;                       --pin=R11 JP2-pin14
   JP2_PIN15         : in  std_logic;                       --pin=P11 JP2-pin15
   JP2_PIN16         : in  std_logic;                       --pin=R10 JP2-pin16
   JP2_PIN17         : in  std_logic;                       --pin=N12 JP2-pin17
   JP2_PIN18         : in  std_logic;                       --pin=P9 JP2-pin18
   JP2_PIN19         : in  std_logic;                       --pin=N9 JP2-pin19
   JP2_PIN20         : in  std_logic;                       --pin=N11 JP2-pin20
   JP2_PIN21         : in  std_logic;                       --pin=L16 JP2-pin21
   JP2_PIN22         : in  std_logic;                       --pin=K16 JP2-pin22
   JP2_PIN23         : in  std_logic;                       --pin=R16 JP2-pin23
   JP2_PIN24         : in  std_logic;                       --pin=L15 JP2-pin24
   JP2_PIN25         : in  std_logic;                       --pin=P15 JP2-pin25
   JP2_PIN26         : in  std_logic;                       --pin=P16 JP2-pin26
   JP2_PIN27         : in  std_logic;                       --pin=R14 JP2-pin27
   JP2_PIN28         : in  std_logic;                       --pin=N16 JP2-pin28
	-- VCC_3P3
   -- GND
   JP2_PIN31         : in  std_logic;                       --pin=N15 JP2-pin31
   JP2_PIN32         : in  std_logic;                       --pin=P14 JP2-pin32
   JP2_PIN33         : in  std_logic;                       --pin=L14 JP2-pin33
   JP2_PIN34         : in  std_logic;                       --pin=N14 JP2-pin34
   JP2_PIN35         : in  std_logic;                       --pin=M10 JP2-pin35
   JP2_PIN36         : in  std_logic;                       --pin=L13 JP2-pin36
   JP2_PIN37         : in  std_logic;                       --pin=J16 JP2-pin37
   JP2_PIN38         : in  std_logic;                       --pin=K15 JP2-pin38
   JP2_PIN39         : in  std_logic;                       --pin=J13 JP2-pin39
   JP2_PIN40         : in  std_logic;                       --pin=J14 JP2-pin40
	--
   -- ------------------------------------------------------
   -- JP3 CONNECTOR
   -- VCC_3P3
   JP3_PIN02         : in  std_logic;                       --pin=E15 JP3-pin2
   JP3_PIN03         : in  std_logic;                       --pin=E16 JP3-pin3
   JP3_PIN04         : in  std_logic;                       --pin=M16 JP3-pin4
   JP3_PIN05         : in  std_logic;                       --pin=A14 JP3-pin5
   JP3_PIN06         : in  std_logic;                       --pin=B16 JP3-pin6
   JP3_PIN07         : in  std_logic;                       --pin=C14 JP3-pin7
   JP3_PIN08         : in  std_logic;                       --pin=C16 JP3-pin8
   JP3_PIN09         : in  std_logic;                       --pin=C15 JP3-pin9
   JP3_PIN10         : in  std_logic;                       --pin=D16 JP3-pin10
   JP3_PIN11         : in  std_logic;                       --pin=D15 JP3-pin11
   JP3_PIN12         : in  std_logic;                       --pin=D14 JP3-pin12
   JP3_PIN13         : in  std_logic;                       --pin=F15 JP3-pin13
   JP3_PIN14         : in  std_logic;                       --pin=F16 JP3-pin14
   JP3_PIN15         : in  std_logic;                       --pin=F14 JP3-pin15
   JP3_PIN16         : in  std_logic;                       --pin=G16 JP3-pin16
   JP3_PIN17         : in  std_logic;                       --pin=G12 JP3-pin17
	--
   -- ------------------------------------------------------
   -- I2C PORT
   I2C_SCLK_IO       : inout  std_logic;                    --pin=F2 eeprom clock
   I2C_SDAT_IO       : inout  std_logic;                    --pin=F1 eeprom data
	--
   -- ------------------------------------------------------
   -- ADC PORT
   ADC_CS_NO         : out std_logic;                       --pin=A10
   ADC_SADDR_O       : out std_logic;                       --pin=B10
   ADC_SDAT_I        : in  std_logic;                       --pin=A9
   ADC_SCLK_O        : out std_logic;                       --pin=B14
   
);
);
end DE0Nano;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of DE0Nano is 
   --
   --component declarations
   --component ??????
   --generic
   --(
   --);
   --port
   --(
   --);
   --end component;

   --
   -- signals
   signal sys_clk          :  std_logic;                    --system clock
   signal sys_rst          :  std_logic;                    --system reset (active '1')
   --
   signal XXXXXXX        :  std_logic;
   --
begin
   --
   --XXXX : COMPONENTNAME
   --generic map
   --(
   --)
   --port map
   --( 
   --);
   --

   -- ------------------------------------------------------
   -- This process does.......
   -- ------------------------------------------------------
   process( sys_clk )
   begin
      --
      if( rising_edge( sys_clk ) )then                      --active edge?
         --                                                 -- yes, then...
         if( sys_rst = '1' )then                            -- is reset active?
            --                                              --  yes, then...
            --
         else    --if( sys_rst = '1' )then                  -- otherwise
            --                                              --  reset is NOT active
            --
         end if; --if( sys_rst = '1' )then
         --
      end if; --if( rising_edge( sys_clk ) )
      --
   end process;
   --
   -- concurrent statements
   --
   sys_clk  <= OSC_50MHZ_I;                                 --setup system clock
   sys_rst  <= KEY_I( 0 );                                  --setup system reset
   --
   -- XXX <= YYY;
   -- with XXX select
   --    YYYY  <= "001" when "XXX",
   --             "010" when "XXX" | "XXX" | "XXX",
   --             "111" others;
   --
end behavioral;
