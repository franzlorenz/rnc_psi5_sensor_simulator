## ----------------------------------------------------------------------------
#  Project        DE0 NANO EVAL BOARD
#  (c) copyright  2014
#  Company        Harman/Becker Automotive Systems GmbH
#                 All rights reserved
#  Secrecy Level  STRICTLY CONFIDENTIAL
#  ----------------------------------------------------------------------------
#
#  @file          CreatePins.pl
#  @ingroup       CreatePins
#  @author        Franz Lorenz
#
#  The functionality of the module 'CreatePins' is:
#   - Scans a VHDL file for the signals
#   - Creates an altera compatible file with the pin numbers
## ----------------------------------------------------------------------------

#  ---------------------------------------------------------
#  USEs
#  ---------------------------------------------------------
use warnings;
use strict;

#  ---------------------------------------------------------
#  GLOBALs
#  ---------------------------------------------------------
my $gsSrcFile = "test.vhdl";
my $gsQsfFile = "test.qsf";
my $QSF;

#  ---------------------------------------------------------
#  SUBs
#  ---------------------------------------------------------

sub writePin
{
   my $sSignal = $_[0];
   my $nNumber = $_[1];
   my $sPin    = $_[2];
   #
   #set_location_assignment PIN_A15 -to LED_O[0]
   #set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to LED_O[0]   
   #set_location_assignment PIN_R8 -to OSC_50MHZ_I
   #set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to OSC_50MHZ_I
   if( $nNumber >= 0 )
   {
      $sSignal .= "\[".$nNumber."\]";
   }
   print $QSF "set_location_assignment PIN_".$sPin." -to ".$sSignal."\n";
   print $QSF "set_instance_assignment -name IO_STANDARD \"3.3-V LVTTL\" -to ".$sSignal."\n";
}

sub writePins
{
   my $sSignal = $_[0];
   my $sType   = $_[1];
   my $sPins   = $_[2];
   my @aPins   = split( ",", $sPins );
   #
   if( $sType =~ /std_logic_vector\(\s+([0-9]*)\s+downto\s+([0-9]*)\s+\)/ )
   {
      my $nCurr = $1;
      my $nTill = $2;
      while( $nCurr >= $nTill )
      {
         writePin( $sSignal, $nCurr, $aPins[$nCurr] );
         $nCurr--;
      }
   }
   elsif( $sType =~ /std_logic/ )
   {
      writePin( $sSignal, -1, $aPins[0] );
   }
}

#  ---------------------------------------------------------
#  MAIN
#  ---------------------------------------------------------
my $nEntity = 0;
my $sEntity = "";
#
if( exists( $ARGV[0] ) )                                    #first parameter given?
{                                                           # yes, then...
   $gsSrcFile = $ARGV[0];                                   # store the parameter as source file
}
#
print "info : source file '$gsSrcFile'\n";
print "info : output file '$gsQsfFile'\n";
#
open( my $SRC, "<", $gsSrcFile );                           
open(    $QSF, ">", $gsQsfFile );
while( <$SRC> )                                             #scan all the lines
{                                                           # then...
   if( /entity\s+([^\s]*)\s+is/ )                           # entity start found?
   {                                                        #  yes, then...
      $sEntity = $1;                                        #  store entity name
      $nEntity = 1;
      print "info : entity '$sEntity' found.\n";            #  output info for user
   }
   if( /end\s+([^;]*);/ )                                   # end-of-entity found?
   {                                                        #  yes, then...
      print "info : end of entity '$1' found.\n";
      last;
   }
   if( $nEntity )
   {
      if( /\s+([^\s]*)\s+:\s+([^\s]*)\s+([^-]*)--pin=([^\s+|^\n]*)/ )
      {
         print "info : signal='$1' pins='$4' found\n";
         writePins( $1, $3, $4 );
      }
   }
}
close( $SRC );
close( $QSF );
#
print "info : script ends.";

