-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_PSI5Sensor16G.vhdl
-- @ingroup       PSI5Sensor16G
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'PSI5Sensor16G'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_PSI5Sensor16G is
end tb_PSI5Sensor16G;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_PSI5Sensor16G is
   --
   -- component declaration for the DUT
   component PSI5Sensor16G
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      SEN_TRIGGER_I                 : in     std_logic;
      SEN_DATA_1_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_DATA_2_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_BUSY_O                    : out    std_logic;
      PSI5_DAT_O                    : out    std_logic;
      PSI5_VAL_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal SEN_TRIGGER_I             : std_logic := '0';
   signal SEN_DATA_1_I              : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   signal SEN_DATA_2_I              : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   --
   -- ----------------------
   -- output signals
   --
   signal SEN_BUSY_O                : std_logic;
   signal PSI5_DAT_O                : std_logic;
   signal PSI5_VAL_O                : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   --
begin
   --
   DUT   : PSI5Sensor16G
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      SEN_TRIGGER_I              => SEN_TRIGGER_I,
      SEN_DATA_1_I               => SEN_DATA_1_I,
      SEN_DATA_2_I               => SEN_DATA_2_I,
      SEN_BUSY_O                 => SEN_BUSY_O,
      PSI5_DAT_O                 => PSI5_DAT_O,
      PSI5_VAL_O                 => PSI5_VAL_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 10 ns;
      CLK_50MHZ_I <= '1';
      wait for 10 ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      SEN_DATA_1_I   <= X"79C0";                            --set slot1 data
      SEN_DATA_2_I   <= X"AFFE";                            --set slot2 data
      --
      SEN_TRIGGER_I  <= '1';
      --
      wait for 900 us;
      --

      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
