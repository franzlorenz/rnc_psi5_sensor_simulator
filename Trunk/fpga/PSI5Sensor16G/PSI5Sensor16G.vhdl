-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          PSI5Sensor16G.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'PSI5Sensor16G' is:
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity PSI5Sensor16G is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- Sensor trigger input
   -- If a synchronous
   SEN_TRIGGER_I  : in     std_logic;
   --
   -- Sensor Data for slot #1
   SEN_DATA_1_I   : in     std_logic_vector( 15 downto 0 );
   --
   -- Sensor Data for slot #2
   SEN_DATA_2_I   : in     std_logic_vector( 15 downto 0 );
   --
   -- Sensor Busy signal, when '1' otherwise idle '0'
   SEN_BUSY_O     : out    std_logic;
   --
   -- PSI5 stream datat signal. 
   -- Only valid, if PSI5_VAL_O = '1'
   PSI5_DAT_O     : out    std_logic;
   --
   -- PSI5 stream valid signal, when '1' PSI5_DAT_O is valid
   PSI5_VAL_O     : out    std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end PSI5Sensor16G;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of PSI5Sensor16G is 
   --
   -- components usTimer to setup the data transmission rate
   component usTimer
   port
   (
      CLK_50MHZ_I             : in     std_logic;
      RST_I                   : in     std_logic;
      TIM_TIMEUS_I            : in     std_logic_vector( 7 downto 0 );
      TIM_TRIG_I              : in     std_logic;
      TIM_BUSY_O              : out    std_logic;
      TIM_STOP_O              : out    std_logic;
      DEBUG_O                 : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- component for the PSI5 transmission
   component PSI5Tx
   port
   (
      CLK_I                   : in     std_logic;
      RST_I                   : in     std_logic;
      TX_CLK_DIV_I            : in     std_logic_vector( 7 downto 0 );
      TX_DATA_I               : in     std_logic_vector( 15 downto 0 );
      TX_BITS_I               : in     std_logic_vector( 3 downto 0 );
      TX_CRCPAR_I             : in     std_logic_vector( 1 downto 0 );
      TX_TRIG_I               : in     std_logic;
      PSI5_DATA_O             : out    std_logic;
      PSI5_VALID_O            : out    std_logic;
      DEBUG_O                 : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- type
   type tStates is ( IDLE,
                     SEND_D1,
                     TRIG_D1,
                     WAIT_D1,
                     SEND_D2,
                     TRIG_D2,
                     WAIT_D2
                  );   
   --
   -- signals
   signal state               :  tStates                    := IDLE;   
   --
   signal timTimeUs           :  std_logic_vector( 7 downto 0 );
   signal timTrigger          :  std_logic;
   signal timStop             :  std_logic;
   --
   signal psiClkDiv           :  std_logic_vector( 7 downto 0 );
   signal psiData             :  std_logic_vector( 15 downto 0 );
   signal psiBits             :  std_logic_vector( 3 downto 0 );
   signal psiCrcPar           :  std_logic_vector( 1 downto 0 );
   signal psiTrigger          :  std_logic;
   signal psiSDat             :  std_logic;
   signal psiSVal             :  std_logic;
   --
begin
   --
   TIM   : usTimer
   port map
   (
      CLK_50MHZ_I             => CLK_50MHZ_I,
      RST_I                   => RST_I,
      TIM_TIMEUS_I            => X"77",                     -- frame to frame time = 120us = (0x77+1)
      TIM_TRIG_I              => timTrigger,
      TIM_BUSY_O              => open,
      TIM_STOP_O              => timStop,
      DEBUG_O                 => open
   );   
   --
   PSI   : PSI5Tx
   port map
   (
      CLK_I                   => CLK_50MHZ_I,
      RST_I                   => RST_I,
      TX_CLK_DIV_I            => X"84",
      TX_DATA_I               => psiData,
      TX_BITS_I               => psiBits,
      TX_CRCPAR_I             => psiCrcPar,
      TX_TRIG_I               => psiTrigger,
      PSI5_DATA_O             => psiSDat,
      PSI5_VALID_O            => psiSVal,
      DEBUG_O                 => open
   );   --

   -- ------------------------------------------------------
   -- This process does calculates the 
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            state    <= IDLE;                               --  set idle state
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            --
            case state is                                   --  handle state machine...
            when IDLE =>                                    --  STATE IDLE
               if( SEN_TRIGGER_I = '1' )then                --   triggered?
                  --                                        --   yes, then...
                  state       <= SEND_D1;
                  --
               end if;
               --
            when SEND_D1 =>
               timTrigger     <= '1';
               psiData        <= SEN_DATA_1_I( 15 downto 1 ) & '0';
               psiBits        <= x"F";                      --   send 16bits
               psiCrcPar      <= "10";                      --   set parity mode, even
               psiTrigger     <= '1';                       --   trigger the psi5 sender
               state          <= TRIG_D1;                   --   set next state
               --
            when TRIG_D1 =>
               timTrigger  <= '0';                          --   release timer trigger
               psiTrigger  <= '0';                          --   release psi5 trigger
               state       <= WAIT_D1;                      --   set next state
               --
            when WAIT_D1 =>
               if( timStop = '1' )then                      --   timer stopped?
                  state <= SEND_D2;                         --    yes, then send second data slot
               end if;
               --
            when SEND_D2 =>
               timTrigger     <= '1';
               psiData        <= SEN_DATA_2_I( 15 downto 1 ) & '1';
               psiBits        <= x"F";                      --   send 16bits
               psiCrcPar      <= "10";                      --   set parity mode, even
               psiTrigger     <= '1';                       --   trigger the psi5 sender
               state          <= TRIG_D2;                   --   set next state
               --
            when TRIG_D2 =>
               timTrigger  <= '0';                          --   release timer trigger
               psiTrigger  <= '0';                          --   release psi5 trigger
               state       <= WAIT_D2;                      --   set next state
               --
            when WAIT_D2 =>
               timTrigger  <= '0';                          --   release timer trigger
               psiTrigger  <= '0';                          --   release psi5 trigger
               if( timStop = '1' )then                      --   timer stopped?
                  state <= IDLE;                            --    yes, then go back to idle
               end if;
               --
            when others  =>                                 
               state <= IDLE;
               --
            end case;
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK... ) )
      --
   end process;
   --
   -- concurrent statements
   PSI5_DAT_O     <= psiSDat;
   PSI5_VAL_O     <= psiSVal;
   SEN_BUSY_O     <= '1' when( state /= IDLE ) else '0';
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
