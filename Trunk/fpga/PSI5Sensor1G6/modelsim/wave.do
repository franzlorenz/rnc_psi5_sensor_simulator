onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/RST_I
add wave -noupdate -divider {TX DATA}
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/SEN_TRIGGER_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/SEN_DATA_1_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/SEN_DATA_2_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/SEN_BUSY_O
add wave -noupdate -divider {PSI5 DATA}
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI5_DAT_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI5_VAL_O
add wave -noupdate -divider {DEBUG SIGNALS}
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/state
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/timTimeUs
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/timTrigger
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/timBusy
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/timStop
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiClkDiv
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiData
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiBits
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiCrcPar
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiTrigger
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiSDat
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/psiSVal
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/RST_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/TIM_TIMEUS_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/TIM_TRIG_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/TIM_BUSY_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/TIM_STOP_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/DEBUG_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/state
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/expired
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/divider
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/TIM/prediv
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/CLK_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/RST_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/TX_CLK_DIV_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/TX_DATA_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/TX_BITS_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/TX_CRCPAR_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/TX_TRIG_I
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/PSI5_DATA_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/PSI5_VALID_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/DEBUG_O
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/state
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/clkdiv
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/bitctr
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/shiftreg
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/crcmode
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/crc3c
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/parity
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/psi5dat
add wave -noupdate -radix hexadecimal /tb_psi5sensor1g6/DUT/PSI/psi5val
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {68752278 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {945052500 ps}
