onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/RST_I
add wave -noupdate -divider {INPUT VALUES}
add wave -noupdate -radix unsigned /tb_ustimer/DUT/TIM_TIMEUS_I
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/TIM_TRIG_I
add wave -noupdate -divider {OUTPUT SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/TIM_BUSY_O
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/TIM_STOP_O
add wave -noupdate -divider DEBUG
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/state
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/expired
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/divider
add wave -noupdate -radix hexadecimal /tb_ustimer/DUT/prediv
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10110000 ps} 0} {{Cursor 2} {266110000 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {325657500 ps}
