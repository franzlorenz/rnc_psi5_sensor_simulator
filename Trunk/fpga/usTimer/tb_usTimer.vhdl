-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_usTimer.vhdl
-- @ingroup       usTimer
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'usTimer'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_usTimer is
end tb_usTimer;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_usTimer is
   --
   -- component declaration for the DUT
   component usTimer
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      TIM_TIMEUS_I                  : in     std_logic_vector( 7 downto 0 );
      TIM_TRIG_I                    : in     std_logic;
      TIM_BUSY_O                    : out    std_logic;
      TIM_STOP_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal TIM_TIMEUS_I              : std_logic_vector( 7 downto 0 ) := "00000000";
   signal TIM_TRIG_I                : std_logic := '0';
   --
   -- ----------------------
   -- output signals
   --
   signal TIM_BUSY_O                : std_logic;
   signal TIM_STOP_O                : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   --
begin
   --
   DUT   : usTimer
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      TIM_TIMEUS_I               => TIM_TIMEUS_I,
      TIM_TRIG_I                 => TIM_TRIG_I,
      TIM_BUSY_O                 => TIM_BUSY_O,
      TIM_STOP_O                 => TIM_STOP_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 10 ns;
      CLK_50MHZ_I <= '1';
      wait for 10 ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      TIM_TIMEUS_I   <= X"00";                              --set minimum time
      TIM_TRIG_I     <= '1';                                --set trigger
      wait for 50 ns;
      TIM_TRIG_I     <= '0';                                --release trigger
      --
      wait for 10 us;
      --
      -- ---------------------------------------------------
      TIM_TIMEUS_I   <= X"FF";                              --set maximum time
      TIM_TRIG_I     <= '1';                                --set trigger
      wait for 50 ns;
      TIM_TRIG_I     <= '0';                                --release trigger
      --
      wait for 300 us;
      --
      
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
