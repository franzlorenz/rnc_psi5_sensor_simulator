-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          usTimer.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'usTimer' is:
-- Generates a pulse with 
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity usTimer is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- The timer value in microseconds (1..256us)
   TIM_TIMEUS_I   : in     std_logic_vector( 7 downto 0 );
   --
   -- Triggers the timer
   -- '1' triggers the timer
   TIM_TRIG_I     : in     std_logic;
   --
   -- Timer busy signal
   -- '1' means timer is triggered and busy
   TIM_BUSY_O     : out    std_logic;
   --
   -- Timer expired
   -- '1' means timer is expired and has stopped
   TIM_STOP_O     : out    std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end usTimer;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of usTimer is 
   --
   -- signals
   signal state         :  std_logic                        := '0';
   signal expired       :  std_logic                        := '0';
   signal divider       :  std_logic_vector( 7 downto 0 )   := X"00";
   signal prediv        :  std_logic_vector( 5 downto 0 )   := "000000";
   --
begin
   -- ------------------------------------------------------
   -- This process...
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            state    <= '0';                                --  state is IDLE
            expired  <= '0';                                --  reset expired flag
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            if( state = '0' )then                           --  IDLE state
               --                                           --   then...
               if( TIM_TRIG_I = '1' )then                   --   trigger active?
                  --                                        --    yes, then...
                  divider  <= TIM_TIMEUS_I;                 --    setup timer
                  prediv   <= ( others => '0' );            --    init predivider
                  expired  <= '0';                          --    reset exired flag
                  state    <= '1';                          --    set state to "RUN"
                  --
               end if;  --if( TIM_TRIG_I... )then
               --
            else     --if( state... )                       --  state "RUN"
               --                                           --   then...
               if( prediv = "110001" )then                  --   pre-divider for 1us expired?
                  --                                        --    yes, then...
                  prediv   <= ( others => '0' );            --    reset predivider
                  --
                  if( divider = X"00" )then                 --    us-timer expired?
                     state    <= '0';                       --     yes, then set state to IDLE
                     expired  <= '1';                       --     set expired flag
                  else                                      --    us-timer NOT expired
                     divider <= divider - 1;                --     then decrement timer
                  end if;
                  --
               else     --if( prediv... )                   --   pre-divider for 1us not expired
                  --                                        --    then...
                  prediv   <= prediv + 1;                   --    increment pre-divider
                  --
               end if;  --if( prediv... )
               --
            end if;  --if( state... )
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_50MHZ_I ) )
      --
   end process;
   --
   -- concurrent statements
   TIM_BUSY_O  <= state;                                    --reflect state
   TIM_STOP_O  <= expired;                                  --reflect expired flag
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
