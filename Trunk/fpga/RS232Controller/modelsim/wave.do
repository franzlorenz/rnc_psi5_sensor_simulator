onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/CLK_I
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/RST_I
add wave -noupdate -divider {RXTX SIGNALS}
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/UART_TX_O
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/UART_RX_I
add wave -noupdate -divider {BUS SIGNALS}
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/BUS_ADDR_O
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/BUS_DATA_O
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/BUS_DATA_I
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/BUS_READ_O
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/BUS_WRITE_O
add wave -noupdate -divider DEBUG
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/DEBUG_O
add wave -noupdate -divider INTERNALS
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/uarttxdat
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/uarttxval
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/state
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/busaddr
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/busread
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/buswrite
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/busdatao
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/busdatai
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/uarttxbusy
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/uartrxdat
add wave -noupdate -radix hexadecimal /tb_rs232controller/DUT/uartrxval
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {68198 ps} 0} {{Cursor 2} {10843516 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {527136252 ps} {600005086 ps}
