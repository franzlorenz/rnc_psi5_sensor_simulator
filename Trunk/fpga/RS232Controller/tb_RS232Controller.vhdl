-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_RS232Controller.vhdl
-- @ingroup       RS232Controller
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'RS232Controller'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_RS232Controller is
end tb_RS232Controller;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_RS232Controller is
   --
   -- component declaration for the DUT
   component RS232Controller
   port
   (
      CLK_I                         : in     std_logic;
      RST_I                         : in     std_logic;
      UART_RX_I                     : in     std_logic;
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      UART_TX_O                     : out    std_logic;
      BUS_ADDR_O                    : out    std_logic_vector( 6 downto 0 );
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      BUS_READ_O                    : out    std_logic;
      BUS_WRITE_O                   : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component RS232Uart
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      TX_DATA_I                     : in     std_logic_vector( 7 downto 0 );
      TX_VAL_I                      : in     std_logic;
      UART_RX_I                     : in     std_logic;
      TX_BUSY_O                     : out    std_logic;
      RX_DATA_O                     : out    std_logic_vector( 7 downto 0 );
      RX_VAL_O                      : out    std_logic;
      UART_TX_O                     : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_I                     : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal UART_RX_I                 : std_logic := '0';
   signal BUS_DATA_I                : std_logic_vector( 7 downto 0 ) := "00000000";
   --
   -- ----------------------
   -- output signals
   --
   signal UART_TX_O                 : std_logic;
   signal BUS_ADDR_O                : std_logic_vector( 6 downto 0 );
   signal BUS_DATA_O                : std_logic_vector( 7 downto 0 );
   signal BUS_READ_O                : std_logic;
   signal BUS_WRITE_O               : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   signal iCtr                      : integer := 0;
   signal tstdata                   : std_logic_vector( 7 downto 0 ) := X"00";
   --
   signal tsttxdata                 : std_logic_vector( 7 downto 0 ) := X"00";
   signal tsttxval                  : std_logic := '0';
   signal tstrx                     : std_logic;
   signal tsttxbusy                 : std_logic;
   signal tstrxdata                 : std_logic_vector( 7 downto 0 );
   signal tstrxval                  : std_logic;
   signal tsttx                     : std_logic;
   --
begin
   --
   DUT   : RS232Controller
   port map
   (
      CLK_I                      => CLK_I,
      RST_I                      => RST_I,
      UART_RX_I                  => tsttx,
      BUS_DATA_I                 => BUS_DATA_I,
      UART_TX_O                  => tstrx,
      BUS_ADDR_O                 => BUS_ADDR_O,
      BUS_DATA_O                 => BUS_DATA_O,
      BUS_READ_O                 => BUS_READ_O,
      BUS_WRITE_O                => BUS_WRITE_O,
      DEBUG_O                    => open
   );
   --
   -- This is a UART to test the DUT
   TESTUART    : RS232Uart
   port map
   (
      CLK_50MHZ_I                => CLK_I,
      RST_I                      => RST_I,
      TX_DATA_I                  => tsttxdata,
      TX_VAL_I                   => tsttxval,
      UART_RX_I                  => tstrx,
      TX_BUSY_O                  => tsttxbusy,
      RX_DATA_O                  => tstrxdata,
      RX_VAL_O                   => tstrxval,
      UART_TX_O                  => tsttx,
      DEBUG_O                    => open
   );
   --
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLKI  : process
   begin
      --
      CLK_I <= '0';
      wait for 10 ns;
      CLK_I <= '1';
      wait for 10 ns;
      --
   end process;
   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- TEST COMMAND 'A' SET ADDRESS
      --
      for iCtr in 0 to 16 loop                              --test command 'A' in the loop
         --                                                 -- then...
         tsttxdata   <= X"41";                              -- set command 'A'
         tsttxval    <= '1';                                -- trigger transmission
         wait for 50 ns;                                    -- wait till transmission has started
         tsttxval    <= '0';                                -- release transmission trigger
         wait until tsttxbusy = '0';                        -- wait till transmission stopped
         --
         tsttxdata   <= tstdata;                            -- set address
         tsttxval    <= '1';                                -- trigger transmission
         wait for 50 ns;                                    -- wait till transmission has started
         tsttxval    <= '0';                                -- release transmission trigger
         wait until tsttxbusy = '0';                        -- wait till transmission stopped
         --
         wait for 10 ns;
         if( BUS_ADDR_O = tstdata( 6 downto 0 ) )then
            report "Test of command 'A' address check : PASS"
            severity note;
         else
            report "Test of command 'A' address check : FAIL"
            severity note;
         end if;
         --
         wait for 10 ns;
         --
         tstdata  <= tstdata + X"13";                       -- change the address for the next test
         --
      end loop;   --for iCtr...

      --
      -- TEST COMMAND 'R' READ FROM ADDRESS
      --
      tstdata     <= X"07";
      --
      tsttxdata   <= X"41";                                 --set command 'A'
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      tsttxdata   <= X"00";                                 --set address
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      tsttxdata   <= X"52";                                 --set command 'R'
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      tsttxdata   <= tstdata;                               --set length
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      wait for 100 us;

      --
      -- TEST COMMAND 'W' WRITE TO ADDRESS
      --
      tstdata     <= X"07";
      --
      tsttxdata   <= X"41";                                 --set command 'A'
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      tsttxdata   <= X"00";                                 --set address
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      tsttxdata   <= X"57";                                 --set command 'W'
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      tsttxdata   <= X"04";                                 --set blocklen
      tsttxval    <= '1';                                   --trigger transmission
      wait for 50 ns;                                       --wait till transmission has started
      tsttxval    <= '0';                                   --release transmission trigger
      wait until tsttxbusy = '0';                           --wait till transmission stopped
      --
      for iCtr in 0 to 3 loop                               --transmit data byte
         --                                                 -- then...
         tsttxdata   <= tstdata;                            -- set address
         tsttxval    <= '1';                                -- trigger transmission
         wait for 50 ns;                                    -- wait till transmission has started
         tsttxval    <= '0';                                -- release transmission trigger
         wait until tsttxbusy = '0';                        -- wait till transmission stopped
         --
         wait for 10 ns;
         if( BUS_DATA_O = tstdata )then
            report "Test of command 'W' data check : PASS"
            severity note;
         else
            report "Test of command 'W' data check : FAIL"
            severity note;
         end if;
         --
         wait for 10 ns;
         --
         tstdata  <= tstdata + X"13";                       -- change the address for the next test
         --
      end loop;   --for iCtr...
      --
      wait for 1 us;
      

      
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
   BUS_DATA_I  <= '1' & BUS_ADDR_O;                         --loop for check reading
   --
end test;
