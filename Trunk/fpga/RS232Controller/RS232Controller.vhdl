-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          RS232Controller.vhdl
-- @ingroup       RS232Controller
-- @author        Franz Lorenz
--
-- The functionality of the module 'RS232Controller' is:
--  - Handle the RS232 interface to a PC
--  - Handle a parallel 8bit bus
--  - Command 'setup the address register'
--    This command sets the address register 'a' to the 
--    received byte after the character 'A'.
--    RX: 'A' <ADDR> </x0D>
--  - Command 'read'
--    This command sends the data
--    RX: 'R' <CHAR> <CHAR>          <\x0D> reads from the address register
--    TX:     <DATA[a]> <DATA[a+1]>  <\x0D>
-- - Command 'write'
--    RX: 'W' <LEN> <DATA> <DATA>
--    TX: 
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity RS232Controller is
generic
(
   g_BAUDRATE_DIV :  std_logic_vector( 8 downto 0 ) := '0' & X"35"
);
port
(
   CLK_I       : in     std_logic;
   RST_I       : in     std_logic;
   --
   -- UART interface
   UART_TX_O      : out    std_logic;
   UART_RX_I      : in     std_logic;
   --
   -- BUS interface
   BUS_ADDR_O  : out    std_logic_vector( 6 downto 0 );
   BUS_DATA_O  : out    std_logic_vector( 7 downto 0 );
   BUS_DATA_I  : in     std_logic_vector( 7 downto 0 );
   BUS_READ_O  : out    std_logic;
   BUS_WRITE_O : out    std_logic;
   --
   DEBUG_O     : out    std_logic_vector( 15 downto 0 )
);
end RS232Controller;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of RS232Controller is
   --
   component RS232Uart
   port
   (
      CLK_50MHZ_I       : in     std_logic;
      RST_I             : in     std_logic;
      TX_DATA_I         : in     std_logic_vector( 7 downto 0 );
      TX_VAL_I          : in     std_logic;
      UART_RX_I         : in     std_logic;
      TX_BUSY_O         : out    std_logic;
      RX_DATA_O         : out    std_logic_vector( 7 downto 0 );
      RX_VAL_O          : out    std_logic;
      UART_TX_O         : out    std_logic;
      DEBUG_O           : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- types
   type  tStates is (   IDLE,
                        -- state for the command 'A'
                        CMD_A_RD_ADDR,
                        -- states for the command 'R'
                        CMD_R_RD_LEN,
                        CMD_R_RD_1,
                        CMD_R_RD_0,
                        CMD_R_TX_V0,
                        CMD_R_WAIT,
                        -- states for the command 'W'
                        CMD_W_RD_LEN,
                        CMD_W_WAIT,
                        CMD_W_BUS,
                        CMD_W_LOOP,
                        -- states for the prompt
                        PROMPT_OUT,
                        PROMPT_TX_0,
                        PROMPT_WAIT,
                        --
                        WAIT_FOR_END
                     );   
   --   
   -- signals
   signal state         :  tStates           := IDLE;
   --
   signal busaddr       :  std_logic_vector( 6 downto 0 )   := "0000000";
   signal busread       :  std_logic                        := '0';
   signal buswrite      :  std_logic                        := '0';
   signal busdatao      :  std_logic_vector( 7 downto 0 )   := X"00";
   signal busdatai      :  std_logic_vector( 7 downto 0 );
   --
   signal uarttxdat     :  std_logic_vector( 7 downto 0 )   := X"00";
   signal uarttxval     :  std_logic                        := '0';
   signal uarttxbusy    :  std_logic;
   signal uartrxdat     :  std_logic_vector( 7 downto 0 );
   signal uartrxval     :  std_logic;
   --
   signal readlen       :  std_logic_vector( 7 downto 0 );
   --
begin
   --
   UART   : RS232Uart
   port map
   (
      CLK_50MHZ_I                => CLK_I,
      RST_I                      => RST_I,
      TX_DATA_I                  => uarttxdat,
      TX_VAL_I                   => uarttxval,
      UART_RX_I                  => UART_RX_I,
      TX_BUSY_O                  => uarttxbusy,
      RX_DATA_O                  => uartrxdat,
      RX_VAL_O                   => uartrxval,
      UART_TX_O                  => UART_TX_O,
      DEBUG_O                    => open
   );
   --
   -- ------------------------------------------------------
   -- This process 
   -- ------------------------------------------------------
   UARTCTRL : process( CLK_I )
   begin
      --
      if( rising_edge( CLK_I ) )then                        --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            state       <= IDLE;                            --  reset state machine
            busaddr     <= ( others => '0' );               --  reset bus address
            busdatao    <= ( others => '0' );               --  reset bus data output
            buswrite    <= '0';                             --  reset bus write signal
            busread     <= '0';                             --  reset bus read signal
            uarttxdat   <= ( others => '0' );
            uarttxval   <= '0';
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --
            case state is
            when IDLE            =>                         --  STATE: IDLE 
               if( uartrxval = '1' )then                    --  a character received?
                  --                                        --   yes, then...
                  if( uartrxdat = X"41" )then               --   command 'A' for 'address setup'?
                     state <= CMD_A_RD_ADDR;                --    yes, then next state
                  elsif( uartrxdat = X"52" )then            --   command 'R' for 'read'?
                     state <= CMD_R_RD_LEN;                 --    yes, then handle read
                  elsif( uartrxdat = X"57" )then            --   command 'W' for 'write'?
                     state <= CMD_W_RD_LEN;                 --    yes, then handle write
                  else                                      --   otherwise
                     state <= PROMPT_OUT;                   --    output the prompt
                  end if;
                  --
               end if;
            --
            when CMD_A_RD_ADDR   =>                         --  STATE: COMMAND 'A' READ ADDR
               if( uartrxval = '1' )then                    --  a character received?
                  busaddr  <= uartrxdat( 6 downto 0 );      --   yes, then get the address
                  state    <= IDLE;                         --   command complete, back to IDLE
               end if;
            --
            when CMD_R_RD_LEN    =>                         --  STATE: COMMAND 'R' READ LENGTH
               if( uartrxval = '1' )then                    --  a character received?
                  readlen  <= uartrxdat;                    --   yes, then set the length
                  state    <= CMD_R_RD_1;                   --   set next state
               end if;
            --
            when CMD_R_RD_1      =>                         --  STATE: COMMAND 'R' ACTIVATE INTERNAL READ ACCESS
               busread  <= '1';                             --  activate internal read signal
               state    <= CMD_R_RD_0;                      --  set next state
            --
            when CMD_R_RD_0      =>                         --  STATE: COMMAND 'R' RELEASE INTERNAL READ ACCESS
               busread     <= '0';                          --  release internal read signal
               uarttxdat   <= busdatai;                     --  set received byte as uart tx byte
               uarttxval   <= '1';                          --  activate uart transmitter
               state       <= CMD_R_TX_V0;                  --  set next state
            --
            when CMD_R_TX_V0     =>                         --  STATE: COMMAND 'R' RELEASE UART TX
               uarttxval   <= '0';                          --  release uart transmitter
               busaddr     <= busaddr + 1;                  --  increment busaddress for the next byte
               readlen     <= readlen - 1;                  --  decrement the number of transmitted bytes
               state       <= CMD_R_WAIT;                   --  set next state
            --
            when CMD_R_WAIT      =>                         --  STATE: COMMAND 'R' WAIT TILL BYTE TRANSMITTED
               if( uarttxbusy = '0' )then                   --  is uart not busy (then the byte was transmitted)?
                  if( readlen = X"00" )then                 --   yes, all bytes are transmitted?
                     state <= IDLE;                         --    command complete go to IDLE
                  else                                      --   otherwise (not all bytes are transmitted)
                     state <= CMD_R_RD_1;                   --    set next state (read loop)
                  end if;
               end if;
            --
            when CMD_W_RD_LEN    =>                         --  STATE: COMMAND 'W' READ LENGTH
               if( uartrxval = '1' )then                    --  a character received?
                  readlen  <= uartrxdat;                    --   yes, then set the length
                  state    <= CMD_W_WAIT;                   --   set next state
               end if;
            --
            when CMD_W_WAIT      =>                         --  STATE: COMMAND 'W' WAIT FOR DATABYTE
               if( uartrxval = '1' )then                    --  a data byte received?
                  busdatao <= uartrxdat;                    --   set received byte as bus data
                  buswrite <= '1';                          --   activate write signal
                  state    <= CMD_W_BUS;                    --   set next state
               end if;
            --
            when CMD_W_BUS       =>                         --  STATE: COMMAND 'W' RELEASE BUS
               buswrite    <= '0';                          --  release write signal
               busaddr     <= busaddr + 1;                  --  increment bus address
               readlen     <= readlen - 1;                  --  decrement number of byte
               state       <= CMD_W_LOOP;                   --  set next state
               --
            when CMD_W_LOOP      =>                         --  STATE: COMMAND 'W' LOOP OR END
               if( readlen = X"00" )then                    --  all bytes written?
                  state <= IDLE;                            --   yes, then command complete back to IDLE
               else                                         --  if not
                  state <= CMD_W_WAIT;                      --   wait for the next data byte
               end if;
            --
            when PROMPT_OUT      =>                         --  STATE: PROMPT OUTPUT
               uarttxdat   <= X"40";                        --  set prompt character '@'
               uarttxval   <= '1';                          --  activate uart transmitter
               state       <= PROMPT_TX_0;                  --  set next state
            --
            when PROMPT_TX_0     =>                         --  STATE: PROMPT OUTPUT RELEASE TX LINE
               uarttxval   <= '0';                          --  activate uart transmitter
               state       <= PROMPT_WAIT;                  --  set next state
            --
            when PROMPT_WAIT     =>                         --  STATE: PROMPT OUTPUT WAIT
               if( uarttxbusy = '0' )then                   --  is uart not busy (then the byte was transmitted)?
                  state    <= IDLE;                         --   yes, then go back to IDLE state 
               end if;
            --
            when others          =>                         --  UNKNOWN STATE
               state <= IDLE;                               --  go back to IDLE state
            --
            end case;   --case state is
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_I ) )
      --
   end process;
   --
   BUS_ADDR_O     <= busaddr;                               --address of the internal bus
   BUS_DATA_O     <= busdatao;                              --data for write to internal bus
   busdatai       <= BUS_DATA_I;                            --data from the internal bus
   BUS_READ_O     <= busread;                               --read strobe signal for the internal bus
   BUS_WRITE_O    <= buswrite;                              --write strobe signal for the internal bus
   --
   DEBUG_O  <= ( others => '0' );
   --
end behavioral;
