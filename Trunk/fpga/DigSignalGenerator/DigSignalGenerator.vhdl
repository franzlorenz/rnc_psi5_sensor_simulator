-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          DigSignalGenerator.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'DigSignalGenerator' is:
-- Generates different test signals with a fixed
-- frequency delivered by the clock input CLK_50MHZ_I.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity DigSignalGenerator is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- INTERFACE INTERNAL
   GEN_CHL_O      : out    std_logic_vector( 2 downto 0 );
   GEN_DAT_O      : out    std_logic_vector( 15 downto 0 );
   GEN_VAL_O      : out    std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end DigSignalGenerator;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of DigSignalGenerator is 
   --
   -- type
   type tMemory is array( 63 downto 0 ) of std_logic_vector( 15 downto 0 );
   type tStates is ( IDLE,
                     COUNT,
                     CHL0_STA,
                     CHL0_END
                  );   
   --
   -- signals
   signal state         :  tStates                          := IDLE;
   signal timer         :  std_logic_vector( 15 downto 0 )  := X"0000";
   signal memaddr       :  std_logic_vector(  5 downto 0 )  := "000000";
   signal gendat        :  std_logic_vector( 15 downto 0 )  := X"0000";
   signal genchl        :  std_logic_vector(  2 downto 0 )  := "000";
   signal genval        :  std_logic                        := '0';
   --
   signal sinewave      :  tMemory  := (     X"0000",       --00,0.000
                                             X"0C8B",       --01,0.098
                                             X"18F8",       --02,0.195
                                             X"2528",       --03,0.290
                                             X"30FB",       --04,0.383
                                             X"3C56",       --05,0.471
                                             X"471C",       --06,0.556
                                             X"5133",       --07,0.634
                                             X"5A82",       --08,0.707
                                             X"62F2",       --09,0.773
                                             X"6A6D",       --10,0.831
                                             X"70E2",       --11,0.882
                                             X"7641",       --12,0.924
                                             X"7A7D",       --13,0.957
                                             X"7D8A",       --14,0.981
                                             X"7F62",       --15,0.995
                                             X"7FFF",       --16,1.000
                                             X"7F62",       --17,0.995
                                             X"7D8A",       --18,0.981
                                             X"7A7D",       --19,0.957
                                             X"7641",       --20,0.924
                                             X"70E2",       --21,0.882
                                             X"6A6D",       --22,0.831
                                             X"62F2",       --23,0.773
                                             X"5A82",       --24,0.707
                                             X"5133",       --25,0.634
                                             X"471C",       --26,0.556
                                             X"3C56",       --27,0.471
                                             X"30FB",       --28,0.383
                                             X"2528",       --29,0.290
                                             X"18F8",       --30,0.195
                                             X"0C8B",       --31,0.098
                                             X"0000",       --32,0.000
                                             X"F375",       --33,-0.098
                                             X"E708",       --34,-0.195
                                             X"DAD8",       --35,-0.290
                                             X"CF05",       --36,-0.383
                                             X"C3AA",       --37,-0.471
                                             X"B8E4",       --38,-0.556
                                             X"AECD",       --39,-0.634
                                             X"A57E",       --40,-0.707
                                             X"9D0F",       --41,-0.773
                                             X"9593",       --42,-0.831
                                             X"8F1E",       --43,-0.882
                                             X"89BF",       --44,-0.924
                                             X"8583",       --45,-0.957
                                             X"8276",       --46,-0.981
                                             X"809E",       --47,-0.995
                                             X"8001",       --48,-1.000
                                             X"809E",       --49,-0.995
                                             X"8276",       --50,-0.981
                                             X"8583",       --51,-0.957
                                             X"89BF",       --52,-0.924
                                             X"8F1E",       --53,-0.882
                                             X"9593",       --54,-0.831
                                             X"9D0E",       --55,-0.773
                                             X"A57E",       --56,-0.707
                                             X"AECD",       --57,-0.634
                                             X"B8E4",       --58,-0.556
                                             X"C3AA",       --59,-0.471
                                             X"CF05",       --60,-0.383
                                             X"DAD8",       --61,-0.290
                                             X"E708",       --62,-0.195
                                             X"F375"        --63,-0.098
                                       );
   --
begin
   --
   -- ------------------------------------------------------
   -- This process does calculates the 
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            state    <= IDLE;                               --  set idle state
            memaddr  <= ( others => '0' );                  --  reset memory address
            timer    <= ( others => '0' );
            gendat   <= ( others => '0' );
            genchl   <= ( others => '0' );
            genval   <= '0';
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            --
            case state is                                   --  handle state machine...
            when IDLE   =>                                  --  STATE IDLE
               timer    <= ( others => '0' );               --   reset timer
               memaddr  <= memaddr + 1;
               state    <= COUNT;
               --
            when COUNT  =>
               timer <= timer + 1;
               if( timer = X"1E81" )then
                  state <= CHL0_STA;
               end if;
               --
            when CHL0_STA =>
               genchl      <= "000";
               gendat      <= sinewave( conv_integer( unsigned( memaddr ) ) );
               genval      <= '1';
               timer       <= ( others => '0' );
               state       <= CHL0_END;
               --
            when CHL0_END  =>
               genval      <= '0';
               state       <= IDLE;
               --
            when others =>                                 
               state       <= IDLE;
               --
            end case;   --case state is
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK... ) )
      --
   end process;
   --
   -- concurrent statements
   --
   GEN_CHL_O      <= genchl;
   GEN_VAL_O      <= genval;
   GEN_DAT_O      <= gendat;
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
