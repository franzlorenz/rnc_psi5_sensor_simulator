## ----------------------------------------------------------------------------
#  Project        <PROJECTNAME>
#  (c) copyright  2017
#  Company        Harman/Becker Automotive Systems GmbH
#                 All rights reserved
#  Secrecy Level  STRICTLY CONFIDENTIAL
#  ----------------------------------------------------------------------------
#
#  @file          sinewave.pl
#  @ingroup       sinewave
#  @author        Franz Lorenz
#
#  The functionality of the module 'sinewave' is:
#   - ....
#   - ....
#   - ....
## ----------------------------------------------------------------------------

#  ---------------------------------------------------------
#  USEs
#  ---------------------------------------------------------
use warnings;
use strict;

#  ---------------------------------------------------------
#  GLOBALs
#  ---------------------------------------------------------

#  ---------------------------------------------------------
#  SUBs
#  ---------------------------------------------------------

#  ---------------------------------------------------------
#  MAIN
#  ---------------------------------------------------------
my $nVal = 0.0;
my $sVal = "";
#
for( my $nCtr=0; $nCtr < 64; $nCtr++ )
{
   $nVal = sin( 2.0 * 3.1415921654 / 64 * $nCtr );
   $sVal = sprintf( "%04X", $nVal*32768 );
   $sVal = substr( $sVal, length( $sVal )-4, 4 );
   printf( "X\"%s\",       #%02d,%5.3f\n", $sVal, $nCtr, $nVal );
   ##  print " info : step '$nCtr' = '$nVal'\n";
}
#
print "info : script ends.";
