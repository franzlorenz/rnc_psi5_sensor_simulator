-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_PSI5SensorBlock.vhdl
-- @ingroup       PSI5SensorBlock
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'PSI5SensorBlock'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_PSI5SensorBlock is
end tb_PSI5SensorBlock;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_PSI5SensorBlock is
   --
   -- component declaration for the DUT
   component PSI5SensorBlock
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      MOD_ADDR_I                    : in     std_logic_vector( 3 downto 0 );
      BUS_ADDR_I                    : in     std_logic_vector( 6 downto 0 );
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      BUS_WRITE_I                   : in     std_logic;
      BUS_READ_I                    : in     std_logic;
      ADC_CHL_I                     : in     std_logic_vector( 2 downto 0 );
      ADC_DAT_I                     : in     std_logic_vector( 15 downto 0 );
      ADC_VAL_I                     : in     std_logic;
      PSI5_SYNC_NI                  : in     std_logic;
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      PSI5_DAT_O                    : out    std_logic;
      PSI5_VAL_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal MOD_ADDR_I                : std_logic_vector( 3 downto 0 ) := "0000";
   signal BUS_ADDR_I                : std_logic_vector( 6 downto 0 ) := "0000000";
   signal BUS_DATA_I                : std_logic_vector( 7 downto 0 ) := "00000000";
   signal BUS_WRITE_I               : std_logic := '0';
   signal BUS_READ_I                : std_logic := '0';
   signal ADC_CHL_I                 : std_logic_vector( 2 downto 0 ) := "000";
   signal ADC_DAT_I                 : std_logic_vector( 15 downto 0 ) := "0000000000000000";
   signal ADC_VAL_I                 : std_logic := '0';
   signal PSI5_SYNC_NI              : std_logic := '0';
   --
   -- ----------------------
   -- output signals
   --
   signal BUS_DATA_O                : std_logic_vector( 7 downto 0 );
   signal PSI5_DAT_O                : std_logic;
   signal PSI5_VAL_O                : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   signal testCtr                   : integer := 0;
   signal testAdcChl                : integer := 0;
   --
begin
   --
   DUT   : PSI5SensorBlock
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => MOD_ADDR_I,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_READ_I                 => BUS_READ_I,
      ADC_CHL_I                  => ADC_CHL_I,
      ADC_DAT_I                  => ADC_DAT_I,
      ADC_VAL_I                  => ADC_VAL_I,
      PSI5_SYNC_NI               => PSI5_SYNC_NI,
      BUS_DATA_O                 => BUS_DATA_O,
      PSI5_DAT_O                 => PSI5_DAT_O,
      PSI5_VAL_O                 => PSI5_VAL_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 10 ns;
      CLK_50MHZ_I <= '1';
      wait for 10 ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- This process will generates data coming from the ADC
   -- ------------------------------------------------------
   ADC         : process
   begin
      --
      ADC_CHL_I   <= "000";                                 --reset adc channel
      ADC_DAT_I   <= X"1234";                               --set adc value
      for testAdcChl in 0 to 7 loop
         wait for 10 ns;                                    -- wait
         ADC_VAL_I   <= '1';                                -- set valid flag
         wait for 50 ns;                                    -- wait a little bit
         ADC_VAL_I   <= '0';                                -- release valid flag
         wait for 90 ns;
         ADC_CHL_I   <= ADC_CHL_I + 1;
         ADC_DAT_I   <= ADC_DAT_I + X"1111";
      end loop;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      
      -- CHECK DATABUS INTERFACE
      -- g_MOD_ADDR is X"1", so the 
      -- bus address is set to 0001000-0001111 (0x8-0xF)
      -- ---------------------------------------------------
      MOD_ADDR_I  <= X"1";                                  --init module address
      BUS_ADDR_I  <= "0000000";                             --init busaddress
      BUS_READ_I  <= '0';                                   --activate read
      BUS_WRITE_I <= '0';                                   --deactivate write
      --
      for testCtr in 0 to 16 loop
         BUS_WRITE_I <= '1';
         wait for 50 ns;
         BUS_WRITE_I <= '0';
         wait for 50 ns;
         BUS_READ_I  <= '1';
         wait for 50 ns;
         BUS_READ_I  <= '0';
         wait for 1 ns;
         BUS_DATA_I  <= BUS_DATA_I + X"17";
         BUS_ADDR_I  <= BUS_ADDR_I + 1;
      end loop;
      --
      -- ---------------------------------------------------
      -- setup default value
      BUS_ADDR_I  <= "0001000";                             --init busaddress
      --
      BUS_DATA_I  <= X"02";                                 --set data
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      --
      BUS_ADDR_I  <= BUS_ADDR_I + 1;
      BUS_DATA_I  <= X"04";                                 --set selector
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      --
      BUS_ADDR_I  <= BUS_ADDR_I + 1;
      BUS_DATA_I  <= X"CC";                                 --set data slot 1 high
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      --
      BUS_ADDR_I  <= BUS_ADDR_I + 1;
      BUS_DATA_I  <= X"11";                                 --set data slot 1 low
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      --
      BUS_ADDR_I  <= BUS_ADDR_I + 1;
      BUS_DATA_I  <= X"DD";                                 --set data slot 2 high
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      --
      BUS_ADDR_I  <= BUS_ADDR_I + 1;
      BUS_DATA_I  <= X"22";                                 --set data slot 2 low
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access

      --
      -- check if sensor data will be selected
      -- the signals sens_data1 and sens_data2
      -- has to reflect the ADC input samples
      --
      BUS_ADDR_I  <= "0001001";                             --init busaddress to data selector register
      --
      BUS_DATA_I  <= X"00";                                 --set data
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      wait for 1 us;
      --
      BUS_DATA_I  <= X"01";                                 --set data
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      wait for 1 us;
      --
      BUS_DATA_I  <= X"02";                                 --set data
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      wait for 1 us;
      --
      BUS_DATA_I  <= X"03";                                 --set data
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      wait for 1 us;
      --
      BUS_DATA_I  <= X"04";                                 --set data
      BUS_WRITE_I <= '1';                                   --activate write access
      wait for 50 ns;                                       --wait
      BUS_WRITE_I <= '0';                                   --release write access
      wait for 1 us;
      --
      --
      -- check when the register values are valid
      -- on the internal bus when the bus address
      -- changes.
      --
      BUS_ADDR_I  <= MOD_ADDR_I & "000";                    --setup valid bus address
      for testCtr in 0 to 8 loop                            -- read all registers
         wait for 50 ns;                                    --  wait a little bit
         BUS_ADDR_I  <= BUS_ADDR_I + 1;                     --  increment address
      end loop;
      --
      wait for 100 ns;
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
