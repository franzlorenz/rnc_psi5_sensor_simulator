-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          PSI5SensorBlock.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'PSI5SensorBlock' is:
--  - All available sensor
--  - I2C registers
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity PSI5SensorBlock is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- Bus interface to I2C,SPI or others
   MOD_ADDR_I     : in     std_logic_vector( 3 downto 0 );
   BUS_ADDR_I     : in     std_logic_vector( 6 downto 0 );
   BUS_DATA_I     : in     std_logic_vector( 7 downto 0 );
   BUS_DATA_O     : out    std_logic_vector( 7 downto 0 );
   BUS_WRITE_I    : in     std_logic;
   BUS_READ_I     : in     std_logic;
   --
   -- ADC interface for the analogue signals
   ADC_CHL_I      : in     std_logic_vector( 2 downto 0 );
   ADC_DAT_I      : in     std_logic_vector( 15 downto 0 );
   ADC_VAL_I      : in     std_logic;
   --
   -- ADC interface for the analogue signals
   GEN_CHL_I      : in     std_logic_vector( 2 downto 0 );
   GEN_DAT_I      : in     std_logic_vector( 15 downto 0 );
   GEN_VAL_I      : in     std_logic;
   --
   PSI5_SYNC_NI   : in     std_logic;
   PSI5_READY_I   : in     std_logic;
   PSI5_DAT_O     : out    std_logic;
   PSI5_VAL_O     : out    std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end PSI5SensorBlock;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of PSI5SensorBlock is 
   --
   component Routing
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      ADC_CHL_I                     : in     std_logic_vector(  2 downto 0 );
      ADC_DAT_I                     : in     std_logic_vector( 15 downto 0 );
      ADC_VAL_I                     : in     std_logic;
      GEN_CHL_I                     : in     std_logic_vector(  2 downto 0 );
      GEN_DAT_I                     : in     std_logic_vector( 15 downto 0 );
      GEN_VAL_I                     : in     std_logic;
      SENS_SEL_I                    : in     std_logic_vector(  3 downto 0 );
      SENS_CONST1_I                 : in     std_logic_vector( 15 downto 0 );
      SENS_CONST2_I                 : in     std_logic_vector( 15 downto 0 );
      SENS_DATA1_O                  : out    std_logic_vector( 15 downto 0 );
      SENS_DATA2_O                  : out    std_logic_vector( 15 downto 0 );
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component PSI5Sensor16G
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      SEN_TRIGGER_I                 : in     std_logic;
      SEN_DATA_1_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_DATA_2_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_BUSY_O                    : out    std_logic;
      PSI5_DAT_O                    : out    std_logic;
      PSI5_VAL_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component PSI5Sensor1G6
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      SEN_TRIGGER_I                 : in     std_logic;
      SEN_DATA_1_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_DATA_2_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_BUSY_O                    : out    std_logic;
      PSI5_DAT_O                    : out    std_logic;
      PSI5_VAL_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component PSI5SensorRNS20
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      SEN_TRIGGER_I                 : in     std_logic;
      SEN_DATA_1_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_DATA_2_I                  : in     std_logic_vector( 15 downto 0 );
      SEN_BUSY_O                    : out    std_logic;
      PSI5_DAT_O                    : out    std_logic;
      PSI5_VAL_O                    : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   component IOReg8bit
   generic
   (
      g_RESETVALUE                  :  std_logic_vector( 7 downto 0 )   := X"00"
   );
   port
   (
      CLK_I                         : in     std_logic;
      RST_I                         : in     std_logic;
      MOD_ADDR_I                    : in     std_logic_vector( 6 downto 0 );
      BUS_ADDR_I                    : in     std_logic_vector( 6 downto 0 );
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      BUS_WRITE_I                   : in     std_logic;
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      DATA_I                        : in     std_logic_vector( 7 downto 0 );
      DATA_O                        : out    std_logic_vector( 7 downto 0 );
      DATAWR_O                      : out    std_logic
   );
   end component;
   --
   -- signals
   signal reg0addr                  : std_logic_vector( 6 downto 0 );
   signal reg0busdata               : std_logic_vector( 7 downto 0 );
   signal reg0datai                 : std_logic_vector( 7 downto 0 );
   signal reg0datao                 : std_logic_vector( 7 downto 0 );
   --
   signal reg1addr                  : std_logic_vector( 6 downto 0 );
   signal reg1busdata               : std_logic_vector( 7 downto 0 );
   signal reg1datai                 : std_logic_vector( 7 downto 0 );
   signal reg1datao                 : std_logic_vector( 7 downto 0 );
   --
   signal reg2addr                  : std_logic_vector( 6 downto 0 );
   signal reg2busdata               : std_logic_vector( 7 downto 0 );
   signal reg2datai                 : std_logic_vector( 7 downto 0 );
   signal reg2datao                 : std_logic_vector( 7 downto 0 );
   --
   signal reg3addr                  : std_logic_vector( 6 downto 0 );
   signal reg3busdata               : std_logic_vector( 7 downto 0 );
   signal reg3datai                 : std_logic_vector( 7 downto 0 );
   signal reg3datao                 : std_logic_vector( 7 downto 0 );
   --
   signal reg4addr                  : std_logic_vector( 6 downto 0 );
   signal reg4busdata               : std_logic_vector( 7 downto 0 );
   signal reg4datai                 : std_logic_vector( 7 downto 0 );
   signal reg4datao                 : std_logic_vector( 7 downto 0 );
   --
   signal reg5addr                  : std_logic_vector( 6 downto 0 );
   signal reg5busdata               : std_logic_vector( 7 downto 0 );
   signal reg5datai                 : std_logic_vector( 7 downto 0 );
   signal reg5datao                 : std_logic_vector( 7 downto 0 );
   --
   --
   -- signals of the SENSOR routine interface
   signal sens_const1               :  std_logic_vector( 15 downto 0 );
   signal sens_const2               :  std_logic_vector( 15 downto 0 );
   signal sens_data1                :  std_logic_vector( 15 downto 0 );
   signal sens_data2                :  std_logic_vector( 15 downto 0 );
   signal sens_trigedgedet          :  std_logic_vector( 1 downto 0 );
   signal psi5_ready                :  std_logic;
   signal psi5_valid                :  std_logic;
   signal psi5_data                 :  std_logic;
   --
   -- signals of the SENSORs
   signal sens_trig                 :  std_logic;
   signal sens_16G_busy             :  std_logic;
   signal sens_16G_pdat             :  std_logic;
   signal sens_16G_pval             :  std_logic;
   signal sens_1G6_busy             :  std_logic;
   signal sens_1G6_pdat             :  std_logic;
   signal sens_1G6_pval             :  std_logic;
   signal sens_R20_busy             :  std_logic;
   signal sens_R20_pdat             :  std_logic;
   signal sens_R20_pval             :  std_logic;
   --
begin
   --
   I2C_0       : IOReg8bit
   generic map
   (
      g_RESETVALUE               => X"00"                   --setup sensor type to 'OFF'
   )
   port map
   (
      CLK_I                      => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => reg0addr,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_DATA_O                 => reg0busdata,
      DATA_I                     => reg0datai,
      DATA_O                     => reg0datao,
      DATAWR_O                   => open
   );
   --
   I2C_1       : IOReg8bit
   generic map
   (
      g_RESETVALUE               => X"00"
   )
   port map
   (
      CLK_I                      => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => reg1addr,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_DATA_O                 => reg1busdata,
      DATA_I                     => reg1datai,
      DATA_O                     => reg1datao,
      DATAWR_O                   => open
   );
   --
   I2C_2       : IOReg8bit
   generic map
   (
      g_RESETVALUE               => X"CC"
   )
   port map
   (
      CLK_I                      => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => reg2addr,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_DATA_O                 => reg2busdata,
      DATA_I                     => reg2datai,
      DATA_O                     => reg2datao,
      DATAWR_O                   => open
   );
   --
   I2C_3       : IOReg8bit
   generic map
   (
      g_RESETVALUE               => X"11"
   )
   port map
   (
      CLK_I                      => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => reg3addr,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_DATA_O                 => reg3busdata,
      DATA_I                     => reg3datai,
      DATA_O                     => reg3datao,
      DATAWR_O                   => open
   );
   --
   I2C_4       : IOReg8bit
   generic map
   (
      g_RESETVALUE               => X"CC"
   )
   port map
   (
      CLK_I                      => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => reg4addr,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_DATA_O                 => reg4busdata,
      DATA_I                     => reg4datai,
      DATA_O                     => reg4datao,
      DATAWR_O                   => open
   );
   --
   I2C_5       : IOReg8bit
   generic map
   (
      g_RESETVALUE               => X"22"
   )
   port map
   (
      CLK_I                      => CLK_50MHZ_I,
      RST_I                      => RST_I,
      MOD_ADDR_I                 => reg5addr,
      BUS_ADDR_I                 => BUS_ADDR_I,
      BUS_DATA_I                 => BUS_DATA_I,
      BUS_WRITE_I                => BUS_WRITE_I,
      BUS_DATA_O                 => reg5busdata,
      DATA_I                     => reg5datai,
      DATA_O                     => reg5datao,
      DATAWR_O                   => open
   );
   --
   SENSOR_ROUTE   : Routing
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      ADC_CHL_I                  => ADC_CHL_I,
      ADC_DAT_I                  => ADC_DAT_I,
      ADC_VAL_I                  => ADC_VAL_I,
      GEN_CHL_I                  => GEN_CHL_I,
      GEN_DAT_I                  => GEN_DAT_I,
      GEN_VAL_I                  => GEN_VAL_I,
      SENS_SEL_I                 => reg1datao( 3 downto 0 ),
      SENS_CONST1_I              => sens_const1,
      SENS_CONST2_I              => sens_const2,
      SENS_DATA1_O               => sens_data1,
      SENS_DATA2_O               => sens_data2,
      DEBUG_O                    => open
   );
   --
   SENSOR_16G     : PSI5Sensor16G
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      SEN_TRIGGER_I              => '1',
      SEN_DATA_1_I               => sens_data1,
      SEN_DATA_2_I               => sens_data2,
      SEN_BUSY_O                 => sens_16G_busy,
      PSI5_DAT_O                 => sens_16G_pdat,
      PSI5_VAL_O                 => sens_16G_pval,
      DEBUG_O                    => open
   );
   --
   SENSOR_1G6     : PSI5Sensor1G6
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      SEN_TRIGGER_I              => '1',
      SEN_DATA_1_I               => sens_data1,
      SEN_DATA_2_I               => sens_data2,
      SEN_BUSY_O                 => sens_1G6_busy,
      PSI5_DAT_O                 => sens_1G6_pdat,
      PSI5_VAL_O                 => sens_1G6_pval,
      DEBUG_O                    => open
   );
   --
   SENSOR_RNS20   : PSI5SensorRNS20
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      SEN_TRIGGER_I              => sens_trig,
      SEN_DATA_1_I               => sens_data1,
      SEN_DATA_2_I               => sens_data2,
      SEN_BUSY_O                 => sens_R20_busy,
      PSI5_DAT_O                 => sens_R20_pdat,
      PSI5_VAL_O                 => sens_R20_pval,
      DEBUG_O                    => open
   );
   --
   --
   -- ------------------------------------------------------
   -- This process does calculates the 
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            sens_trigedgedet  <= PSI5_SYNC_NI & PSI5_SYNC_NI;
            sens_trig         <= '0';
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            --
            if( psi5_valid = '0' )then                      --  no communication on PSI5?
               psi5_ready  <= PSI5_READY_I;                 --   yes, then take the external voltage-ready signal
            end if;
            --
            sens_trigedgedet  <= sens_trigedgedet( 0 ) &    --  update trigger edge
                                 PSI5_SYNC_NI;              --   detector
            if( sens_trigedgedet = "10" )then               --  start (falling edge) detected?
               sens_trig   <= '1';                          --   yes, then activate trigger
            else                                            --  otherwise
               sens_trig   <= '0';                          --   release trigger signal
            end if;  --if( sens_trigedgedet = "10" )then
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK... ) )
      --
   end process;
   --
   -- concurrent statements
   --
   BUS_DATA_O     <= reg0busdata    or                      --generate the data bus output
                     reg1busdata    or
                     reg2busdata    or
                     reg3busdata    or
                     reg4busdata    or
                     reg5busdata    or
                     X"00";
   reg0addr       <= MOD_ADDR_I & "000";                    --setup register bus address
   reg0datai      <= "000000" & reg0datao( 1 downto 0 );    --reflect only the lowest 2 bits of the sensor type
   reg1addr       <= MOD_ADDR_I & "001";                    --setup register bus address
   reg1datai      <= psi5_ready & "000" & reg1datao( 3 downto 0 );    --reflect only the lowest 4 bits of the input source
   reg2addr       <= MOD_ADDR_I & "010";                    --setup register bus address
   reg2datai      <= reg2datao;                             --reflect const sensor data high byte of slot 1
   reg3addr       <= MOD_ADDR_I & "011";                    --setup register bus address
   reg3datai      <= reg3datao;                             --reflect const sensor data low  byte of slot 1
   sens_const1    <= reg2datao & reg3datao;                 --
   reg4addr       <= MOD_ADDR_I & "100";                    --setup register bus address
   reg4datai      <= reg4datao;                             --reflect const sensor data high byte of slot 2
   reg5addr       <= MOD_ADDR_I & "101";                    --setup register bus address
   reg5datai      <= reg5datao;                             --reflect const sensor data low  byte of slot 2
   sens_const2    <= reg4datao & reg5datao;                 --
   --
   psi5_data      <= '0'              when( psi5_ready = '0'               ) else
                     '0'              when( reg0datao( 1 downto 0 ) = "00" ) else
                     sens_16G_pdat    when( reg0datao( 1 downto 0 ) = "01" ) else
                     sens_1G6_pdat    when( reg0datao( 1 downto 0 ) = "10" ) else
                     sens_R20_pdat;
   PSI5_DAT_O     <= psi5_data;
   --
   psi5_valid     <= '0'              when( reg0datao( 1 downto 0 ) = "00" ) else
                     sens_16G_pval    when( reg0datao( 1 downto 0 ) = "01" ) else
                     sens_1G6_pval    when( reg0datao( 1 downto 0 ) = "10" ) else
                     sens_R20_pval;
   PSI5_VAL_O     <= psi5_valid;
   --
   DEBUG_O(0)     <= psi5_data;
   DEBUG_O(1)     <= psi5_valid;
   DEBUG_O(2)     <= psi5_ready;
   DEBUG_O(3)     <= PSI5_READY_I;
   DEBUG_O(15 downto 4 )   <= ( others => '0' );
   --
end behavioral;
