#-->---------------------------------- Set Simulation Parameter ---------------------------------<--
# Simply change the project settings in this section
# for each new project. There should be no need to
# modify the rest of the script.

set library_file_list {
   work 
   {  
      ../PSI5SensorBlock.vhdl
      ../tb_PSI5SensorBlock.vhdl
      ../../PSI5Sensor16G/PSI5Sensor16G.vhdl
      ../../PSI5Sensor1G6/PSI5Sensor1G6.vhdl
      ../../PSI5SensorRNS20/PSI5SensorRNS20.vhdl
      ../../PSI5Tx/PSI5Tx.vhdl
      ../../Routing/Routing.vhdl
      ../../usTimer/usTimer.vhdl
      ../../IOReg8bit/IOReg8bit.vhdl
   }
}
set top_level     work.tb_PSI5SensorBlock

# start compilation
foreach {library file_list} $library_file_list {
   vlib $library
   vmap work $library
   foreach file $file_list {
      if [regexp {.vhdl?$} $file] {
         vcom -93 $file
      } else {
         vlog $file
      }
   }
}

# start simulation
eval vsim $top_level

# add the previews
do ./wave.do

#  run all
run -all
