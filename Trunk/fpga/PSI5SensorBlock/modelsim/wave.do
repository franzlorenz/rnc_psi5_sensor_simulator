onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/RST_I
add wave -noupdate -divider {BUS INTERFACE}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/MOD_ADDR_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/BUS_ADDR_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/BUS_DATA_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/BUS_DATA_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/BUS_WRITE_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/BUS_READ_I
add wave -noupdate -divider {ADC INTERFACE}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/ADC_CHL_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/ADC_DAT_I
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/ADC_VAL_I
add wave -noupdate -divider {PSI5 INTERFACE}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/PSI5_SYNC_NI
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/PSI5_DAT_O
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/PSI5_VAL_O
add wave -noupdate -divider {DEBUG INTERFACE}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL REGISTERS}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg0busdata
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg0datai
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg0datao
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg1busdata
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg1datai
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg1datao
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg2busdata
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg2datai
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg2datao
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg3busdata
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg3datai
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg3datao
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg4busdata
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg4datai
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg4datao
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg5busdata
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg5datai
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/reg5datao
add wave -noupdate -divider {INTERNAL ROUTING}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_route_sel
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_const1
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_const2
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_data1
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_data2
add wave -noupdate -divider {INTERNAL SENSORS}
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_trig
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_16G_busy
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_16G_pdat
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_16G_pval
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_1G6_busy
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_1G6_pdat
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_1G6_pval
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_R20_busy
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_R20_pdat
add wave -noupdate -radix hexadecimal /tb_psi5sensorblock/DUT/sens_R20_pval
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1883725 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {2852850 ps}
