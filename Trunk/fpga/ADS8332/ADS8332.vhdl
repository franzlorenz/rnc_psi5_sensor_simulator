-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          ADS8332.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'ADS8332' is:
--  - reads all eight channels with the
--    maximum speed rate of the A/D
--  - reads all eight channel one-after-one
--  - maximum spi speed rate
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity ADS8332 is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- INTERFACE TO THE ADS8332 A/D CONVERTER
   ADS_SCLK_O     : out    std_logic;                       -- connect to SCLK pin of the ADS8332
   ADS_SDI_O      : out    std_logic;                       -- connect to SDI pin of the ADS8332
   ADS_SDO_I      : in     std_logic;                       -- connect to SDO pin of the ADS8332
   ADS_CS_NO      : out    std_logic;                       -- connect to CS# of the ADS8332
   ADS_EOC_I      : in     std_logic;                       -- connect to the EOC pin of the ADS8332
   ADS_CONVST_NO  : out    std_logic;                       -- connect to the CONVST# pin of the ADS8332
   --
   -- INTERFACE INTERNAL
   ADC_CHL_O      : out    std_logic_vector( 2 downto 0 );
   ADC_DATA_O     : out    std_logic_vector( 15 downto 0 );
   ADC_VALID_O    : out    std_logic;
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end ADS8332;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of ADS8332 is 
   --
   -- component declaration for SPI interface
   component ADS8332Spi
   port
   (
      CLK_50MHZ_I       : in     std_logic;
      RST_I             : in     std_logic;
      SPI_MISO_I        : in     std_logic;
      TXRXDIR_I         : in     std_logic;
      TRIGGER_I         : in     std_logic;
      DATA_I            : in     std_logic_vector( 3 downto 0 );
      SPI_SCLK_O        : out    std_logic;
      SPI_MOSI_O        : out    std_logic;
      SPI_CS_O          : out    std_logic;
      BUSY_O            : out    std_logic;
      DATA_O            : out    std_logic_vector( 19 downto 0 );
      DEBUG_O           : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- type
   type tStates is ( START,
                     TX_CHANNEL,
                     TX_TRIG,
                     TX_WAIT,
                     CONV_START,
                     CONV_STOP,
                     EOC_WAIT,
                     RX_DATA,
                     RX_TRIG,
                     RX_WAIT,
                     RX_OUT
                  );   
   --
   -- signals
   signal state         :  tStates                          := START;   
   signal spi_txrxdir   :  std_logic                        := '0';
   signal spi_trigger   :  std_logic                        := '0';
   signal spi_din       :  std_logic_vector( 3 downto 0 )   := "0000";
   signal spi_busy      :  std_logic                        := '0';
   signal spi_dout      :  std_logic_vector( 19 downto 0 )  := X"00000";
   --
   signal channel       :  std_logic_vector( 2 downto 0 )   := "000";
   signal eocedge       :  std_logic_vector( 1 downto 0 )   := "00";
   signal convst        :  std_logic                        := '1';
   signal counter       :  std_logic_vector( 3 downto 0 )   := "0000";
   --
begin
   --
   SPI   : ADS8332Spi
   port map
   (
      CLK_50MHZ_I       => CLK_50MHZ_I,
      RST_I             => RST_I,
      SPI_MISO_I        => ADS_SDO_I,
      TXRXDIR_I         => spi_txrxdir,
      TRIGGER_I         => spi_trigger,
      DATA_I            => spi_din,
      SPI_SCLK_O        => ADS_SCLK_O,
      SPI_MOSI_O        => ADS_SDI_O,
      SPI_CS_O          => ADS_CS_NO,
      BUSY_O            => spi_busy,
      DATA_O            => spi_dout,
      DEBUG_O           => open
   );
   --
   -- ------------------------------------------------------
   -- This process does calculates the 
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            state       <= START;                           --  set to init state
            spi_trigger <= '0';                             --  reset spi trigger
            eocedge     <= ADS_EOC_I & ADS_EOC_I;           --  reset edge detector
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            --
            eocedge  <= eocedge(0) & ADS_EOC_I;             --  update end-of-conversion edge detector

            case state is                                   --  handle states
            when START        =>                            --  STATE: START
               channel     <= "000";                        --  reset channel number
               spi_trigger <= '0';                          --  clear trigger bit
               state       <= TX_CHANNEL;                   --  set next state
               --
            when TX_CHANNEL   =>                            --  STATE: TRANSMIT CHANNEL
               spi_trigger <= '1';                          --  trigger spi transmitter
               spi_txrxdir <= '1';                          --  transmit the channel
               state       <= TX_TRIG;                      --  set next state
               --
            when TX_TRIG      =>                            --  STATE: TRIGGER SPI HARDWARE
               spi_trigger <= '0';                          --  release spi trigger
               state       <= TX_WAIT;                      --  set next state
               --
            when TX_WAIT      =>                            --  STATE: WAIT FOR SPI HARDWARE
               if( spi_busy = '0' )then                     --  spi transmission finished?
                  state       <= CONV_START;                --   yes, then set next state
                  counter     <= X"0";                      --   reset counter for width of CONVST pulse
               end if;
               --
            when CONV_START   =>                            --  STATE: GENERATE CONVST PULSE
               if( counter(2) = '1' )then                   --  pulse width reached?
                  state       <= CONV_STOP;                 --   yes, then set next state
               else                                         --  otherwise
                  counter  <= counter + 1;                  --   width counter increment
               end if;
               --
            when CONV_STOP    =>                            --  STATE: RELEASE CONVST PULSE
               state       <= EOC_WAIT;                     --  set next state
               --
            when EOC_WAIT     =>                            --  STATE: WAIT FOR END-OF-CONVERSION
               if( eocedge = "01" )then                     --  rising edge of EOC signal detected?
                  state    <= RX_DATA;                      --   yes, then next state read data
               end if;
               --
            when RX_DATA      =>                            --  STATE: READ ADC VALUE
               spi_trigger <= '1';                          --  trigger spi transmitter
               spi_txrxdir <= '0';                          --  receive the AD value
               state       <= RX_TRIG;                      --  set next state
               --
            when RX_TRIG      =>                            --  STATE: RELEASE SPI TRIGGER
               spi_trigger <= '0';                          --  release spi trigger
               state       <= RX_WAIT;                      --  set next state
               --
            when RX_WAIT      =>                            --  STATE: WAIT TILL AD VALUE READ
               if( spi_busy = '0' )then                     --  spi transmission finished?
                  state       <= RX_OUT;                    --   yes, then set next state
                  counter     <= X"0";                      --   reset counter for width of CONVST pulse
               end if;
               --
            when RX_OUT       =>                            --  STATE: OUTPUT THE RECEIVED DATA
               channel     <= channel + 1;                  --  trigger next ad channel
               state       <= TX_CHANNEL;                   --  go back in the loop...
               --
            when others       =>
               state       <= START;
               --
            end case;   --case state is
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK... ) )
      --
   end process;
   --
   -- concurrent statements
   spi_din        <= '0' & channel;                            --setup transmit data
   ADS_CONVST_NO  <= '0' when( state = CONV_START ) else '1';
   ADC_CHL_O      <= spi_dout( 3 downto 1 )  when( state = RX_OUT );
   ADC_DATA_O     <= spi_dout( 18 downto 3 ) when( state = RX_OUT );
   ADC_VALID_O    <= '1' when( state = RX_OUT ) else '0';
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
