onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/RST_I
add wave -noupdate -divider {AD8332 SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADS_SCLK_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADS_SDI_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADS_SDO_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADS_CS_NO
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADS_EOC_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADS_CONVST_NO
add wave -noupdate -divider {SAMPLE INTERFACE}
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADC_CHL_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADC_DATA_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/ADC_VALID_O
add wave -noupdate -divider {DEBUG SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/state
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/spi_txrxdir
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/spi_trigger
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/spi_din
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/spi_busy
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/spi_dout
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/channel
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/eocedge
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/convst
add wave -noupdate -divider {SPI COMPONENT}
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/RST_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/SPI_SCLK_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/SPI_MOSI_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/SPI_MISO_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/SPI_CS_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/TXRXDIR_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/TRIGGER_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/BUSY_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/DATA_I
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/DATA_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/DEBUG_O
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/state
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/cs
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/bitctr
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/dout
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/din
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/busy
add wave -noupdate -radix hexadecimal /tb_ads8332/DUT/SPI/txrxdir
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {120213 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {105052500 ps}
