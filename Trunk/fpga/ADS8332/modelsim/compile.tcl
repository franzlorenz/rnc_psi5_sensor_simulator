#-->---------------------------------- Set Simulation Parameter ---------------------------------<--
# Simply change the project settings in this section
# for each new project. There should be no need to
# modify the rest of the script.

set library_file_list {
   work 
   {  
      ../ADS8332.vhdl
      ../tb_ADS8332.vhdl
   }
}
set top_level     work.tb_ADS8332

# start compilation
foreach {library file_list} $library_file_list {
   vlib $library
   vmap work $library
   foreach file $file_list {
      if [regexp {.vhdl?$} $file] {
         vcom -93 $file
      } else {
         vlog $file
      }
   }
}

# start simulation
eval vsim $top_level

# add the previews
do ./wave.do

#  run all
run -all
