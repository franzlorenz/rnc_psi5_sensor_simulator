onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/CLK_50MHZ_I
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/RST_I
add wave -noupdate -divider {TX DATA}
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/TRIGGER_I
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/TXRXDIR_I
add wave -noupdate -radix binary /tb_ads8332spi/DUT/DATA_I
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/DATA_O
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/BUSY_O
add wave -noupdate -divider {AD8332 SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/SPI_CS_O
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/SPI_SCLK_O
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/SPI_MOSI_O
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/SPI_MISO_I
add wave -noupdate -divider {DEBUG SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/DEBUG_O
add wave -noupdate -divider {INTERNAL SIGNALS}
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/state
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/cs
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/bitctr
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/dout
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/din
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/busy
add wave -noupdate -radix hexadecimal /tb_ads8332spi/DUT/txrxdir
add wave -noupdate -radix hexadecimal /tb_ads8332spi/testData
add wave -noupdate -radix hexadecimal /tb_ads8332spi/testTx
add wave -noupdate -radix hexadecimal /tb_ads8332spi/testCS
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {68752278 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {11707500 ps}
