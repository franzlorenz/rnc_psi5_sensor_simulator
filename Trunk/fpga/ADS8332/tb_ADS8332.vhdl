-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_ADS8332.vhdl
-- @ingroup       ADS8332
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'ADS8332'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_ADS8332 is
end tb_ADS8332;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_ADS8332 is
   --
   -- component declaration for the DUT
   component ADS8332
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      ADS_SDO_I                     : in     std_logic;
      ADS_EOC_I                     : in     std_logic;
      ADS_SCLK_O                    : out    std_logic;
      ADS_SDI_O                     : out    std_logic;
      ADS_CS_NO                     : out    std_logic;
      ADS_CONVST_NO                 : out    std_logic;
      ADC_CHL_O                     : out    std_logic_vector( 2 downto 0 );
      ADC_DATA_O                    : out    std_logic_vector( 15 downto 0 );
      ADC_VALID_O                   : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal ADS_SDO_I                 : std_logic := '0';
   signal ADS_EOC_I                 : std_logic := '0';
   --
   -- ----------------------
   -- output signals
   --
   signal ADS_SCLK_O                : std_logic;
   signal ADS_SDI_O                 : std_logic;
   signal ADS_CS_NO                 : std_logic;
   signal ADS_CONVST_NO             : std_logic;
   signal ADC_CHL_O                 : std_logic_vector( 2 downto 0 );
   signal ADC_DATA_O                : std_logic_vector( 15 downto 0 );
   signal ADC_VALID_O               : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   signal testData                  : std_logic_vector( 19 downto 0 ) := X"00000";
   signal testTx                    : std_logic_vector( 19 downto 0 ) := X"00000";
   signal testclk                   : std_logic_vector( 1 downto 0 )  := "00";
   signal testcs                    : std_logic_vector( 1 downto 0 )  := "00";
   --
begin
   --
   DUT   : ADS8332
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      ADS_SDO_I                  => ADS_SDO_I,
      ADS_EOC_I                  => ADS_EOC_I,
      ADS_SCLK_O                 => ADS_SCLK_O,
      ADS_SDI_O                  => ADS_SDI_O,
      ADS_CS_NO                  => ADS_CS_NO,
      ADS_CONVST_NO              => ADS_CONVST_NO,
      ADC_CHL_O                  => ADC_CHL_O,
      ADC_DATA_O                 => ADC_DATA_O,
      ADC_VALID_O                => ADC_VALID_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 10 ns;
      CLK_50MHZ_I <= '1';
      wait for 10 ns;
      --
   end process;
   --
   -- ------------------------------------------------------
   -- This process generates a data word from the ADS8332
   -- ------------------------------------------------------
   AD8332_SIM : process
   begin
      wait until falling_edge( CLK_50MHZ_I );
      testclk  <= testclk( 0 ) & ADS_SCLK_O;
      testcs   <= testcs( 0 )  & ADS_CS_NO;
      wait for 1 ns;
      if( testcs = "01" )then
         testTx   <= testData;
         testData <= testData + X"10101";
      else
         if( testclk = "10" )then
            ADS_SDO_I  <= testTx( 19 );
         elsif( testclk = "01" )then
            testTx     <= testTx( 18 downto 0 ) & '0';
         end if;
      end if;
   end process;
   --
   -- ------------------------------------------------------
   -- This process simulates the conversion signals
   -- EOC    (end-of-conversion) and 
   -- CONVST (conversion start)
   -- ------------------------------------------------------
   EOC : process
   begin
      --
      wait until rising_edge( ADS_CONVST_NO );
      wait for 2 us;
      ADS_EOC_I   <= '1';
      wait for 100 ns;
      ADS_EOC_I   <= '0';
      --
   end process;
   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      wait for 100 us;
      
      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
