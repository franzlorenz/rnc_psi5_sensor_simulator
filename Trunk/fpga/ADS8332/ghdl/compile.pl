## ----------------------------------------------------------------------------
#  Project        20170213_RNC_SensorSimulator
#  (c) copyright  2017
#  Company        Harman/Becker Automotive Systems GmbH
#                 All rights reserved
#  Secrecy Level  STRICTLY CONFIDENTIAL
#  ----------------------------------------------------------------------------
#
#  @file          compile.pl
#  @ingroup       compile
#  @author        Franz Lorenz
#
## ----------------------------------------------------------------------------

#  ---------------------------------------------------------
#  USEs
#  ---------------------------------------------------------
use warnings;
use strict;

#  ---------------------------------------------------------
#  GLOBALs
#  ---------------------------------------------------------
my @aVhdlFiles  = {  };
my $sVhdlTBName = "tb_ADS8332";

#  ---------------------------------------------------------
#  SETTINGS
#  ---------------------------------------------------------
my $sGhdlBin     = $ENV{"HDLTOOLS_PATH"}."\\ghdl\\v0.33\\bin\\ghdl";
my $sGhdlFlags   = "--ieee=synopsys -fexplicit";
my $sViewBin     = $ENV{"HDLTOOLS_PATH"}."\\gtkwave\\v3.3.84\\bin\\gtkwave";

#  ---------------------------------------------------------
#  MAIN
#  ---------------------------------------------------------
my $sVhdlTBFile = $sVhdlTBName.".vhdl";
my $nErr = 0;
my $sCmd = "";
#
if( 0 == $#aVhdlFiles )
{  
   @aVhdlFiles = glob( "..\\*.vhd?" );
}
# compile the sources
if( 0 == $nErr )
{
   foreach my $sVhdlFile ( @aVhdlFiles )
   {
      $sCmd = $sGhdlBin." -a ".$sGhdlFlags." $sVhdlFile";
      print "info : compile ghdl '$sCmd'\n";
      system( $sCmd );
   }
}  #if( 0 == $nErr )
#
# run
if( 0 == $nErr )
{
   $sCmd = $sGhdlBin." -e ".$sGhdlFlags." ".$sVhdlTBName;
   print "info : run ghdl '$sCmd'\n";
   system( $sCmd );
   #
   $sCmd = $sGhdlBin." -r ".$sGhdlFlags." ".$sVhdlTBName." --vcd=".$sVhdlTBName.".vcd";
   print "info : run ghdl '$sCmd'\n";
   system( $sCmd );
}  #if( 0 == $nErr )
#
# run viewer
if( 0 == $nErr )
{
   $sCmd = $sViewBin." ".$sVhdlTBName.".vcd";
   print "info : run viewer '$sCmd'\n";
   system( $sCmd );
}  #if( 0 == $nErr )
#
print "info : script ends.\n";
