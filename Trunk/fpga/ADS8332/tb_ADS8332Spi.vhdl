-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_ADS8332Spi.vhdl
-- @ingroup       ADS8332Spi
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'ADS8332Spi'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_ADS8332Spi is
end tb_ADS8332Spi;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_ADS8332Spi is
   --
   -- component declaration for the DUT
   component ADS8332Spi
   port
   (
      CLK_50MHZ_I                   : in     std_logic;
      RST_I                         : in     std_logic;
      SPI_MISO_I                    : in     std_logic;
      TXRXDIR_I                     : in     std_logic;
      TRIGGER_I                     : in     std_logic;
      DATA_I                        : in     std_logic_vector( 3 downto 0 );
      SPI_SCLK_O                    : out    std_logic;
      SPI_MOSI_O                    : out    std_logic;
      SPI_CS_O                      : out    std_logic;
      BUSY_O                        : out    std_logic;
      DATA_O                        : out    std_logic_vector( 19 downto 0 );
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_50MHZ_I               : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal SPI_MISO_I                : std_logic := '0';
   signal TXRXDIR_I                 : std_logic := '0';
   signal TRIGGER_I                 : std_logic := '0';
   signal DATA_I                    : std_logic_vector( 3 downto 0 ) := "0000";
   --
   -- ----------------------
   -- output signals
   --
   signal SPI_SCLK_O                : std_logic;
   signal SPI_MOSI_O                : std_logic;
   signal SPI_CS_O                  : std_logic;
   signal BUSY_O                    : std_logic;
   signal DATA_O                    : std_logic_vector( 19 downto 0 );
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   signal testData                  : std_logic_vector( 19 downto 0 ) := X"00000";
   signal testTx                    : std_logic_vector( 19 downto 0 ) := X"00000";
   signal testclk                   : std_logic_vector( 1 downto 0 )  := "00";
   signal testcs                    : std_logic_vector( 1 downto 0 )  := "00";
   --
begin
   --
   DUT   : ADS8332Spi
   port map
   (
      CLK_50MHZ_I                => CLK_50MHZ_I,
      RST_I                      => RST_I,
      SPI_MISO_I                 => SPI_MISO_I,
      TXRXDIR_I                  => TXRXDIR_I,
      TRIGGER_I                  => TRIGGER_I,
      DATA_I                     => DATA_I,
      SPI_SCLK_O                 => SPI_SCLK_O,
      SPI_MOSI_O                 => SPI_MOSI_O,
      SPI_CS_O                   => SPI_CS_O,
      BUSY_O                     => BUSY_O,
      DATA_O                     => DATA_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_50MHZ_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLK50MHZ_I  : process
   begin
      --
      CLK_50MHZ_I <= '0';
      wait for 10 ns;
      CLK_50MHZ_I <= '1';
      wait for 10 ns;
      --
   end process;
   --
   -- ------------------------------------------------------
   -- This process generates a data word from the ADS8332
   -- ------------------------------------------------------
   AD8332_SIM : process
   begin
      wait until falling_edge( CLK_50MHZ_I );
      testclk  <= testclk( 0 ) & SPI_SCLK_O;
      testcs   <= testcs( 0 )  & SPI_CS_O;
      wait for 1 ns;
      if( testcs = "01" )then
         testTx   <= testData;
         testData <= testData + X"10101";
      else
         if( testclk = "10" )then
            SPI_MISO_I  <= testTx( 19 );
         elsif( testclk = "01" )then
            testTx      <= testTx( 18 downto 0 ) & '0';
         end if;
      end if;
   end process;
   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      RST_I    <= '1';                                      --activate reset
      wait for 50 ns;                                       --wait a little bit
      RST_I    <= '0';                                      --release reset

      --
      -- PUT YOUR TESTS HERE
      --
      TXRXDIR_I   <= '1';                                   --transmit mode
      DATA_I      <= "1100";                                --setup date
      TRIGGER_I   <= '1';                                   --trigger
      wait for 50 ns;                                       -- the module
      TRIGGER_I   <= '0';
      wait for 1 us;                                        --wait...
      --
      TXRXDIR_I   <= '0';                                   --receive mode
      TRIGGER_I   <= '1';                                   --trigger
      wait for 50 ns;                                       -- the module
      TRIGGER_I   <= '0';
      wait for 4 us;
      --
      TXRXDIR_I   <= '0';                                   --receive mode
      TRIGGER_I   <= '1';                                   --trigger
      wait for 50 ns;                                       -- the module
      TRIGGER_I   <= '0';
      wait for 4 us;
      --
      TXRXDIR_I   <= '0';                                   --receive mode
      TRIGGER_I   <= '1';                                   --trigger
      wait for 50 ns;                                       -- the module
      TRIGGER_I   <= '0';
      wait for 4 us;
      --
      TXRXDIR_I   <= '0';                                   --receive mode
      TRIGGER_I   <= '1';                                   --trigger
      wait for 50 ns;                                       -- the module
      TRIGGER_I   <= '0';
      wait for 4 us;
      --

      -- output a testbench result
      -- if( SIGNAL = '?' )then
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- else
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      -- end if;
      --
      -- output a testbench result
      -- assert( xxxxx )
         -- report "Test of XXXXX: PASS"
         -- severity note;
      -- assert( xxxxx )
         -- report "Test of XXXXX: FAIL"
         -- severity note;
      --

      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
