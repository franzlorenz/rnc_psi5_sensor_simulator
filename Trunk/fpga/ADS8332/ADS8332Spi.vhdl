-- ----------------------------------------------------------------------------
-- Project        20170213_RNC_SensorSimulator
-- (c) copyright  2017
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          ADS8332Spi.vhdl
-- @ingroup       20170213_RNC_SensorSimulator
-- @author        Franz Lorenz
--
-- The functionality of the module 'ADS8332Spi' is:
--  - reads all eight channels with the
--    maximum speed rate of the A/D
--  - reads all eight channel one-after-one
--  - maximum spi speed rate
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity ADS8332Spi is
port
(
   CLK_50MHZ_I    : in     std_logic;
   RST_I          : in     std_logic;
   --
   -- INTERFACE TO THE ADS8332 A/D CONVERTER
   SPI_SCLK_O     : out    std_logic;
   SPI_MOSI_O     : out    std_logic;
   SPI_MISO_I     : in     std_logic;
   SPI_CS_O       : out    std_logic;
   --
   TXRXDIR_I      : in     std_logic;
   TRIGGER_I      : in     std_logic;
   BUSY_O         : out    std_logic;
   DATA_I         : in     std_logic_vector( 3 downto 0 );
   DATA_O         : out    std_logic_vector( 19 downto 0 );
   --
   DEBUG_O        : out    std_logic_vector( 15 downto 0 )
);
end ADS8332Spi;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of ADS8332Spi is 
   --
   -- type
   type tStates is ( IDLE,
                     CS_SET_0,
                     DATA_WR,
                     DATA_RD,
                     CS_SET_1
                  );   
   --
   -- signals
   signal state      :  tStates                             := IDLE;
   signal cs         :  std_logic                           := '1';
   signal bitctr     :  std_logic_vector( 7 downto 0 )      := ( others => '0' );
   signal dout       :  std_logic_vector( 3 downto 0 )      := ( others => '0' );
   signal din        :  std_logic_vector( 19 downto 0 )     := ( others => '0' );
   signal txrxdir    :  std_logic                           := '0';
   --
begin

   -- ------------------------------------------------------
   -- This process handles all...
   -- ------------------------------------------------------
   process( CLK_50MHZ_I )
   begin
      --
      if( rising_edge( CLK_50MHZ_I ) )then                  --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            bitctr   <= ( others => '0' );                  --  reset
            dout     <= ( others => '0' );                  --   all signals
            din      <= ( others => '0' );
            txrxdir  <= '0';
            state    <= IDLE;                               --  setup IDLE state
            --
         else     --if( RST_I... )then                      -- reset is NOT active (operation mode)
            --                                              --  then...
            case state is                                   --  state machine....
            when IDLE      =>                               --  STATE: IDLE
               if( TRIGGER_I = '1' )then                    --   trigger detected?
                  state    <= CS_SET_0;                     --    yes, then set next state
                  txrxdir  <= TXRXDIR_I;                    --    get the direction bit
                  dout     <= DATA_I;                       --    get the data for tx
                  bitctr   <= ( others => '0' );            --    reset bit counter
               end if;
               --
            when CS_SET_0  =>                               --  STATE: SET CS ACTIVE 0
               cs    <= '0';                                --  set cs to '0'
               if( txrxdir = '1' )then                      --  direction TX?
                  state    <= DATA_WR;                      --   yes, then set next write state
               else                                         --  otherwise
                  state    <= DATA_RD;                      --   set next read state
               end if;
               --
            when DATA_WR   =>                               --  STATE: DATA WRITE TO ADS8332
               bitctr   <= bitctr + 1;                      --  increment bit counter
               if( bitctr = "00100001" )then                --  all bits transmitted?
                  state <= CS_SET_1;                        --   yes, then set next state
               else                                         --  otherwise
                  if( bitctr( 2 downto 0 ) = "011" )then    --   end of a clock-phase reached?
                     dout  <= dout( 2 downto 0 ) & '0';     --    yes, then shift out register to output the next databit
                  end if;
               end if;  --if( bitctr... )then
               --
            when DATA_RD   =>                               --  STATE: DATA READ FROM ADS8332
               bitctr   <= bitctr + 1;                      --  increment bit counter
               if( bitctr = "10100001" )then                --  all bits transmitted?
                  state <= CS_SET_1;                        --   yes, then set next state
               else                                         --  otherwise
                  if( bitctr( 2 downto 0 ) = "011" )then    --   end of a clock-phase reached?
                     din  <= din(18 downto 0) & SPI_MISO_I; --    yes, then ADS8332 data bit into receive shift register
                  end if;
               end if;  --if( bitctr... )then
               --
            when CS_SET_1  =>                               --  STATE: SET CS DEACTIVE 1
               cs    <= '1';                                --  set cs to '1'
               state <= IDLE;                               --  go back to IDLE state
               --
            when others    => 
               state <= IDLE;
               --
            end case;   --case state is
            --
         end if; --if( RST_I... )then
         --
      end if; --if( rising_edge( CLK... ) )
      --
   end process;
   --
   -- concurrent statements
   SPI_SCLK_O  <= bitctr( 2 );                              --reflect clock for spi
   SPI_MOSI_O  <= dout( 3 );                                --reflect serial tx data
   SPI_CS_O    <= cs;                                       --reflect CS
   --
   BUSY_O      <= '1' when( state /= IDLE ) else '0';       --update busy signal
   DATA_O      <= din;                                      --update serial shift in register
   --
   DEBUG_O        <= ( others => '0' );
   --
end behavioral;
