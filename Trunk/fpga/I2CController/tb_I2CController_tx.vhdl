-- ----------------------------------------------------------------------------
-- Project        PH3 
-- (c) copyright  2016
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          tb_I2CController.vhdl
-- @ingroup       I2CController
-- @author        Franz Lorenz
--
-- This testbench tests the functionality of the module 'I2CController'.
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity tb_I2CController is
end tb_I2CController;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture test of tb_I2CController is
   --
   -- component declaration for the DUT
   component I2CController
   port
   (
      CLK_I                         : in     std_logic;
      RST_I                         : in     std_logic;
      I2C_SCL_I                     : in     std_logic;
      I2C_SDA_I                     : in     std_logic;
      I2C_SDA_O                     : out    std_logic;
      BUS_ADDR_O                    : out    std_logic_vector( 6 downto 0 );
      BUS_DATA_O                    : out    std_logic_vector( 7 downto 0 );
      BUS_DATA_I                    : in     std_logic_vector( 7 downto 0 );
      BUS_READ_O                    : out    std_logic;
      BUS_WRITE_O                   : out    std_logic;
      DEBUG_O                       : out    std_logic_vector( 15 downto 0 )
   );
   end component;
   --
   -- ----------------------
   -- input signals
   --
   signal CLK_I                     : std_logic := '0';
   signal RST_I                     : std_logic := '0';
   signal I2C_SCL_I                 : std_logic := '0';
   signal I2C_SDA_I                 : std_logic := '0';
   signal BUS_DATA_I                : std_logic_vector( 7 downto 0 ) := "00000000";
   --
   -- ----------------------
   -- output signals
   --
   signal I2C_SDA_O                 : std_logic;
   signal BUS_ADDR_O                : std_logic_vector( 6 downto 0 );
   signal BUS_DATA_O                : std_logic_vector( 7 downto 0 );
   signal BUS_READ_O                : std_logic;
   signal BUS_WRITE_O               : std_logic;
   signal DEBUG_O                   : std_logic_vector( 15 downto 0 );
   --
   -- ----------------------
   -- test signals
   --
   constant tI2CStart               : time      := 1 us;
   constant tI2CStop                : time      := 1 us;
   constant tI2CClock               : time      := 1 us;
   signal   iCtr                    : integer   := 0;
   signal   iAccess                 : integer   := 0;
   signal   iWrites                 : integer   := 0;
   signal   txByte                  : std_logic_vector( 7 downto 0 );
   signal   tRegAddr                : std_logic_vector( 7 downto 0 );
   signal   tRegData                : std_logic_vector( 7 downto 0 );
   --
begin
   --
   DUT   : I2CController
   port map
   (
      CLK_I                      => CLK_I,
      RST_I                      => RST_I,
      I2C_SCL_I                  => I2C_SCL_I,
      I2C_SDA_I                  => I2C_SDA_I,
      BUS_DATA_I                 => BUS_DATA_I,
      I2C_SDA_O                  => I2C_SDA_O,
      BUS_ADDR_O                 => BUS_ADDR_O,
      BUS_DATA_O                 => BUS_DATA_O,
      BUS_READ_O                 => BUS_READ_O,
      BUS_WRITE_O                => BUS_WRITE_O,
      DEBUG_O                    => DEBUG_O
   );
   --
   -- ------------------------------------------------------
   -- This process will generate a clock base of the design.
   -- The clock is at signal CLK_I frequency is 50 MHz.
   -- ------------------------------------------------------
   CLKI  : process
   begin
      --
      CLK_I <= '0';
      wait for 10 ns;
      CLK_I <= '1';
      wait for 10 ns;
      --
   end process;

   --
   -- ------------------------------------------------------
   -- The MAIN process.
   -- ------------------------------------------------------
   MAIN  : process
   begin
      --
      -- SET YOUR DESIGN IN RESET
      --
      I2C_SCL_I   <= '1';
      I2C_SDA_I   <= '1';
      RST_I <= '1';                                         --set reset
      wait for 100 ns;
      RST_I <= '0';
      --
      tRegAddr <= X"00";                                    --init register address
      tRegData <= X"00";                                    --init register data
      wait for 1 ns;                                        --wait for init
      
      for iAccess in 0 to 15 loop
         --
         -- START CONDITION   ------------------------------
         --
         I2C_SCL_I   <= '1';                                --generate START condition
         I2C_SDA_I   <= '1';
         wait for tI2CStart;
         I2C_SDA_I   <= '0';
         wait for tI2CStart;
         --
         -- I2C SLAVE ADDRESS + R/W  -----------------------
         txByte <= X"90";                                   --setup TX byte
         wait for 1 ns;
         for iCtr in 7 downto 0 loop                        --send byte
            I2C_SCL_I   <= '0';                             -- deactivate clock
            I2C_SDA_I   <= txByte(iCtr);                    -- output data bit
            wait for tI2CClock;
            I2C_SCL_I   <= '1';                             -- setup clock
            wait for tI2CClock;
         end loop;
         I2C_SCL_I   <= '0';                                -- deactivate clock
         I2C_SDA_I   <= '1';                                -- output data bit
         wait for tI2CClock;                                -- wait
         I2C_SCL_I   <= '1';                                -- setup clock
         wait for tI2CClock;                                -- wait
         --
         -- ADDRESS REGISTER BYTE  -------------------------
         txByte <= tRegAddr;                                --setup register address
         wait for 1 ns;
         for iCtr in 7 downto 0 loop                        --send byte
            I2C_SCL_I   <= '0';                             -- deactivate clock
            I2C_SDA_I   <= txByte(iCtr);                    -- output data bit
            wait for tI2CClock;
            I2C_SCL_I   <= '1';                             -- setup clock
            wait for tI2CClock;
         end loop;
         I2C_SCL_I   <= '0';                                -- deactivate clock
         I2C_SDA_I   <= '1';                                -- output data bit
         wait for tI2CClock;                                -- wait
         I2C_SCL_I   <= '1';                                -- setup clock
         wait for tI2CClock;                                -- wait
         --
         -- WRITE DATA BYTES
         for iWrites in 0 to 3 loop
            --
            -- DATA BYTE      ------------------------------
            txByte <= tRegData;                             --setup DATA#0 byte
            wait for 1 ns;
            for iCtr in 7 downto 0 loop                     --send byte
               I2C_SCL_I   <= '0';                          -- deactivate clock
               I2C_SDA_I   <= txByte(iCtr);                 -- output data bit
               wait for tI2CClock;
               I2C_SCL_I   <= '1';                          -- setup clock
               wait for tI2CClock;
            end loop;
            I2C_SCL_I   <= '0';                             -- deactivate clock
            I2C_SDA_I   <= '1';                             -- output data bit
            wait for tI2CClock;                             -- wait
            I2C_SCL_I   <= '1';                             -- setup clock
            wait for tI2CClock;                             -- wait
            --
            tRegAddr <= tRegAddr + X"01";
            tRegData <= tRegData + X"03";
            wait for 1 ns;
            --
         end loop;   --for iWrites... loop
         --
         -- STOP CONDITION ---------------------------------
         I2C_SCL_I   <= '0';                                --generate STOP condition
         I2C_SDA_I   <= '0';
         wait for tI2CStop;
         I2C_SCL_I   <= '1';
         wait for tI2CStop;
         I2C_SDA_I   <= '1';
         wait for tI2CStop;
         --
      end loop;   --for iAccess... loop

      --
      -- stops the testbench execution
      -- NOTE: DO NOT REMOVE THE FOLLOWING CODE!
      assert( false )
         report "."
         severity failure;
      --
   end process;
   --
end test;
