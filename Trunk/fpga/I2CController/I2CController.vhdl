-- ----------------------------------------------------------------------------
-- Project        PH3 
-- (c) copyright  2016
-- Company        Harman/Becker Automotive Systems GmbH
--                All rights reserved
-- Secrecy Level  STRICTLY CONFIDENTIAL
-- ----------------------------------------------------------------------------
--
-- @file          I2CController.vhdl
-- @ingroup       I2CController
-- @author        Franz Lorenz
--
-- The functionality of the module 'I2CController' is:
--  - Handle the I2C interface with address
--  - Handle a parallel 8bit bus
-- ----------------------------------------------------------------------------

-- ---------------------------------------------------------
-- LIBRARY
-- ---------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ---------------------------------------------------------
-- ENTITY
-- ---------------------------------------------------------
entity I2CController is
generic
(
   g_I2CADDRESS      :  std_logic_vector( 6 downto 0 ) := "1001000";
   g_BUSADDRWIDTH    :  integer  := 6
);
port
(
   CLK_I       : in     std_logic;
   RST_I       : in     std_logic;
   -- I2C interface
   I2C_SCL_I   : in     std_logic;
   I2C_SDA_I   : in     std_logic;
   I2C_SDA_O   : out    std_logic;                          --if this signal is '0', then the I2C_SDA signal must be driven active low!
   -- BUS interface
   BUS_ADDR_O  : out    std_logic_vector( 6 downto 0 );
   BUS_DATA_O  : out    std_logic_vector( 7 downto 0 );
   BUS_DATA_I  : in     std_logic_vector( 7 downto 0 );
   BUS_READ_O  : out    std_logic;
   BUS_WRITE_O : out    std_logic;
   --
   DEBUG_O     : out    std_logic_vector( 15 downto 0 )
);
end I2CController;

-- ---------------------------------------------------------
-- ARCHITECTURE
-- ---------------------------------------------------------
architecture behavioral of I2CController is
   --
   -- signals
   signal busaddr       :  std_logic_vector( g_BUSADDRWIDTH-1 downto 0 );
   signal busread       :  std_logic;
   signal buswrite      :  std_logic;
   signal busdatao      :  std_logic_vector( 7 downto 0 );
   --
   signal i2cscl        :  std_logic_vector( 1 downto 0 );
   signal i2csda        :  std_logic_vector( 1 downto 0 );
   --
   type  tDrvEvent is  (   eventIdle,
                           eventStart, 
                           eventStop,
                           eventXBit,
                           event8Bit,
                           event9Bit
                        );
   signal drvevent      :  tDrvEvent;                       --current driveer state
   signal drvrx         :  std_logic_vector( 10 downto 0 ); --readen byte (7 downto 0)
   signal drvrxack      :  std_logic;                       --acknowledge bit state
   signal drvtx         :  std_logic_vector(  7 downto 0 ); --write byte
   signal drvdoack      :  std_logic;                       --flag to generate an ACK level '0'
   --
   type  tMainStates is (    
                           mainReset, 
                           mainWaitForAddr,
                           mainWrOutputAck,
                           mainWrWaitForRegAddr,
                           mainWrAckForRegAddr,
                           mainWrWaitForRegData,
                           mainWrMemWriteData,
                           mainWrAckForRegData,
                           mainRdMemReadData,
                           mainRdOutputAck,
                           mainRdWaitForByte,
                           mainRdWaitStartStop,
                           mainEnd
                        );
   signal mainState     :  tMainStates;                     --current main state
   signal mainEvent     :  tDrvEvent;                       --current detected drvevent
-- delete    signal mainRWFlag    :  std_logic;                       --current r/w flag of the i2c address
   signal maintx        :  std_logic_vector( 7 downto 0 );  --byte to transmit
   --
begin
   --
   -- ------------------------------------------------------
   -- This process receives a byte from the I2C bus
   -- and sets its state (rxstate) to START, STOP
   -- ------------------------------------------------------
   MAIN : process( CLK_I, RST_I, mainState )
   begin
      --
      if( rising_edge( CLK_I ) )then                        --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            mainState   <= mainReset;                       --  setup reset state 
-- delete            mainRWFlag  <= '0';                             --  reset r/w flag
            mainEvent   <= eventIdle;                       --  setup idle
            maintx      <= ( others => '1' );               --  reset transmit byte buffer
            drvdoack    <= '0';                             --  do not generate an ack pulse
            busaddr     <= ( others => '0' );               --  reset bus address
            busdatao    <= ( others => '0' );               --  reset bus data output
            buswrite    <= '0';                             --  reset bus write signal
            busread     <= '0';                             --  reset bus read signal
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --
            if( mainEvent /= drvEvent )then                 --  event changes?
               mainEvent   <= drvEvent;                     --   yes, then store new event
               if(   ( drvEvent = eventStart ) or           --   is new event START
                     ( drvEvent = eventStop  )     )then    --    or STOP?
                  mainState   <= mainReset;                 --    yes, then reset main state machine
                  maintx      <= ( others => '1' );         --    reset transmit byte buffer
               end if;
            end if;  --if( mainEvent /= drvEvent )then 
            --
            case mainState is
            when  mainReset            => 
               drvdoack <= '0';                             --  generate no ACK signal!
               if( drvevent = eventStart )then  
                  mainState   <= mainWaitForAddr;
               end if;
               --
            when  mainWaitForAddr      => 
               if( drvevent = event8Bit )then               -- the address+r/w flag readen?
                  if( drvrx( 7 downto 1 ) = g_I2CADDRESS )then -- yes, then...
                     drvdoack    <= '1';                    --  generate ACK bit
                     if( drvrx( 0 ) = '0' )then             --  write access?
                        mainState   <= mainWrOutputAck;     --   yes, then change state
                     else                                   --  otherwise read access
                        mainState   <= mainRdMemReadData;   --   then change to read states
                     end if;  --if( drvrx( 0 ) = '0' )then
                  end if;  --if( drvrx... = g_I2CADDRESS )then
               end if;  --if( drvevent... )then
               -- 
            when mainRdMemReadData     =>
               maintx      <= BUS_DATA_I;                   -- get data from memory
               mainState   <= mainRdOutputAck;              -- set new state
               --
            when  mainRdOutputAck      => 
               if( drvevent = event9Bit )then              -- acknowledge bit over?
                  drvdoack    <= '0';                       --  yes, release ack signal
                  if( drvrxack = '0' )then                  --  is ACK from master?
                     mainState   <= mainRdWaitForByte;      --   yes, then change state
                  else                                      --  otherwise
                     mainState   <= mainRdWaitStartStop;    --   then release into reset state
                  end if;  --if( drvrxack = '0' )then
               end if;  --if( drvevent = event9Bit )then
               --
            when mainRdWaitForByte     =>
               if( drvevent = event8Bit )then               -- all the bits out?
                  mainState   <= mainRdMemReadData;         --  yes, then read next byte
                  busaddr     <= busaddr + 1;               --  increment address
               end if;
               --
            when mainRdWaitStartStop   => 
               null;
               --
            when  mainWrOutputAck      => 
               if( drvevent = event9Bit )then               -- acknowledge bit over?
                  drvdoack    <= '0';                       --  yes, release ack signal
                  mainState   <= mainWrWaitForRegAddr;      --  change state
               end if;
               --
            when mainWrWaitForRegAddr  =>
               if( drvevent = event8Bit )then
                  busaddr( g_BUSADDRWIDTH-1 downto 0 ) <= 
                     drvrx( g_BUSADDRWIDTH-1 downto 0 );
                  drvdoack    <= '1';
                  mainState   <= mainWrAckForRegAddr;
               end if;
               --
            when  mainWrAckForRegAddr  => 
               if( drvevent = event9Bit )then               -- acknowledge bit over?
                  drvdoack    <= '0';                       --  yes, release ack signal
                  mainState   <= mainWrWaitForRegData;      --  get the data byte
               end if;
               --
            when mainWrWaitForRegData  =>
               if( drvevent = event8Bit )then               -- all 8 databits readen in?
                  -- HERE A BUS WRITE MUST BE PERFORMED!    --  yes, then...
                  busdatao    <= drvrx( 7 downto 0 );       --  output data byte
                  drvdoack    <= '1';                       --  acknowledge by ACK
                  mainState   <= mainWrMemWriteData;        --  change state
               end if;
               --
            when mainWrMemWriteData    =>
               mainState   <= mainWrAckForRegData;          --  change state
               --
            when mainWrAckForRegData   => 
               if( drvevent = event9Bit )then               -- acknowledge bit over?
                  drvdoack    <= '0';                       --  yes, release ack signal
                  busaddr     <= busaddr + 1;               --  increment address pointer
                  mainState   <= mainWrWaitForRegData;      --  get the next bytes
               end if;
               --
            when others                => 
               mainState   <= mainReset;
            end case;   --case mainState is
            --
         end if; --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_I ) )
      --
      if( mainState = mainWrMemWriteData )then              -- write data state?
         buswrite <= '1';                                   --  yes, then activate write signal
      else                                                  -- otherwise
         buswrite <= '0';                                   --  release/deactivate write signal
      end if;  --if( mainState == mainMemWriteData )then
      --
      if( mainState = mainRdMemReadData )then               -- read data state?
         busread  <= '1';                                   --  yes, then activate read signal
      else                                                  -- otherwise
         busread  <= '0';                                   --  release/deactivate read signal
      end if;  --if( mainState == mainMemWriteData )then
      --
   end process;
   --
   -- ------------------------------------------------------
   -- This process receives a byte from the I2C bus
   -- and sets its state (rxstate) to START, STOP
   -- ------------------------------------------------------
   DRV : process( CLK_I )
   begin
      --
      if( rising_edge( CLK_I ) )then                        --active edge?
         --                                                 -- yes, then...
         if( RST_I = '1' )then                              -- is reset active?
            --                                              --  yes, then...
            drvevent <= eventIdle;                          --  reset state to IDLE
            drvtx    <= ( others => '1' );                  --  reset tx buffer
            drvrxack <= '0';                                --  reset rx ack bit state
            --
         else    --if( RST_I = '1' )then                    -- otherwise
            --                                              --  reset is NOT active
            i2cscl   <= i2cscl(0) & I2C_SCL_I;              --  fetch the clock line
            i2csda   <= i2csda(0) & I2C_SDA_I;              --  fetch the sda input line
            --
            if( i2cscl = "11" )then                         --  clock STABLE HIGH?
               --                                           --   yes, then...
               if( i2csda = "10" )then                      --   START condition detected?
                  --                                        --    yes, then...
                  drvevent <= eventStart;                   --    set event
                  drvrx    <= X"00" & "011";                --    init receive register
                  drvtx    <= ( others => '1' );            --    init transmit register
                  --
               elsif( i2csda = "01" )then                   --   STOP condition detected?
                  --                                        --    yes, then...
                  drvevent <= eventStop;                    --    set event
                  drvtx    <= ( others => '1' );            --    init transmit register
                  --
               end if;
               --
            elsif( i2cscl = "01" )then                      --  clock RISING EDGE?
               --                                           --   yes, then...
               drvrx    <= drvrx( 9 downto 0 ) & i2csda(1); --   shift data bit in receiver
               --
            elsif( i2cscl = "10" )then                      --  clock FALLING EDGE?
               --                                           --   yes, then...
               drvtx <= drvtx( 6 downto 0 ) & '1';          --   shift transmit byte
               --
               if( drvrx( 10 downto 9 ) = "01" )then        --   all 8 bits readen?
                  --                                        --    yes, then...
                  drvevent  <= event8Bit;                   --    signal byte is readen in
                  --
               elsif( drvrx( 10 downto 9 ) = "11" )then     --   9 bits readen?
                  --                                        --    yes, then...
                  drvrxack <= drvrx( 0 );                   --    store acknowledge bit
                  drvrx    <= X"00" & "011";                --    reset byte receive buffer
                  drvevent <= event9Bit;                    --    reset state
                  drvtx    <= maintx( 7 downto 0 );         --    copy the new transmit byte
                  --
               end if;
               --
            end if;  --if( i2cscl... )then
            --
         end if;  --if( RST_I = '1' )
         --
      end if; --if( rising_edge( CLK_I ) )
      --
   end process;
   --
   -- concurrent statements
   --
   BUS_ADDR_O  <= busaddr            when( g_BUSADDRWIDTH = 7 ) else 
                  '0' & busaddr      when( g_BUSADDRWIDTH = 6 ) else 
                  "00" & busaddr     when( g_BUSADDRWIDTH = 5 ) else
                  "000" & busaddr    when( g_BUSADDRWIDTH = 4 ) else
                  "0000" & busaddr   when( g_BUSADDRWIDTH = 3 ) else
                  "00000" & busaddr  when( g_BUSADDRWIDTH = 2 ) else
                  "000000" & busaddr when( g_BUSADDRWIDTH = 1 ) else
                  "0000000";
   BUS_DATA_O  <= busdatao;                                 --reflect output data
   BUS_READ_O  <= busread;                                  --reflect read signal
   BUS_WRITE_O <= buswrite;                                 --reflect write signal
   --
   I2C_SDA_O   <= '0' when(   ( ( drvdoack = '1' ) and ( drvevent = event8Bit )                                          ) or 
                              ( ( drvtx(7) = '0' ) and ( drvevent = event9Bit ) and ( mainState /= mainRdWaitStartStop ) )     )
                  else '1';
-- this works   I2C_SDA_O   <= '0' when( ( drvdoack = '1' ) ) else '1';
   --
   -- ONLY FOR DEBUGGING!
   DEBUG_O( 3 downto 0 )   <= X"0"  when( drvevent = eventIdle  ) else
                              X"1"  when( drvevent = eventStart ) else
                              X"2"  when( drvevent = eventStop  ) else
                              X"3"  when( drvevent = eventXBit  ) else
                              X"4"  when( drvevent = event8Bit  ) else
                              X"5"  when( drvevent = event9Bit  ) else
                              X"F";
   DEBUG_O( 7 downto 4 )   <= X"0"  when( mainState = mainReset            ) else
                              X"1"  when( mainState = mainWaitForAddr      ) else
                              X"2"  when( mainState = mainWrOutputAck      ) else
                              X"3"  when( mainState = mainWrWaitForRegAddr ) else
                              X"4"  when( mainState = mainWrAckForRegAddr  ) else
                              X"5"  when( mainState = mainWrWaitForRegData ) else
                              X"6"  when( mainState = mainWrMemWriteData   ) else
                              X"7"  when( mainState = mainWrAckForRegData  ) else
                              X"8"  when( mainState = mainRdMemReadData    ) else
                              X"9"  when( mainState = mainRdOutputAck      ) else
                              X"A"  when( mainState = mainRdWaitForByte    ) else
                              X"B"  when( mainState = mainRdWaitStartStop  ) else
                              X"F";
   DEBUG_O( 15 downto 8 )  <= ( others => '0' );
   --
end behavioral;
