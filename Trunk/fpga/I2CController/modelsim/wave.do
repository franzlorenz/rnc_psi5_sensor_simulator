onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix binary /tb_i2ccontroller/DUT/CLK_I
add wave -noupdate -radix binary /tb_i2ccontroller/DUT/RST_I
add wave -noupdate -divider {I2C BUS}
add wave -noupdate -radix binary /tb_i2ccontroller/I2C_SCL
add wave -noupdate -radix binary /tb_i2ccontroller/I2C_SDA
add wave -noupdate -divider <NULL>
add wave -noupdate -radix binary /tb_i2ccontroller/DUT/I2C_SCL_I
add wave -noupdate -radix binary /tb_i2ccontroller/DUT/I2C_SDA_I
add wave -noupdate -radix binary /tb_i2ccontroller/DUT/I2C_SDA_O
add wave -noupdate -divider {INTERNAL BUS}
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/BUS_ADDR_O
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/BUS_DATA_O
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/BUS_DATA_I
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/BUS_READ_O
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/BUS_WRITE_O
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/DEBUG_O
add wave -noupdate -divider INTERNALS
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/tRegAddr
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/tRegData
add wave -noupdate -divider {New Divider}
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/txByte
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/busaddr
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/busdatao
add wave -noupdate /tb_i2ccontroller/DUT/busread
add wave -noupdate /tb_i2ccontroller/DUT/i2cscl
add wave -noupdate /tb_i2ccontroller/DUT/i2csda
add wave -noupdate /tb_i2ccontroller/DUT/drvevent
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/drvrx
add wave -noupdate /tb_i2ccontroller/DUT/drvrxack
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/drvtx
add wave -noupdate /tb_i2ccontroller/DUT/drvdoack
add wave -noupdate /tb_i2ccontroller/DUT/mainState
add wave -noupdate -radix binary /tb_i2ccontroller/DUT/mainRWFlag
add wave -noupdate -radix hexadecimal /tb_i2ccontroller/DUT/maintx
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1392280000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2087862551 ps} {2248231445 ps}
