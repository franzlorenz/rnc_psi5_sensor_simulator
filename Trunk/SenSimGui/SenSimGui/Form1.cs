﻿/******************************************************************************
 * Project        SENSOR SIMULATION GUI
 * (c) copyright  2017
 * Company        Harman/Becker Automotive Systems GmbH
 *                All rights reserved
 * Secrecy Level  STRICTLY CONFIDENTIAL
 *****************************************************************************/
/**
 * @file          Form1.cs
 * @ingroup       test
 * @author        Franz Lorenz
 *
 */

/*----------------------------------------------------------
 *  INCLUDES
 *--------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

/*----------------------------------------------------------
 *  CLASSES
 *--------------------------------------------------------*/
namespace SenSimGui
{
   public partial class Form1 : Form
   {

      /**
       * NOTE: Please change this number with EACH release/change
       */
      public string  sVersion = "3.1.0";

      /**
       * The identification string of a simulator setup file.
       * This will be checked when reading a setup file
       */
      public string  sSetupFileId = "RNC SENSOR SIMULATOR SETUP FILE";

      /**
       * This is the global flag, if the GUI is connected
       * to the sensor simulation device.
       */
      public bool    bConnected = false;

      /**
       * This variable defines the number of bytes to read
       * from the device.
       * 8 bytes of the version registers
       * 8 bytes per sensor
       * 4 sensors
       * = 8 + 4 x 8 = 40 bytes
       */
      public byte    bBytesToRead = ( byte )40;

      /**
       * This variable enables/disable the automatic
       * output of the device information to the user.
       */
      public bool    bShowDeviceInfo = false;

      /**
       * The serial receive buffer.
       */
      public List<byte> lbReceiveBuffer = new List<byte>();

      /**
       * The initial directory for file operations
       */
      public string  sFileInitDirectory = "";

      /**
       * The constructor of the windows form.
       */
      public Form1()
      {
         InitializeComponent();                             //initialize GUI elements
         sFileInitDirectory = System.IO.Path.GetDirectoryName( Application.ExecutablePath );
         this.Text += " - v" + sVersion;                    //add version to window title
         ChangeConnection();                                //update connection status
         appfileRead();                                     //read previous configuration
      }

      /**
       * This function will disable/enable all gui elements
       * inside a TabPage.
       * @param   Page     tabpage to enable/disable
       * @param   bEnable  true = all gui elements enable
       *                   false = all gui elements disable
       */
      private void EnableTab( TabPage Page, bool bEnable ) 
      {
         foreach( Control ctl in Page.Controls )            //run through all gui elements
         {                                                  // then...
            ctl.Enabled = bEnable;                          // update the enable property
         }
      }

      /**
       * This function must be called, if the status of connection
       * changed from connected->disconnected or vice versa.
       * The current state is stored in bConnected.
       */
      private void ChangeConnection()
      {
         if( bConnected )                                   //connected?
         {                                                  // yes, then...
            buttonConn.Text = "Disconnect";                 // rename the button
            buttonConn.BackColor = Color.FromArgb( 15, Color.Red );
            buttonConnUpdate.Enabled = false;
            listBoxConnInterface.Enabled = false;
         }
         else
         {
            buttonConn.Text = "Connect";                    // rename the button
            buttonConn.BackColor = Color.FromArgb( 15, Color.Green );
            buttonConnUpdate.Enabled = true;
            listBoxConnInterface.Enabled = true;
         }
         //
         int iIdx = listBoxConnInterface.SelectedIndex;     //is an interface
         if( iIdx >= 0 )                                    // selected?
         {                                                  // yes, then...
            buttonConn.Visible = true;                      // make button visible
         }
         else                                               //if no interface
         {                                                  // is selected
            buttonConn.Visible = false;                     // hide the button
         }
         //
         EnableTab( tabSensorData, bConnected );            //enable/disable tab version 
         EnableTab( tabDebug, bConnected );                 //enable/disable tab debug
         EnableTab( tabSensorType, bConnected );            //enable/disable tab node1
         EnableTab(tabDevInfo, bConnected );                //enable/disable tab device
         EnableTab( tabSetup, bConnected );                 //enable/disable tab setup file load/save
      }

      /**
       * This function update the list of all known
       * serial connections.
       */
      private void updateInterfaceList()
      {
         listBoxConnInterface.Items.Clear();
         foreach( string s in SerialPort.GetPortNames() )
         {
            listBoxConnInterface.Items.Add( s );
         }  
         ChangeConnection();
      }

      /*****************************************************
       * CONNECTION TAB
       * **************************************************/

      /**
       * This function is called, if the connection
       * page is shown.
       */
      private void tabConn_Enter( object sender, EventArgs e )
      {
         if( ! bConnected )
         {
            if( listBoxConnInterface.Items.Count == 0 )
            {
               updateInterfaceList();
            }
         }
         ChangeConnection();
      }

      /**
       * This function is called, if the user clicks on the 
       * buttton "Update Interface List" to see all new interfaces.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonConnUpdate_Click( object sender, EventArgs e )
      {
         if( ! serialPort.IsOpen )
         {
            updateInterfaceList();
            ChangeConnection();
         }
      }

      /**
       * This function is called, if the user wants to
       * connect or disconnect the current serial interface.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonConn_Click( object sender, EventArgs e )
      {
         if( ! bConnected )                                 //already connected to serial?
         {                                                  // no, then...
            int iIdx = listBoxConnInterface.SelectedIndex;  // get index
            string sPort = listBoxConnInterface.Items[iIdx].ToString(); // get port name
            try
            {
               serialPort.PortName = sPort;                 // set port name
               serialPort.ReadBufferSize = bBytesToRead;    // setup buffer size
               serialPort.WriteBufferSize = 4;              //  ...
               serialPort.Open();                           // open the port
               bConnected = true;                           // set flag
               ChangeConnection();                          // update the GUI
               bShowDeviceInfo = true;                      // show the device information
               serialSend( "A\x00R\x28" );                  // read all registers
            }
            catch( Exception ex )
            {
               MessageBox.Show( "The serial port '"+sPort+"' can not open", 
                                 "Serial Port Exception '"+ex.Message+"'",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Error );
            }
         }
         else
         {
            serialPort.Close();
            bConnected = false;
            ChangeConnection();
         }
      }
      
      /**
       * This function is called, if the user selects an
       * interface device (USB).
       * @param   sender   sender
       * @param   e        event
       */
      private void listBoxConnInterface_SelectedIndexChanged( object sender, EventArgs e )
      {
         ChangeConnection();
      }

      /**
       * This function is called, if the user closes the application.
       * @param   sender   sender
       * @param   e        event
       */
      private void Form1_FormClosed( object sender, FormClosedEventArgs e )
      {
         appfileWrite();                                    //write global settings
         if( serialPort.IsOpen )                            //is serial port open?
         {                                                  // yes, then...
            serialPort.Close();                             // close before terminating...
            ChangeConnection();                             // update GUI
         }
      }

      /*****************************************************
       * DEBUG TAB
       * **************************************************/

      /**
       * This function is called, if the user presses a key in
       * the textbox command line.
       * @param   sender   sender
       * @param   e        event
       */
      private void textBoxDebugCommand_KeyPress( object sender, KeyPressEventArgs e )
      {
         //if( e.KeyChar == '\r' )                            //key <RETURN> pressed?
         //{                                                  // yes, then...
         //   serialSend( textBoxDebugCommand.Text );
         //}
      }

      /*****************************************************
       * SERIAL FUNCTIONALITIES
       * **************************************************/

      /**
       * This function will write a string out to the 
       * serial port and echo it in the debug view.
       * @param   sSend[]     string to write to serial port
       */
      private void serialSend( string sSend )
      {
         if( serialPort.IsOpen )                            //is serial port open?
         {                                                  // yes, then...
            serialPort.NewLine = "\x00";                    // setup new-line character
            lbReceiveBuffer.Clear();                        // clear the receive buffer
            serialPort.WriteLine( sSend );                  // send data via serial port
         }
         else                                               //otherwise
         {                                                  // serial port is closed!
         }
         richTextBoxDebug.AppendText( "\n" );               //output newline
         richTextBoxDebug.ScrollToCaret();                  //update cursor
      }

      /**
       * This function will be called, if the serial
       * port receives any data.
       * @param   sender   sender
       * @param   e        event
       */
      private void serialPort_DataReceived( object sender, SerialDataReceivedEventArgs e )
      {
         int iLen = serialPort.BytesToRead;                 //get number of bytes in the receive buffer
         byte[] bBuffer = new byte[iLen];                   //allocate buffer
         serialPort.Read( bBuffer, 0, iLen );               //read the bytes from the serial port
         //
         for( int iCtr=0; iCtr < iLen; iCtr++ )             //copy all received
         {                                                  // byte into the
            lbReceiveBuffer.Add( bBuffer[iCtr] );           // global receive buffer
         }
         iLen = lbReceiveBuffer.Count();                    //get the number of bytes of the global receive buffer
         //
         if( iLen >= ( int )bBytesToRead )                  //whole answer received from device?
         {                                                  // yes, then...
            richTextBoxDebug.Invoke(                        // invoke the processing!
               ( MethodInvoker ) delegate 
               {
                  richTextBoxDebug.AppendText( "> " + 
                     BitConverter.ToString( lbReceiveBuffer.ToArray() ) + "\n" );
                  richTextBoxDebug.ScrollToCaret();
                  routeUpdate( lbReceiveBuffer );
               }
            );
         }  //if( iLen >= ... )
      }

      /**
       * This function is called, if the user wants to clear the
       * current output view.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonDebugClear_Click( object sender, EventArgs e )
      {
         richTextBoxDebug.Clear();
      }

      /**
       * This function will be called, if the serial port
       * receives any error.
       * @param   sender   sender
       * @param   e        event
       */
      private void serialPort_ErrorReceived( object sender, SerialErrorReceivedEventArgs e )
      {
         MessageBox.Show(  e.ToString(), 
                           "Serial Port Exception",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error    );
      }

      /**
       * This function sends the sensor type
       * @param   iNode       node number [0..3]
       * @param   iAnalogIn   used analog input [0=ain0/1, 1=ain2/3..]
       */
      private void serialSend_AnalogIn( int iNode, int iAnalogIn )
      {
      }

      /**
       * This function sends the 16 bit value
       * @param   iNode       node number [0..3]
       * @param   bLeft       
       * @param   iData       16 bit value
       */
      private void serialSend_Value( int iNode, bool bLeft, int iValue )
      {
         int iAddr = 0;
         iAddr  = bLeft.CompareTo( false ) + 1;
         iAddr *= 2;
         iAddr += 0x03;
         //
         //serialSend( string.Format( "iw{0:X2}{1:X2}{2:X4}", //command
         //            iNode + iI2CFirstNodeAddr,             // i2c address
         //            iAddr,                                 // register address
         //            ( iValue & 0xFFFF )              ) );  // data
      }

      /**
       * This function sends a register read command to the 
       * device. When the answer is send, then the whole
       * GUI will be updated.
       */
      private void guiUpdate()
      {
         string sSend = string.Format( "A\x00R{0}", ( Char )bBytesToRead );
         serialSend( sSend );                               //send "set address to 0, read bytes
      }


      /*****************************************************
       * SENSOR DATA ROUTING
       * **************************************************/

      /**
       * This function will be called, if the user wants to route the
       * analogue input to the PSI5 node.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButtonRoute1AIN01_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute1AIN23_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute1AIN45_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute1AIN67_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute1GEN0_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }
      private void radioButtonRoute2AIN01_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute2AIN23_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute2AIN45_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute2AIN67_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute2GEN0_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }
      private void radioButtonRoute3AIN01_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute3AIN23_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute3AIN45_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute3AIN67_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute3GEN0_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }
      private void radioButtonRoute4AIN01_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute4AIN23_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute4AIN45_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute4AIN67_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }
      private void radioButtonRoute4GEN0_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user wants to route the
       * analogue input to the PSI5 node.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButtonRoute1Const_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user wants to route the
       * analogue input to the PSI5 node.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButtonRoute2Const_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user wants to route the
       * analogue input to the PSI5 node.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButtonRoute3Const_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user wants to route the
       * analogue input to the PSI5 node.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButtonRoute4Const_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function returns the byte
       * @param   lRouteButtons  buttons
       * @return  byte           return
       */
      private byte getRouteByte( List<RadioButton> lRouteButtons )
      {
         int   iCtr = 0;
         byte  bRet = 0xFF;                                 //set default return value
         //
         for( iCtr=0; iCtr < lRouteButtons.Count; iCtr++ )  //scan all radiobuttons
         {                                                  // then...
            if( true == lRouteButtons[iCtr].Checked )       // is radiobutton checked?
            {                                               //  yes, then...
               bRet = ( byte )iCtr;                         //  setup return
               break;                                       //  break for() loop
            }
         }  //for()
         //
         return bRet;                                       //return byte
      }

      /**
       * This function writes the value
       * @param   Ctrl     numericupdown gui element
       * @param   pbData   data
       */
      private void getValue( NumericUpDown Ctrl, ref byte bHigh, ref byte bLow )
      {
         int iVal = ( int )Ctrl.Value;                      //get the value
         bHigh = ( byte )( iVal / 256 );                    //write the high byte
         bLow  = ( byte )( iVal % 256 );                    //write low byte
      }

      /**
       * This function will update a routing radiobuttons
       * of one node.
       * @param   Buttons  list of the node radiobuttons
       * @param   bSet     byte that reflects the settings
       */
      private void routeUpdateRadioButton( List<RadioButton> Buttons, byte bSet, int iDefault = -1 )
      {
         Color cBack = Form1.DefaultBackColor;
         //
         for( int iCtr=0; iCtr < Buttons.Count; iCtr++ )    //reset all
         {                                                  // the colors
            Buttons[iCtr].BackColor = cBack;
         }
         //
         cBack = Color.FromArgb( cBack.A, 
                                 cBack.R, 
                                 ( byte )Math.Min( cBack.G+100, 255 ), 
                                 cBack.B );
         if( bSet >= Buttons.Count )                        //value too big?
         {                                                  // yes, then...
            bSet = ( byte )( Buttons.Count - 1 );           // limit to valid range
         }
         Buttons[bSet].Checked = true;
         Buttons[bSet].BackColor = cBack;
      }

      /**
       * This function sets the background color regarding of
       * the parameter bSet of the labels given by Labels.
       * @param   Labels   list of all labels
       * @param   bSet     byte for evaluation
       */
      private void allUpdateLabelColor( List<Label> Labels, byte bSet )
      {
         Color cColor = Color.Black;                        //set default color
         if( 0 != bSet )                                    //is active?
         {                                                  // yes, then...
            cColor = Color.LightGreen;                      // set "active" color
         }
         //
         for( int iCtr=0; iCtr < Labels.Count; iCtr++ )     //step through all labels
         {                                                  // then...
            Labels[iCtr].BackColor = cColor;                // set the background color
         }
      }

      /**
       * This function will update a numericUpDown GUI element
       * due to the value stored in bHigh and bLow.
       * @param   Const    numeric GUI element
       * @param   bHigh    high byte of the new value
       * @param   bLow     low byte of the new value
       */
      private void routeUpdateConst( NumericUpDown Const, byte bHigh, byte bLow )
      {
         Const.Value = ( decimal )bHigh * 256 + ( decimal )bLow;
      }

      /**
       * This function updates the whole route page of the GUI
       * with the readen data from the hardware device.
       * @param   bRegisters        read registers from the device
       *          [0..7]            version
       *          [8]               sensor type
       *          [9]               input selector
       *          [15]
       */
      private void routeUpdate( List<byte> lbRegisters, bool bFromFile = false )
      {
         List<RadioButton> Route1 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute1AIN01, 
                                       radioButtonRoute1AIN23,
                                       radioButtonRoute1AIN45, 
                                       radioButtonRoute1AIN67,
                                       radioButtonRoute1GEN0,
                                       radioButtonRoute1GEN1,
                                       radioButtonRoute1GEN2,
                                       radioButtonRoute1GEN3,
                                       radioButtonRoute1Const
                                    };
         List<RadioButton> Sens1 = new List<RadioButton>  
                                    { 
                                       radioButton1SensorOFF,
                                       radioButton1SensorF1F2, 
                                       radioButton1SensorF3, 
                                       radioButton1SensorRNS20
                                    };
         //
         List<RadioButton> Route2 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute2AIN01, 
                                       radioButtonRoute2AIN23,
                                       radioButtonRoute2AIN45, 
                                       radioButtonRoute2AIN67,
                                       radioButtonRoute2GEN0,
                                       radioButtonRoute2GEN1,
                                       radioButtonRoute2GEN2,
                                       radioButtonRoute2GEN3,
                                       radioButtonRoute2Const
                                    };
         List<RadioButton> Sens2 = new List<RadioButton>  
                                    { 
                                       radioButton2SensorOFF,
                                       radioButton2SensorF1F2, 
                                       radioButton2SensorF3, 
                                       radioButton2SensorRNS20
                                    };
         //
         List<RadioButton> Route3 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute3AIN01, 
                                       radioButtonRoute3AIN23,
                                       radioButtonRoute3AIN45, 
                                       radioButtonRoute3AIN67,
                                       radioButtonRoute3GEN0,
                                       radioButtonRoute3GEN1,
                                       radioButtonRoute3GEN2,
                                       radioButtonRoute3GEN3,
                                       radioButtonRoute3Const
                                    };
         List<RadioButton> Sens3 = new List<RadioButton>  
                                    { 
                                       radioButton3SensorOFF,
                                       radioButton3SensorF1F2, 
                                       radioButton3SensorF3, 
                                       radioButton3SensorRNS20
                                    };
         //
         List<RadioButton> Route4 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute4AIN01, 
                                       radioButtonRoute4AIN23,
                                       radioButtonRoute4AIN45, 
                                       radioButtonRoute4AIN67,
                                       radioButtonRoute4GEN0,
                                       radioButtonRoute4GEN1,
                                       radioButtonRoute4GEN2,
                                       radioButtonRoute4GEN3,
                                       radioButtonRoute4Const
                                    };
         List<RadioButton> Sens4 = new List<RadioButton>  
                                    { 
                                       radioButton4SensorOFF,
                                       radioButton4SensorF1F2, 
                                       radioButton4SensorF3, 
                                       radioButton4SensorRNS20
                                    };
         //
         List<NumericUpDown> Consts = new List<NumericUpDown>
                                    {
                                       numericUpDownRoute1Chl1,
                                       numericUpDownRoute1Chl2,
                                       numericUpDownRoute2Chl1,
                                       numericUpDownRoute2Chl2,
                                       numericUpDownRoute3Chl1,
                                       numericUpDownRoute3Chl2,
                                       numericUpDownRoute4Chl1,
                                       numericUpDownRoute4Chl2
                                    };
         //
         List<Label> StatusSens1 = new List<Label>
                                    {
                                       labelTypeSens1,
                                       labelDataSens1,
                                       labelDevSens1
                                    };
         //
         List<Label> StatusSens2 = new List<Label>
                                    {
                                       labelTypeSens2,
                                       labelDataSens2,
                                       labelDevSens2
                                    };
         //
         List<Label> StatusSens3 = new List<Label>
                                    {
                                       labelTypeSens3,
                                       labelDataSens3,
                                       labelDevSens3
                                    };
         //
         List<Label> StatusSens4 = new List<Label>
                                    {
                                       labelTypeSens4,
                                       labelDataSens4,
                                       labelDevSens4
                                    };
         //
         int iBaseAddr = 8;
         if( bFromFile )                                    //data read from file?
         {                                                  // yes, then...
            iBaseAddr = 0;                                  // no version data available
         }
         routeUpdateRadioButton( Sens1, lbRegisters[iBaseAddr+0] );
         routeUpdateRadioButton( Route1, ( byte )( lbRegisters[iBaseAddr+1] & 0x0F ) );
         allUpdateLabelColor( StatusSens1, ( byte )( lbRegisters[iBaseAddr+1] & 0x80 ) );
         routeUpdateConst( Consts[0], lbRegisters[iBaseAddr+2], lbRegisters[iBaseAddr+3] );
         routeUpdateConst( Consts[1], lbRegisters[iBaseAddr+4], lbRegisters[iBaseAddr+5] );
         //
         iBaseAddr += 8;
         routeUpdateRadioButton( Sens2, lbRegisters[iBaseAddr+0] );
         routeUpdateRadioButton( Route2, ( byte )( lbRegisters[iBaseAddr+1] & 0x0F ) );
         allUpdateLabelColor( StatusSens2, ( byte )( lbRegisters[iBaseAddr+1] & 0x80 ) );
         routeUpdateConst( Consts[2], lbRegisters[iBaseAddr+2], lbRegisters[iBaseAddr+3] );
         routeUpdateConst( Consts[3], lbRegisters[iBaseAddr+4], lbRegisters[iBaseAddr+5] );
         //
         iBaseAddr += 8;
         routeUpdateRadioButton( Sens3, lbRegisters[iBaseAddr+0] );
         allUpdateLabelColor( StatusSens3, ( byte )( lbRegisters[iBaseAddr+1] & 0x80 ) );
         routeUpdateRadioButton( Route3, ( byte )( lbRegisters[iBaseAddr+1] & 0x0F ) );
         routeUpdateConst( Consts[4], lbRegisters[iBaseAddr+2], lbRegisters[iBaseAddr+3] );
         routeUpdateConst( Consts[5], lbRegisters[iBaseAddr+4], lbRegisters[iBaseAddr+5] );
         //
         iBaseAddr += 8;
         routeUpdateRadioButton( Sens4, lbRegisters[iBaseAddr+0] );
         allUpdateLabelColor( StatusSens4, ( byte )( lbRegisters[iBaseAddr+1] & 0x80 ) );
         routeUpdateRadioButton( Route4, ( byte )( lbRegisters[iBaseAddr+1] & 0x0F ) );
         routeUpdateConst( Consts[6], lbRegisters[iBaseAddr+2], lbRegisters[iBaseAddr+3] );
         routeUpdateConst( Consts[7], lbRegisters[iBaseAddr+4], lbRegisters[iBaseAddr+5] );
         //
         if( ! bFromFile )                                  //data read from device?
         {                                                  // yes, then...
            string sName = "";
            string sVersion = "";
            //
            sName   += Convert.ToChar( lbRegisters[0] );
            sName   += Convert.ToChar( lbRegisters[1] );
            sName   += Convert.ToChar( lbRegisters[2] );
            sName   += Convert.ToChar( lbRegisters[3] );
            sVersion = string.Format( "{0:X2}.{1:X2}.{2:X2} {3:X2}", 
                        lbRegisters[4], lbRegisters[5], 
                        lbRegisters[6], lbRegisters[7]        );
            labelDeviceVersion.Text = "Device Name: '"+sName+"'  Version Numbers: '"+sVersion+"'";
            //
            if( true == bShowDeviceInfo )                   // should user see the version?
            {                                               //  yes, then...
               MessageBox.Show( "Device Name: "+sName+"\r\nVersion: "+sVersion,
                                 "Device Information",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information
                              );
               bShowDeviceInfo = false;                        // disable more outputs
            }  //if( true == bShowDeviceInfo )
         }  //if( ! bFromFile )
      }

      /**
       * This function send the current GUI settings to the 
       * hardware.
       */
      private List<byte> routeSendToHardware( bool bSendToHardware = true )
      {
         List<RadioButton> Route1 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute1AIN01, 
                                       radioButtonRoute1AIN23,
                                       radioButtonRoute1AIN45, 
                                       radioButtonRoute1AIN67,
                                       radioButtonRoute1GEN0,
                                       radioButtonRoute1GEN1,
                                       radioButtonRoute1GEN2,
                                       radioButtonRoute1GEN3,
                                       radioButtonRoute1Const
                                    };
         List<RadioButton> Sens1 = new List<RadioButton>  
                                    { 
                                       radioButton1SensorOFF,
                                       radioButton1SensorF1F2, 
                                       radioButton1SensorF3, 
                                       radioButton1SensorRNS20
                                    };
         //
         List<RadioButton> Route2 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute2AIN01, 
                                       radioButtonRoute2AIN23,
                                       radioButtonRoute2AIN45, 
                                       radioButtonRoute2AIN67,
                                       radioButtonRoute2GEN0,
                                       radioButtonRoute2GEN1,
                                       radioButtonRoute2GEN2,
                                       radioButtonRoute2GEN3,
                                       radioButtonRoute2Const
                                    };
         List<RadioButton> Sens2 = new List<RadioButton>  
                                    { 
                                       radioButton2SensorOFF,
                                       radioButton2SensorF1F2, 
                                       radioButton2SensorF3, 
                                       radioButton2SensorRNS20
                                    };
         //
         List<RadioButton> Route3 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute3AIN01, 
                                       radioButtonRoute3AIN23,
                                       radioButtonRoute3AIN45, 
                                       radioButtonRoute3AIN67,
                                       radioButtonRoute3GEN0,
                                       radioButtonRoute3GEN1,
                                       radioButtonRoute3GEN2,
                                       radioButtonRoute3GEN3,
                                       radioButtonRoute3Const
                                    };
         List<RadioButton> Sens3 = new List<RadioButton>  
                                    { 
                                       radioButton3SensorOFF,
                                       radioButton3SensorF1F2, 
                                       radioButton3SensorF3, 
                                       radioButton3SensorRNS20
                                    };
         //
         List<RadioButton> Route4 = new List<RadioButton> 
                                    { 
                                       radioButtonRoute4AIN01, 
                                       radioButtonRoute4AIN23,
                                       radioButtonRoute4AIN45, 
                                       radioButtonRoute4AIN67,
                                       radioButtonRoute4GEN0,
                                       radioButtonRoute4GEN1,
                                       radioButtonRoute4GEN2,
                                       radioButtonRoute4GEN3,
                                       radioButtonRoute4Const
                                    };
         List<RadioButton> Sens4 = new List<RadioButton>  
                                    { 
                                       radioButton4SensorOFF,
                                       radioButton4SensorF1F2, 
                                       radioButton4SensorF3, 
                                       radioButton4SensorRNS20
                                    };
         //
         List<NumericUpDown> Consts = new List<NumericUpDown>
                                    {
                                       numericUpDownRoute1Chl1,
                                       numericUpDownRoute1Chl2,
                                       numericUpDownRoute2Chl1,
                                       numericUpDownRoute2Chl2,
                                       numericUpDownRoute3Chl1,
                                       numericUpDownRoute3Chl2,
                                       numericUpDownRoute4Chl1,
                                       numericUpDownRoute4Chl2
                                    };
         //
         byte[]      abTelegram  = new byte[32+4];
         List<byte>  lbTelegram;
         int         iBaseAddr = 0;
         //
         for( int iCtr=0; iCtr < abTelegram.Length; iCtr++ )
         {
            abTelegram[iCtr] = 0x00;
         }
         abTelegram[0] = ( byte )'A';
         abTelegram[1] = 0x08;
         abTelegram[2] = ( byte )'W';
         abTelegram[3] = 0x08*4;
         //
         iBaseAddr = 4;
         abTelegram[iBaseAddr+0] = getRouteByte( Sens1 );   //get sensor type
         abTelegram[iBaseAddr+1] = getRouteByte( Route1 );  //get the routing byte
         getValue( Consts[0], ref abTelegram[iBaseAddr+2], 
                              ref abTelegram[iBaseAddr+3] );
         getValue( Consts[1], ref abTelegram[iBaseAddr+4], 
                              ref abTelegram[iBaseAddr+5] );
         //
         iBaseAddr += 8;
         abTelegram[iBaseAddr+0] = getRouteByte( Sens2 );   //get sensor type
         abTelegram[iBaseAddr+1] = getRouteByte( Route2 );  //get the routing byte
         getValue( Consts[2], ref abTelegram[iBaseAddr+2], 
                              ref abTelegram[iBaseAddr+3] );
         getValue( Consts[3], ref abTelegram[iBaseAddr+4], 
                              ref abTelegram[iBaseAddr+5] );
         //
         iBaseAddr += 8;
         abTelegram[iBaseAddr+0] = getRouteByte( Sens3 );   //get sensor type
         abTelegram[iBaseAddr+1] = getRouteByte( Route3 );  //get the routing byte
         getValue( Consts[4], ref abTelegram[iBaseAddr+2], 
                              ref abTelegram[iBaseAddr+3] );
         getValue( Consts[5], ref abTelegram[iBaseAddr+4], 
                              ref abTelegram[iBaseAddr+5] );
         //
         iBaseAddr += 8;
         abTelegram[iBaseAddr+0] = getRouteByte( Sens4 );   //get sensor type
         abTelegram[iBaseAddr+1] = getRouteByte( Route4 );  //get the routing byte
         getValue( Consts[6], ref abTelegram[iBaseAddr+2], 
                              ref abTelegram[iBaseAddr+3] );
         getValue( Consts[7], ref abTelegram[iBaseAddr+4], 
                              ref abTelegram[iBaseAddr+5] );
         //
         if( false != bSendToHardware )                     //update the device?
         {                                                  // yes, then...
            serialPort.Write( abTelegram, 0, abTelegram.Length );
         }
         //
         lbTelegram = new List<byte>( abTelegram );
         return lbTelegram;                                 //return telegram (used for setup file!)
      }

      /**
       * This function updates all values of the GUI.
       */
      private void buttonVerRead_Click( object sender, EventArgs e )
      {
         guiUpdate();                                       //update the whole GUI
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #1 and the FIRST channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute1Chl1_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #1 and the SECOND channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute1Chl2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #2 and the FIRST channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute2Chl1_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #2 and the SECOND channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute2Chl2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #3 and the FIRST channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute3Chl1_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #3 and the SECOND channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute3Chl2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #4 and the FIRST channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute4Chl1_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function is called, if the user changes the 
       * CONSTANT value of SENSOR #4 and the SECOND channel.
       * @param   sender   sender
       * @param   e        event
       */
      private void numericUpDownRoute4Chl2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }


      /*************************************************************************************************************************
       * PSI5 NODE 1
       * **************************************************/


      /**
       * This function will be called, if the user changes the type of
       * simulated sensor OFF.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton1SensorOFF_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F1/F2.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton1SensorF1F2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F3.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton1SensorF3_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch RNS 2.0.
       * @param   sender   sender
       * @param   e        event
       */

      private void radioButton1SensorRNS20_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user wants to update
       * the whole GUI.
       * @param   sender   sender
       * @param   e        event
       */
      private void button1Update_Click( object sender, EventArgs e )
      {
         guiUpdate();                                       //update the whole GUI
      }

      /**
       * This function will be called, if the user changes the left value.
       * @param   sender   sender
       * @param   e        event
       */
      private void textBox1ValLeft_KeyPress( object sender, KeyPressEventArgs e )
      {
         if( e.KeyChar == ( char )Keys.Return )             //return key pressed?
         {                                                  // yes, then...
            serialSend_Value( 0, true, 0xAA55 );            // send value to device
         }
      }

      /**
       * This function will be called, if the user changes the right value.
       * @param   sender   sender
       * @param   e        event
       */
      private void textBox1ValRight_KeyPress( object sender, KeyPressEventArgs e )
      {
         if( e.KeyChar == ( char )Keys.Return )             //return key pressed?
         {                                                  // yes, then...
            serialSend_Value( 0, false, 0xAA55 );           // send value to device
         }
      }

      /*************************************************************************************************************************
       * PSI5 NODE 2
       * **************************************************/

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor OFF.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton2SensorOFF_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F1/F2.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton2SensorF1F2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F3.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton2SensorF3_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch RNS 2.0.
       * @param   sender   sender
       * @param   e        event
       */

      private void radioButton2SensorRNS20_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the serial port
       * receives any error.
       * @param   sender   sender
       * @param   e        event
       */
      private void button2Update_Click( object sender, EventArgs e )
      {
         //int iNode = 1;
         //serialSend( string.Format( "ir{0:X2}{1:X2}{2:X2}", //command I2C read
         //            iNode + iI2CFirstNodeAddr,             // i2c address
         //            0x00,                                  // register start-address
         //            0x10 ) );                              // length in bytes
      }


      /*************************************************************************************************************************
       * PSI5 NODE 3
       * **************************************************/

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor OFF.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton3SensorOFF_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F1/F2.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton3SensorF1F2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F3.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton3SensorF3_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch RNS 2.0.
       * @param   sender   sender
       * @param   e        event
       */

      private void radioButton3SensorRNS20_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the serial port
       * receives any error.
       * @param   sender   sender
       * @param   e        event
       */
      private void button3Update_Click( object sender, EventArgs e )
      {
         //int iNode = 2;
         //serialSend( string.Format( "ir{0:X2}{1:X2}{2:X2}", //command I2C read
         //            iNode + iI2CFirstNodeAddr,             // i2c address
         //            0x00,                                  // register start-address
         //            0x10 ) );                              // length in bytes
      }


      /*************************************************************************************************************************
       * PSI5 NODE 4
       * **************************************************/

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor OFF.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton4SensorOFF_Click(object sender, EventArgs e)
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F1/F2.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton4SensorF1F2_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch F3.
       * @param   sender   sender
       * @param   e        event
       */
      private void radioButton4SensorF3_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the user changes the type of
       * simulated sensor to Bosch RNS 2.0.
       * @param   sender   sender
       * @param   e        event
       */

      private void radioButton4SensorRNS20_Click( object sender, EventArgs e )
      {
         routeSendToHardware();
      }

      /**
       * This function will be called, if the serial port
       * receives any error.
       * @param   sender   sender
       * @param   e        event
       */
      private void button4Update_Click( object sender, EventArgs e )
      {
         //int iNode = 3;
         //serialSend( string.Format( "ir{0:X2}{1:X2}{2:X2}", //command I2C read
         //            iNode + iI2CFirstNodeAddr,             // i2c address
         //            0x00,                                  // register start-address
         //            0x10 ) );                              // length in bytes
      }


      /*************************************************************************************************************************
       * SETUP FILE TAB
       * **************************************************/

      /**
       * This function returns the name of the GUI settings file.
       * @return  string      full qualified filename
       */
      private string appfileName()
      {
         string sAppFile = System.IO.Path.GetDirectoryName( Application.ExecutablePath );
         sAppFile += "\\" + Application.ProductName + ".conf";
         return sAppFile;
      }

      /**
       * This function writes the application file
       */
      private void appfileWrite()
      {
         string sAppFile = appfileName();
         string sContent = "";
         for( int iIdx=0; iIdx < listBoxSetFiles.Items.Count; iIdx++ )
         {
            sContent += listBoxSetFiles.Items[iIdx] + "\n";
         }
         try
         {
            System.IO.File.WriteAllText( sAppFile, sContent );
         }
         catch( Exception ex )
         {
            // it doesn't matter...
         }
      }

      /**
       * This function reads the application file
       */
      private void appfileRead()
      {
         string sAppFile = appfileName();
         List<string>   lsContent = null;
         try
         {
            lsContent = System.IO.File.ReadAllLines( sAppFile ).ToList();
         }
         catch( Exception ex )
         {
         }
         //
         if( lsContent != null )                            //any data read?
         {                                                  // yes, then...
            listBoxSetFiles.Items.Clear();                  // clear previous content
            for( int iIdx=0; iIdx <lsContent.Count; iIdx++ )// read line by line
            {                                               //  then...
               listBoxSetFiles.Items.Add( lsContent[iIdx] );//  add line-per-line
            }
         }
      }

      /**
       * This function will add the filename sFile to
       * the listbox listBoxSetFiles.
       * @param   sFile    filename to add
       */
      private void listboxSetFilesAdd( string sFile )
      {
         bool  bFound = false;                              //set flag to "not found"
         int   iIdx = 0;
         //
         for( iIdx=0; iIdx < listBoxSetFiles.Items.Count; iIdx++ )   //step through all items
         {                                                  // then...
            if( listBoxSetFiles.Items[iIdx].ToString() == sFile )      // is file already found?
            {                                               //  yes, then...
               bFound = true;                               //  set found flag
               break;                                       //  terminate the for() loop
            }
         }  //for( iIdx... )
         //
         if( false == bFound )                              //was file NOT in the list?
         {                                                  // yes, then...
            listBoxSetFiles.Items.Add( sFile );             // add file to list
         }
      }

      /**
       * This function will write the settings file
       * to the disk.
       * @param   sFile       fully qualified filename
       * @param   lbData      list of bytes
       * @return  bool        true = file stored
       *                      false = file not stored
       */
      private bool setfileWrite( string sFile, List<byte> lbTelegram )
      {
         bool     bStored = false;
         string   sContent = "";
         //
         sContent += sSetupFileId+"\nGUI VERSION "+sVersion+"\nDATA "; //write id and version
         for( int iIdx=0; iIdx < lbTelegram.Count; iIdx++ )
         {
            sContent += string.Format( "{0:X2}", lbTelegram[iIdx] );
         }
         //
         try
         {
            System.IO.File.WriteAllText( sFile, sContent );
            bStored = true;
         }
         catch( Exception ex )
         {
            bStored = false;
         }
         return bStored;
      }

      /**
       * This function reads a setfile sFile in.
       * @return  List<byte>     null = setfile read error
       *                         ...  = readen data
       */
      private List<byte> setfileRead( string sFile )
      {
         List<byte>     lbRet = null;
         List<string>   lsContent = null;
         int            iData = 0;
         //
         try
         {
            string sContent = System.IO.File.ReadAllText( sFile );
            lsContent = sContent.Split( '\n' ).ToList();
         }
         catch( Exception ex )
         {
         }
         //
         if( null != lsContent )                            //is file content available?
         {                                                  // yes, then...
            if( lsContent[0] != sSetupFileId )
            {
               lsContent = null;
            }
         }
         //
         if( null != lsContent )                            //is file content available?
         {                                                  // yes, then...
            if( 0 == lsContent[2].IndexOf( "DATA" ) )       // data line found?
            {                                               //  yes, then...
               lbRet = new List<byte>();                    //  create empty bytelist
               for( iData=5; iData < lsContent[2].Length; iData += 2 )
               {
                  lbRet.Add( Convert.ToByte( lsContent[2].Substring( iData, 2 ), 16 ) );
               }
               //
               MessageBox.Show( "File '"+sFile+"' loaded", 
                  "Setup File", 
                  MessageBoxButtons.OK, 
                  MessageBoxIcon.Information );
            }
         }
         //
         return lbRet;
      }

      /**
       * This function is called, if the user wants to save
       * the current settings in a setup file.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonSetSave_Click( object sender, EventArgs e) 
      {
         SaveFileDialog Dlg = new SaveFileDialog();         //generate save dialog
         List<byte>     lbTelegram = null;
         bool           bSaved = false;
         //
         Dlg.Filter = "Setup Files|*.set";                  //setup filter
         Dlg.Title  = "Save the current GUI settings";      //setup headline
         Dlg.InitialDirectory = sFileInitDirectory;
         Dlg.ShowDialog();                                  //let the user select
         if( Dlg.FileName != "" )                           //filename given?
         {                                                  // yes, then...
            lbTelegram = routeSendToHardware( false );      // get the current GUI setup
            lbTelegram.RemoveRange( 0, 4 );                 // delete the control commands
            bSaved = setfileWrite(Dlg.FileName,lbTelegram); // write the file
            if( false == bSaved )                           // file saved?
            {                                               //  no, then...
               MessageBox.Show( 
                  "The file '" + Dlg.FileName + "' was not saved to disk!",
                  "Problems", 
                  MessageBoxButtons.OK, 
                  MessageBoxIcon.Error       );
            }
            else   //if( false == bSaved )                  // file was saved
            {                                               //  then...
               listboxSetFilesAdd( Dlg.FileName );          //  add the file to list
               sFileInitDirectory = System.IO.Path.GetDirectoryName( Dlg.FileName );
            }
         }  //if( Dlg.FileName != "" )
      }

      /**
       * This function is called, if the user wants to load
       * a setup file.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonSetLoad_Click( object sender, EventArgs e )
      {
         OpenFileDialog Dlg = new OpenFileDialog();         //generate open dialog
         List<byte>     lbData = null;
         //
         Dlg.Filter = "Setup Files|*.set";                  //setup filter
         Dlg.Title  = "Load previous saved GUI settings";   //setup headline
         Dlg.InitialDirectory = sFileInitDirectory;
         Dlg.ShowDialog();                                  //let the user select
         if( Dlg.FileName != "" )                           //filename given?
         {                                                  // yes, then...
            lbData = setfileRead( Dlg.FileName );           // read file
            if( null != lbData )                            // data available?
            {                                               //  yes, then...
               routeUpdate( lbData, true );                 //  setup GUI
               listboxSetFilesAdd( Dlg.FileName );          //  add file to filelist
            }
         }
      }

      /**
       * This function is called, if the user wants to load
       * a setup file within the list of all used setup files.
       * @param   sender   sender
       * @param   e        event
       */
      private void listBoxSetFiles_DoubleClick( object sender, EventArgs e )
      {
         int         iSel = listBoxSetFiles.SelectedIndex;
         List<byte>  lbData = null;
         //
         if( iSel >= 0 )                                    //a file selected?
         {                                                  // yes, then...
            lbData = setfileRead( listBoxSetFiles.Items[iSel].ToString() );  // read file
            if( null != lbData )                            // data available?
            {                                               //  yes, then...
               routeUpdate( lbData, true );                 //  setup GUI
            }
         }  //if( iSel >= 0 )
      }


      /**********************************************************************************************************************
       * DEVICE TAB
       * **************************************************/

      /**
       * This function is called, if the user wants to load
       * a setup file.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonDevReset_Click( object sender, EventArgs e )
      {
         serialSend( "A\x00W\x00" );                        //send "write 0x00 to address 0x00"
      }

      /**
       * This function is called, if the user wants to update
       * the data in the gui.
       * @param   sender   sender
       * @param   e        event
       */
      private void buttonDevUpdate_Click( object sender, EventArgs e )
      {
         guiUpdate();                                       //update the whole GUI
      }

   }
}
