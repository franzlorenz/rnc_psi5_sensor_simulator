﻿/******************************************************************************
 * Project        SENSOR SIMULATION GUI
 * (c) copyright  2017
 * Company        Harman/Becker Automotive Systems GmbH
 *                All rights reserved
 * Secrecy Level  STRICTLY CONFIDENTIAL
 *****************************************************************************/
/**
 * @file          Program.cs
 * @ingroup       test
 * @author        Franz Lorenz
 *
 * This class handles the application start.
 */

/*----------------------------------------------------------
 *  INCLUDES
 *--------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

/*----------------------------------------------------------
 *  CLASSES
 *--------------------------------------------------------*/
namespace SenSimGui
{
   static class Program
   {
      /// <summary>
      /// Der Haupteinstiegspunkt für die Anwendung.
      /// </summary>
      [STAThread]
      static void Main()
      {
         Application.EnableVisualStyles();
         Application.SetCompatibleTextRenderingDefault( false );
         Application.Run( new Splashscreen() );
         Application.Run( new Form1() );
      }
   }
}
