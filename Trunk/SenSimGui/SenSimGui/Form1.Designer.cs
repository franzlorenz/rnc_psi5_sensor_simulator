﻿namespace SenSimGui
{
   partial class Form1
   {
      /// <summary>
      /// Erforderliche Designervariable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Verwendete Ressourcen bereinigen.
      /// </summary>
      /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Vom Windows Form-Designer generierter Code

      /// <summary>
      /// Erforderliche Methode für die Designerunterstützung.
      /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
         this.tabApp = new System.Windows.Forms.TabControl();
         this.tabConn = new System.Windows.Forms.TabPage();
         this.pictureBox5 = new System.Windows.Forms.PictureBox();
         this.buttonConnUpdate = new System.Windows.Forms.Button();
         this.pictureBox1 = new System.Windows.Forms.PictureBox();
         this.label2 = new System.Windows.Forms.Label();
         this.buttonConn = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.listBoxConnInterface = new System.Windows.Forms.ListBox();
         this.tabSensorType = new System.Windows.Forms.TabPage();
         this.labelTypeSens1 = new System.Windows.Forms.Label();
         this.labelTypeSens3 = new System.Windows.Forms.Label();
         this.labelTypeSens2 = new System.Windows.Forms.Label();
         this.labelTypeSens4 = new System.Windows.Forms.Label();
         this.pictureBox4 = new System.Windows.Forms.PictureBox();
         this.labelNode1Info = new System.Windows.Forms.Label();
         this.button1Update = new System.Windows.Forms.Button();
         this.groupBox7 = new System.Windows.Forms.GroupBox();
         this.label25 = new System.Windows.Forms.Label();
         this.radioButton4SensorOFF = new System.Windows.Forms.RadioButton();
         this.label24 = new System.Windows.Forms.Label();
         this.label27 = new System.Windows.Forms.Label();
         this.label28 = new System.Windows.Forms.Label();
         this.radioButton4SensorRNS20 = new System.Windows.Forms.RadioButton();
         this.radioButton4SensorF3 = new System.Windows.Forms.RadioButton();
         this.radioButton4SensorF1F2 = new System.Windows.Forms.RadioButton();
         this.groupBox6 = new System.Windows.Forms.GroupBox();
         this.label22 = new System.Windows.Forms.Label();
         this.radioButton3SensorOFF = new System.Windows.Forms.RadioButton();
         this.label19 = new System.Windows.Forms.Label();
         this.label20 = new System.Windows.Forms.Label();
         this.label23 = new System.Windows.Forms.Label();
         this.radioButton3SensorRNS20 = new System.Windows.Forms.RadioButton();
         this.radioButton3SensorF3 = new System.Windows.Forms.RadioButton();
         this.radioButton3SensorF1F2 = new System.Windows.Forms.RadioButton();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.label21 = new System.Windows.Forms.Label();
         this.radioButton2SensorOFF = new System.Windows.Forms.RadioButton();
         this.label11 = new System.Windows.Forms.Label();
         this.label12 = new System.Windows.Forms.Label();
         this.label18 = new System.Windows.Forms.Label();
         this.radioButton2SensorRNS20 = new System.Windows.Forms.RadioButton();
         this.radioButton2SensorF3 = new System.Windows.Forms.RadioButton();
         this.radioButton2SensorF1F2 = new System.Windows.Forms.RadioButton();
         this.groupBox5 = new System.Windows.Forms.GroupBox();
         this.label10 = new System.Windows.Forms.Label();
         this.label8 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.radioButton1SensorF3 = new System.Windows.Forms.RadioButton();
         this.radioButton1SensorOFF = new System.Windows.Forms.RadioButton();
         this.radioButton1SensorF1F2 = new System.Windows.Forms.RadioButton();
         this.radioButton1SensorRNS20 = new System.Windows.Forms.RadioButton();
         this.tabSensorData = new System.Windows.Forms.TabPage();
         this.labelDataSens1 = new System.Windows.Forms.Label();
         this.labelDataSens3 = new System.Windows.Forms.Label();
         this.labelDataSens2 = new System.Windows.Forms.Label();
         this.labelDataSens4 = new System.Windows.Forms.Label();
         this.buttonVerRead = new System.Windows.Forms.Button();
         this.pictureBox2 = new System.Windows.Forms.PictureBox();
         this.labelRouteInfo = new System.Windows.Forms.Label();
         this.groupBox9 = new System.Windows.Forms.GroupBox();
         this.label7 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.numericUpDownRoute4Chl2 = new System.Windows.Forms.NumericUpDown();
         this.numericUpDownRoute4Chl1 = new System.Windows.Forms.NumericUpDown();
         this.radioButtonRoute4Const = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4AIN67 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4AIN45 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4AIN23 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4GEN3 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4GEN2 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4GEN1 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4GEN0 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute4AIN01 = new System.Windows.Forms.RadioButton();
         this.groupBox8 = new System.Windows.Forms.GroupBox();
         this.numericUpDownRoute3Chl2 = new System.Windows.Forms.NumericUpDown();
         this.label13 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.numericUpDownRoute3Chl1 = new System.Windows.Forms.NumericUpDown();
         this.radioButtonRoute3Const = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3AIN67 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3AIN45 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3AIN23 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3GEN3 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3GEN2 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3GEN1 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3GEN0 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute3AIN01 = new System.Windows.Forms.RadioButton();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.numericUpDownRoute2Chl2 = new System.Windows.Forms.NumericUpDown();
         this.numericUpDownRoute2Chl1 = new System.Windows.Forms.NumericUpDown();
         this.label15 = new System.Windows.Forms.Label();
         this.label14 = new System.Windows.Forms.Label();
         this.radioButtonRoute2Const = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2AIN67 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2AIN45 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2AIN23 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2GEN3 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2GEN2 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2GEN1 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2GEN0 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute2AIN01 = new System.Windows.Forms.RadioButton();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.numericUpDownRoute1Chl2 = new System.Windows.Forms.NumericUpDown();
         this.numericUpDownRoute1Chl1 = new System.Windows.Forms.NumericUpDown();
         this.label17 = new System.Windows.Forms.Label();
         this.label16 = new System.Windows.Forms.Label();
         this.radioButtonRoute1Const = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1AIN67 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1AIN45 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1AIN23 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1GEN3 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1GEN2 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1GEN1 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1GEN0 = new System.Windows.Forms.RadioButton();
         this.radioButtonRoute1AIN01 = new System.Windows.Forms.RadioButton();
         this.tabSetup = new System.Windows.Forms.TabPage();
         this.label29 = new System.Windows.Forms.Label();
         this.listBoxSetFiles = new System.Windows.Forms.ListBox();
         this.buttonSetSave = new System.Windows.Forms.Button();
         this.buttonSetLoad = new System.Windows.Forms.Button();
         this.label26 = new System.Windows.Forms.Label();
         this.pictureBox6 = new System.Windows.Forms.PictureBox();
         this.tabDebug = new System.Windows.Forms.TabPage();
         this.buttonDebugClear = new System.Windows.Forms.Button();
         this.label4 = new System.Windows.Forms.Label();
         this.pictureBox3 = new System.Windows.Forms.PictureBox();
         this.richTextBoxDebug = new System.Windows.Forms.RichTextBox();
         this.serialPort = new System.IO.Ports.SerialPort(this.components);
         this.tabDevInfo = new System.Windows.Forms.TabPage();
         this.pictureBox7 = new System.Windows.Forms.PictureBox();
         this.label30 = new System.Windows.Forms.Label();
         this.labelDevSens1 = new System.Windows.Forms.Label();
         this.labelDevSens3 = new System.Windows.Forms.Label();
         this.labelDevSens2 = new System.Windows.Forms.Label();
         this.labelDevSens4 = new System.Windows.Forms.Label();
         this.buttonDevUpdate = new System.Windows.Forms.Button();
         this.buttonDevReset = new System.Windows.Forms.Button();
         this.groupBox4 = new System.Windows.Forms.GroupBox();
         this.label31 = new System.Windows.Forms.Label();
         this.groupBox10 = new System.Windows.Forms.GroupBox();
         this.labelDeviceVersion = new System.Windows.Forms.Label();
         this.tabApp.SuspendLayout();
         this.tabConn.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
         this.tabSensorType.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
         this.groupBox7.SuspendLayout();
         this.groupBox6.SuspendLayout();
         this.groupBox3.SuspendLayout();
         this.groupBox5.SuspendLayout();
         this.tabSensorData.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
         this.groupBox9.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute4Chl2)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute4Chl1)).BeginInit();
         this.groupBox8.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute3Chl2)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute3Chl1)).BeginInit();
         this.groupBox1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute2Chl2)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute2Chl1)).BeginInit();
         this.groupBox2.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute1Chl2)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute1Chl1)).BeginInit();
         this.tabSetup.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
         this.tabDebug.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
         this.tabDevInfo.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
         this.groupBox4.SuspendLayout();
         this.groupBox10.SuspendLayout();
         this.SuspendLayout();
         // 
         // tabApp
         // 
         this.tabApp.Controls.Add(this.tabConn);
         this.tabApp.Controls.Add(this.tabSensorType);
         this.tabApp.Controls.Add(this.tabSensorData);
         this.tabApp.Controls.Add(this.tabSetup);
         this.tabApp.Controls.Add(this.tabDevInfo);
         this.tabApp.Controls.Add(this.tabDebug);
         this.tabApp.Location = new System.Drawing.Point(0, 0);
         this.tabApp.Name = "tabApp";
         this.tabApp.SelectedIndex = 0;
         this.tabApp.Size = new System.Drawing.Size(528, 392);
         this.tabApp.TabIndex = 0;
         // 
         // tabConn
         // 
         this.tabConn.BackColor = System.Drawing.SystemColors.Menu;
         this.tabConn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
         this.tabConn.Controls.Add(this.pictureBox5);
         this.tabConn.Controls.Add(this.buttonConnUpdate);
         this.tabConn.Controls.Add(this.pictureBox1);
         this.tabConn.Controls.Add(this.label2);
         this.tabConn.Controls.Add(this.buttonConn);
         this.tabConn.Controls.Add(this.label1);
         this.tabConn.Controls.Add(this.listBoxConnInterface);
         this.tabConn.Location = new System.Drawing.Point(4, 22);
         this.tabConn.Name = "tabConn";
         this.tabConn.Padding = new System.Windows.Forms.Padding(3);
         this.tabConn.Size = new System.Drawing.Size(520, 366);
         this.tabConn.TabIndex = 0;
         this.tabConn.Text = "Connection";
         this.tabConn.Enter += new System.EventHandler(this.tabConn_Enter);
         // 
         // pictureBox5
         // 
         this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
         this.pictureBox5.Location = new System.Drawing.Point(368, 152);
         this.pictureBox5.Name = "pictureBox5";
         this.pictureBox5.Size = new System.Drawing.Size(120, 112);
         this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox5.TabIndex = 6;
         this.pictureBox5.TabStop = false;
         // 
         // buttonConnUpdate
         // 
         this.buttonConnUpdate.Location = new System.Drawing.Point(131, 253);
         this.buttonConnUpdate.Name = "buttonConnUpdate";
         this.buttonConnUpdate.Size = new System.Drawing.Size(157, 27);
         this.buttonConnUpdate.TabIndex = 5;
         this.buttonConnUpdate.Text = "Update Interface List";
         this.buttonConnUpdate.UseVisualStyleBackColor = true;
         this.buttonConnUpdate.Click += new System.EventHandler(this.buttonConnUpdate_Click);
         // 
         // pictureBox1
         // 
         this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
         this.pictureBox1.Location = new System.Drawing.Point(8, 320);
         this.pictureBox1.Name = "pictureBox1";
         this.pictureBox1.Size = new System.Drawing.Size(40, 40);
         this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox1.TabIndex = 4;
         this.pictureBox1.TabStop = false;
         // 
         // label2
         // 
         this.label2.BackColor = System.Drawing.Color.Transparent;
         this.label2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
         this.label2.Location = new System.Drawing.Point(56, 320);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(466, 40);
         this.label2.TabIndex = 3;
         this.label2.Text = resources.GetString("label2.Text");
         // 
         // buttonConn
         // 
         this.buttonConn.Location = new System.Drawing.Point(369, 31);
         this.buttonConn.Name = "buttonConn";
         this.buttonConn.Size = new System.Drawing.Size(119, 70);
         this.buttonConn.TabIndex = 2;
         this.buttonConn.Text = "Connect";
         this.buttonConn.UseVisualStyleBackColor = true;
         this.buttonConn.Click += new System.EventHandler(this.buttonConn_Click);
         // 
         // label1
         // 
         this.label1.BackColor = System.Drawing.Color.Transparent;
         this.label1.Location = new System.Drawing.Point(25, 31);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(100, 29);
         this.label1.TabIndex = 1;
         this.label1.Text = "USB Interface";
         // 
         // listBoxConnInterface
         // 
         this.listBoxConnInterface.FormattingEnabled = true;
         this.listBoxConnInterface.Location = new System.Drawing.Point(131, 31);
         this.listBoxConnInterface.Name = "listBoxConnInterface";
         this.listBoxConnInterface.Size = new System.Drawing.Size(157, 225);
         this.listBoxConnInterface.TabIndex = 0;
         this.listBoxConnInterface.SelectedIndexChanged += new System.EventHandler(this.listBoxConnInterface_SelectedIndexChanged);
         // 
         // tabSensorType
         // 
         this.tabSensorType.BackColor = System.Drawing.Color.Transparent;
         this.tabSensorType.Controls.Add(this.labelTypeSens1);
         this.tabSensorType.Controls.Add(this.labelTypeSens3);
         this.tabSensorType.Controls.Add(this.labelTypeSens2);
         this.tabSensorType.Controls.Add(this.labelTypeSens4);
         this.tabSensorType.Controls.Add(this.pictureBox4);
         this.tabSensorType.Controls.Add(this.labelNode1Info);
         this.tabSensorType.Controls.Add(this.button1Update);
         this.tabSensorType.Controls.Add(this.groupBox7);
         this.tabSensorType.Controls.Add(this.groupBox6);
         this.tabSensorType.Controls.Add(this.groupBox3);
         this.tabSensorType.Controls.Add(this.groupBox5);
         this.tabSensorType.Location = new System.Drawing.Point(4, 22);
         this.tabSensorType.Name = "tabSensorType";
         this.tabSensorType.Size = new System.Drawing.Size(520, 366);
         this.tabSensorType.TabIndex = 3;
         this.tabSensorType.Text = "Sensor Type";
         // 
         // labelTypeSens1
         // 
         this.labelTypeSens1.BackColor = System.Drawing.Color.Gray;
         this.labelTypeSens1.Location = new System.Drawing.Point(480, 328);
         this.labelTypeSens1.Name = "labelTypeSens1";
         this.labelTypeSens1.Size = new System.Drawing.Size(7, 23);
         this.labelTypeSens1.TabIndex = 14;
         // 
         // labelTypeSens3
         // 
         this.labelTypeSens3.BackColor = System.Drawing.Color.Gray;
         this.labelTypeSens3.Location = new System.Drawing.Point(496, 328);
         this.labelTypeSens3.Name = "labelTypeSens3";
         this.labelTypeSens3.Size = new System.Drawing.Size(7, 23);
         this.labelTypeSens3.TabIndex = 14;
         // 
         // labelTypeSens2
         // 
         this.labelTypeSens2.BackColor = System.Drawing.Color.Gray;
         this.labelTypeSens2.Location = new System.Drawing.Point(488, 328);
         this.labelTypeSens2.Name = "labelTypeSens2";
         this.labelTypeSens2.Size = new System.Drawing.Size(7, 23);
         this.labelTypeSens2.TabIndex = 14;
         // 
         // labelTypeSens4
         // 
         this.labelTypeSens4.BackColor = System.Drawing.Color.Gray;
         this.labelTypeSens4.Location = new System.Drawing.Point(504, 328);
         this.labelTypeSens4.Name = "labelTypeSens4";
         this.labelTypeSens4.Size = new System.Drawing.Size(7, 23);
         this.labelTypeSens4.TabIndex = 14;
         // 
         // pictureBox4
         // 
         this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
         this.pictureBox4.Location = new System.Drawing.Point(8, 320);
         this.pictureBox4.Name = "pictureBox4";
         this.pictureBox4.Size = new System.Drawing.Size(40, 40);
         this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox4.TabIndex = 7;
         this.pictureBox4.TabStop = false;
         // 
         // labelNode1Info
         // 
         this.labelNode1Info.BackColor = System.Drawing.Color.Transparent;
         this.labelNode1Info.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
         this.labelNode1Info.Location = new System.Drawing.Point(56, 320);
         this.labelNode1Info.Name = "labelNode1Info";
         this.labelNode1Info.Size = new System.Drawing.Size(416, 40);
         this.labelNode1Info.TabIndex = 8;
         this.labelNode1Info.Text = "This dialog shows the sensor types of all the available PSI5 nodes. Press the but" +
             "ton \'Update\' to get the settings from the hardware device.";
         // 
         // button1Update
         // 
         this.button1Update.Location = new System.Drawing.Point(200, 248);
         this.button1Update.Name = "button1Update";
         this.button1Update.Size = new System.Drawing.Size(120, 32);
         this.button1Update.TabIndex = 13;
         this.button1Update.Text = "&Update";
         this.button1Update.UseVisualStyleBackColor = true;
         this.button1Update.Click += new System.EventHandler(this.button1Update_Click);
         // 
         // groupBox7
         // 
         this.groupBox7.Controls.Add(this.label25);
         this.groupBox7.Controls.Add(this.radioButton4SensorOFF);
         this.groupBox7.Controls.Add(this.label24);
         this.groupBox7.Controls.Add(this.label27);
         this.groupBox7.Controls.Add(this.label28);
         this.groupBox7.Controls.Add(this.radioButton4SensorRNS20);
         this.groupBox7.Controls.Add(this.radioButton4SensorF3);
         this.groupBox7.Controls.Add(this.radioButton4SensorF1F2);
         this.groupBox7.Location = new System.Drawing.Point(392, 16);
         this.groupBox7.Name = "groupBox7";
         this.groupBox7.Size = new System.Drawing.Size(120, 216);
         this.groupBox7.TabIndex = 10;
         this.groupBox7.TabStop = false;
         this.groupBox7.Text = "PSI5 Node #4";
         // 
         // label25
         // 
         this.label25.AutoSize = true;
         this.label25.Location = new System.Drawing.Point(24, 40);
         this.label25.Name = "label25";
         this.label25.Size = new System.Drawing.Size(79, 13);
         this.label25.TabIndex = 3;
         this.label25.Text = "No PSI5 Signal";
         // 
         // radioButton4SensorOFF
         // 
         this.radioButton4SensorOFF.BackColor = System.Drawing.Color.Transparent;
         this.radioButton4SensorOFF.Location = new System.Drawing.Point(8, 24);
         this.radioButton4SensorOFF.Name = "radioButton4SensorOFF";
         this.radioButton4SensorOFF.Size = new System.Drawing.Size(104, 32);
         this.radioButton4SensorOFF.TabIndex = 2;
         this.radioButton4SensorOFF.TabStop = true;
         this.radioButton4SensorOFF.Text = "Sensor OFF";
         this.radioButton4SensorOFF.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton4SensorOFF.UseVisualStyleBackColor = false;
         this.radioButton4SensorOFF.Click += new System.EventHandler(this.radioButton4SensorOFF_Click);
         // 
         // label24
         // 
         this.label24.AutoSize = true;
         this.label24.Location = new System.Drawing.Point(24, 160);
         this.label24.Name = "label24";
         this.label24.Size = new System.Drawing.Size(71, 13);
         this.label24.TabIndex = 1;
         this.label24.Text = "+/-15G, Sync";
         // 
         // label27
         // 
         this.label27.AutoSize = true;
         this.label27.Location = new System.Drawing.Point(24, 120);
         this.label27.Name = "label27";
         this.label27.Size = new System.Drawing.Size(79, 13);
         this.label27.TabIndex = 1;
         this.label27.Text = "+/-1.6G, Async";
         // 
         // label28
         // 
         this.label28.AutoSize = true;
         this.label28.Location = new System.Drawing.Point(24, 80);
         this.label28.Name = "label28";
         this.label28.Size = new System.Drawing.Size(76, 13);
         this.label28.TabIndex = 1;
         this.label28.Text = "+/-16G, Async";
         // 
         // radioButton4SensorRNS20
         // 
         this.radioButton4SensorRNS20.BackColor = System.Drawing.Color.Transparent;
         this.radioButton4SensorRNS20.Location = new System.Drawing.Point(8, 144);
         this.radioButton4SensorRNS20.Name = "radioButton4SensorRNS20";
         this.radioButton4SensorRNS20.Size = new System.Drawing.Size(104, 32);
         this.radioButton4SensorRNS20.TabIndex = 0;
         this.radioButton4SensorRNS20.TabStop = true;
         this.radioButton4SensorRNS20.Text = "Bosch RNS 2.0";
         this.radioButton4SensorRNS20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton4SensorRNS20.UseVisualStyleBackColor = false;
         this.radioButton4SensorRNS20.Click += new System.EventHandler(this.radioButton1SensorRNS20_Click);
         // 
         // radioButton4SensorF3
         // 
         this.radioButton4SensorF3.BackColor = System.Drawing.Color.Transparent;
         this.radioButton4SensorF3.Location = new System.Drawing.Point(8, 104);
         this.radioButton4SensorF3.Name = "radioButton4SensorF3";
         this.radioButton4SensorF3.Size = new System.Drawing.Size(104, 32);
         this.radioButton4SensorF3.TabIndex = 0;
         this.radioButton4SensorF3.TabStop = true;
         this.radioButton4SensorF3.Text = "Bosch F3";
         this.radioButton4SensorF3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton4SensorF3.UseVisualStyleBackColor = false;
         this.radioButton4SensorF3.Click += new System.EventHandler(this.radioButton1SensorF3_Click);
         // 
         // radioButton4SensorF1F2
         // 
         this.radioButton4SensorF1F2.BackColor = System.Drawing.Color.Transparent;
         this.radioButton4SensorF1F2.Location = new System.Drawing.Point(8, 64);
         this.radioButton4SensorF1F2.Name = "radioButton4SensorF1F2";
         this.radioButton4SensorF1F2.Size = new System.Drawing.Size(104, 32);
         this.radioButton4SensorF1F2.TabIndex = 0;
         this.radioButton4SensorF1F2.TabStop = true;
         this.radioButton4SensorF1F2.Text = "Bosch F1/F2";
         this.radioButton4SensorF1F2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton4SensorF1F2.UseVisualStyleBackColor = false;
         this.radioButton4SensorF1F2.Click += new System.EventHandler(this.radioButton1SensorF1F2_Click);
         // 
         // groupBox6
         // 
         this.groupBox6.Controls.Add(this.label22);
         this.groupBox6.Controls.Add(this.radioButton3SensorOFF);
         this.groupBox6.Controls.Add(this.label19);
         this.groupBox6.Controls.Add(this.label20);
         this.groupBox6.Controls.Add(this.label23);
         this.groupBox6.Controls.Add(this.radioButton3SensorRNS20);
         this.groupBox6.Controls.Add(this.radioButton3SensorF3);
         this.groupBox6.Controls.Add(this.radioButton3SensorF1F2);
         this.groupBox6.Location = new System.Drawing.Point(264, 16);
         this.groupBox6.Name = "groupBox6";
         this.groupBox6.Size = new System.Drawing.Size(120, 216);
         this.groupBox6.TabIndex = 10;
         this.groupBox6.TabStop = false;
         this.groupBox6.Text = "PSI5 Node #3";
         // 
         // label22
         // 
         this.label22.AutoSize = true;
         this.label22.Location = new System.Drawing.Point(24, 40);
         this.label22.Name = "label22";
         this.label22.Size = new System.Drawing.Size(79, 13);
         this.label22.TabIndex = 3;
         this.label22.Text = "No PSI5 Signal";
         // 
         // radioButton3SensorOFF
         // 
         this.radioButton3SensorOFF.BackColor = System.Drawing.Color.Transparent;
         this.radioButton3SensorOFF.Location = new System.Drawing.Point(8, 24);
         this.radioButton3SensorOFF.Name = "radioButton3SensorOFF";
         this.radioButton3SensorOFF.Size = new System.Drawing.Size(104, 32);
         this.radioButton3SensorOFF.TabIndex = 2;
         this.radioButton3SensorOFF.TabStop = true;
         this.radioButton3SensorOFF.Text = "Sensor OFF";
         this.radioButton3SensorOFF.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton3SensorOFF.UseVisualStyleBackColor = false;
         this.radioButton3SensorOFF.Click += new System.EventHandler(this.radioButton3SensorOFF_Click);
         // 
         // label19
         // 
         this.label19.AutoSize = true;
         this.label19.Location = new System.Drawing.Point(24, 160);
         this.label19.Name = "label19";
         this.label19.Size = new System.Drawing.Size(71, 13);
         this.label19.TabIndex = 1;
         this.label19.Text = "+/-15G, Sync";
         // 
         // label20
         // 
         this.label20.AutoSize = true;
         this.label20.Location = new System.Drawing.Point(24, 120);
         this.label20.Name = "label20";
         this.label20.Size = new System.Drawing.Size(79, 13);
         this.label20.TabIndex = 1;
         this.label20.Text = "+/-1.6G, Async";
         // 
         // label23
         // 
         this.label23.AutoSize = true;
         this.label23.Location = new System.Drawing.Point(24, 80);
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size(76, 13);
         this.label23.TabIndex = 1;
         this.label23.Text = "+/-16G, Async";
         // 
         // radioButton3SensorRNS20
         // 
         this.radioButton3SensorRNS20.BackColor = System.Drawing.Color.Transparent;
         this.radioButton3SensorRNS20.Location = new System.Drawing.Point(8, 144);
         this.radioButton3SensorRNS20.Name = "radioButton3SensorRNS20";
         this.radioButton3SensorRNS20.Size = new System.Drawing.Size(99, 32);
         this.radioButton3SensorRNS20.TabIndex = 0;
         this.radioButton3SensorRNS20.TabStop = true;
         this.radioButton3SensorRNS20.Text = "Bosch RNS 2.0";
         this.radioButton3SensorRNS20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton3SensorRNS20.UseVisualStyleBackColor = false;
         this.radioButton3SensorRNS20.Click += new System.EventHandler(this.radioButton1SensorRNS20_Click);
         // 
         // radioButton3SensorF3
         // 
         this.radioButton3SensorF3.BackColor = System.Drawing.Color.Transparent;
         this.radioButton3SensorF3.Location = new System.Drawing.Point(8, 104);
         this.radioButton3SensorF3.Name = "radioButton3SensorF3";
         this.radioButton3SensorF3.Size = new System.Drawing.Size(104, 32);
         this.radioButton3SensorF3.TabIndex = 0;
         this.radioButton3SensorF3.TabStop = true;
         this.radioButton3SensorF3.Text = "Bosch F3";
         this.radioButton3SensorF3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton3SensorF3.UseVisualStyleBackColor = false;
         this.radioButton3SensorF3.Click += new System.EventHandler(this.radioButton1SensorF3_Click);
         // 
         // radioButton3SensorF1F2
         // 
         this.radioButton3SensorF1F2.BackColor = System.Drawing.Color.Transparent;
         this.radioButton3SensorF1F2.Location = new System.Drawing.Point(8, 64);
         this.radioButton3SensorF1F2.Name = "radioButton3SensorF1F2";
         this.radioButton3SensorF1F2.Size = new System.Drawing.Size(104, 32);
         this.radioButton3SensorF1F2.TabIndex = 0;
         this.radioButton3SensorF1F2.TabStop = true;
         this.radioButton3SensorF1F2.Text = "Bosch F1/F2";
         this.radioButton3SensorF1F2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton3SensorF1F2.UseVisualStyleBackColor = false;
         this.radioButton3SensorF1F2.Click += new System.EventHandler(this.radioButton1SensorF1F2_Click);
         // 
         // groupBox3
         // 
         this.groupBox3.Controls.Add(this.label21);
         this.groupBox3.Controls.Add(this.radioButton2SensorOFF);
         this.groupBox3.Controls.Add(this.label11);
         this.groupBox3.Controls.Add(this.label12);
         this.groupBox3.Controls.Add(this.label18);
         this.groupBox3.Controls.Add(this.radioButton2SensorRNS20);
         this.groupBox3.Controls.Add(this.radioButton2SensorF3);
         this.groupBox3.Controls.Add(this.radioButton2SensorF1F2);
         this.groupBox3.Location = new System.Drawing.Point(136, 16);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(120, 216);
         this.groupBox3.TabIndex = 10;
         this.groupBox3.TabStop = false;
         this.groupBox3.Text = "PSI5 Node #2";
         // 
         // label21
         // 
         this.label21.AutoSize = true;
         this.label21.Location = new System.Drawing.Point(24, 40);
         this.label21.Name = "label21";
         this.label21.Size = new System.Drawing.Size(79, 13);
         this.label21.TabIndex = 3;
         this.label21.Text = "No PSI5 Signal";
         // 
         // radioButton2SensorOFF
         // 
         this.radioButton2SensorOFF.BackColor = System.Drawing.Color.Transparent;
         this.radioButton2SensorOFF.Location = new System.Drawing.Point(8, 24);
         this.radioButton2SensorOFF.Name = "radioButton2SensorOFF";
         this.radioButton2SensorOFF.Size = new System.Drawing.Size(100, 32);
         this.radioButton2SensorOFF.TabIndex = 2;
         this.radioButton2SensorOFF.TabStop = true;
         this.radioButton2SensorOFF.Text = "Sensor OFF";
         this.radioButton2SensorOFF.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton2SensorOFF.UseVisualStyleBackColor = false;
         this.radioButton2SensorOFF.Click += new System.EventHandler(this.radioButton2SensorOFF_Click);
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(24, 160);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(71, 13);
         this.label11.TabIndex = 1;
         this.label11.Text = "+/-15G, Sync";
         // 
         // label12
         // 
         this.label12.AutoSize = true;
         this.label12.Location = new System.Drawing.Point(24, 120);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(79, 13);
         this.label12.TabIndex = 1;
         this.label12.Text = "+/-1.6G, Async";
         // 
         // label18
         // 
         this.label18.AutoSize = true;
         this.label18.Location = new System.Drawing.Point(24, 80);
         this.label18.Name = "label18";
         this.label18.Size = new System.Drawing.Size(76, 13);
         this.label18.TabIndex = 1;
         this.label18.Text = "+/-16G, Async";
         // 
         // radioButton2SensorRNS20
         // 
         this.radioButton2SensorRNS20.BackColor = System.Drawing.Color.Transparent;
         this.radioButton2SensorRNS20.Location = new System.Drawing.Point(8, 144);
         this.radioButton2SensorRNS20.Name = "radioButton2SensorRNS20";
         this.radioButton2SensorRNS20.Size = new System.Drawing.Size(99, 32);
         this.radioButton2SensorRNS20.TabIndex = 0;
         this.radioButton2SensorRNS20.TabStop = true;
         this.radioButton2SensorRNS20.Text = "Bosch RNS 2.0";
         this.radioButton2SensorRNS20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton2SensorRNS20.UseVisualStyleBackColor = false;
         this.radioButton2SensorRNS20.Click += new System.EventHandler(this.radioButton1SensorRNS20_Click);
         // 
         // radioButton2SensorF3
         // 
         this.radioButton2SensorF3.BackColor = System.Drawing.Color.Transparent;
         this.radioButton2SensorF3.Location = new System.Drawing.Point(8, 104);
         this.radioButton2SensorF3.Name = "radioButton2SensorF3";
         this.radioButton2SensorF3.Size = new System.Drawing.Size(104, 32);
         this.radioButton2SensorF3.TabIndex = 0;
         this.radioButton2SensorF3.TabStop = true;
         this.radioButton2SensorF3.Text = "Bosch F3";
         this.radioButton2SensorF3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton2SensorF3.UseVisualStyleBackColor = false;
         this.radioButton2SensorF3.Click += new System.EventHandler(this.radioButton1SensorF3_Click);
         // 
         // radioButton2SensorF1F2
         // 
         this.radioButton2SensorF1F2.BackColor = System.Drawing.Color.Transparent;
         this.radioButton2SensorF1F2.Location = new System.Drawing.Point(8, 64);
         this.radioButton2SensorF1F2.Name = "radioButton2SensorF1F2";
         this.radioButton2SensorF1F2.Size = new System.Drawing.Size(96, 32);
         this.radioButton2SensorF1F2.TabIndex = 0;
         this.radioButton2SensorF1F2.TabStop = true;
         this.radioButton2SensorF1F2.Text = "Bosch F1/F2";
         this.radioButton2SensorF1F2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton2SensorF1F2.UseVisualStyleBackColor = false;
         this.radioButton2SensorF1F2.Click += new System.EventHandler(this.radioButton1SensorF1F2_Click);
         // 
         // groupBox5
         // 
         this.groupBox5.BackColor = System.Drawing.Color.Transparent;
         this.groupBox5.Controls.Add(this.label10);
         this.groupBox5.Controls.Add(this.label8);
         this.groupBox5.Controls.Add(this.label3);
         this.groupBox5.Controls.Add(this.label5);
         this.groupBox5.Controls.Add(this.radioButton1SensorF3);
         this.groupBox5.Controls.Add(this.radioButton1SensorOFF);
         this.groupBox5.Controls.Add(this.radioButton1SensorF1F2);
         this.groupBox5.Controls.Add(this.radioButton1SensorRNS20);
         this.groupBox5.ForeColor = System.Drawing.SystemColors.ControlText;
         this.groupBox5.Location = new System.Drawing.Point(8, 16);
         this.groupBox5.Name = "groupBox5";
         this.groupBox5.Size = new System.Drawing.Size(120, 216);
         this.groupBox5.TabIndex = 10;
         this.groupBox5.TabStop = false;
         this.groupBox5.Text = "PSI5 Node #1";
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.BackColor = System.Drawing.Color.Transparent;
         this.label10.Location = new System.Drawing.Point(24, 160);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(71, 13);
         this.label10.TabIndex = 1;
         this.label10.Text = "+/-15G, Sync";
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.BackColor = System.Drawing.Color.Transparent;
         this.label8.Location = new System.Drawing.Point(24, 120);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(79, 13);
         this.label8.TabIndex = 1;
         this.label8.Text = "+/-1.6G, Async";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.BackColor = System.Drawing.Color.Transparent;
         this.label3.Location = new System.Drawing.Point(24, 40);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(79, 13);
         this.label3.TabIndex = 1;
         this.label3.Text = "No PSI5 Signal";
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.BackColor = System.Drawing.Color.Transparent;
         this.label5.Location = new System.Drawing.Point(24, 80);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(76, 13);
         this.label5.TabIndex = 1;
         this.label5.Text = "+/-16G, Async";
         // 
         // radioButton1SensorF3
         // 
         this.radioButton1SensorF3.BackColor = System.Drawing.Color.Transparent;
         this.radioButton1SensorF3.Location = new System.Drawing.Point(8, 104);
         this.radioButton1SensorF3.Name = "radioButton1SensorF3";
         this.radioButton1SensorF3.Size = new System.Drawing.Size(104, 32);
         this.radioButton1SensorF3.TabIndex = 0;
         this.radioButton1SensorF3.TabStop = true;
         this.radioButton1SensorF3.Text = "Bosch F3";
         this.radioButton1SensorF3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton1SensorF3.UseVisualStyleBackColor = false;
         this.radioButton1SensorF3.Click += new System.EventHandler(this.radioButton1SensorF3_Click);
         // 
         // radioButton1SensorOFF
         // 
         this.radioButton1SensorOFF.BackColor = System.Drawing.Color.Transparent;
         this.radioButton1SensorOFF.Location = new System.Drawing.Point(8, 24);
         this.radioButton1SensorOFF.Name = "radioButton1SensorOFF";
         this.radioButton1SensorOFF.Size = new System.Drawing.Size(104, 32);
         this.radioButton1SensorOFF.TabIndex = 0;
         this.radioButton1SensorOFF.TabStop = true;
         this.radioButton1SensorOFF.Text = "Sensor OFF";
         this.radioButton1SensorOFF.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton1SensorOFF.UseVisualStyleBackColor = false;
         this.radioButton1SensorOFF.Click += new System.EventHandler(this.radioButton1SensorOFF_Click);
         // 
         // radioButton1SensorF1F2
         // 
         this.radioButton1SensorF1F2.BackColor = System.Drawing.Color.Transparent;
         this.radioButton1SensorF1F2.Location = new System.Drawing.Point(8, 64);
         this.radioButton1SensorF1F2.Name = "radioButton1SensorF1F2";
         this.radioButton1SensorF1F2.Size = new System.Drawing.Size(104, 32);
         this.radioButton1SensorF1F2.TabIndex = 0;
         this.radioButton1SensorF1F2.TabStop = true;
         this.radioButton1SensorF1F2.Text = "Bosch F1/F2";
         this.radioButton1SensorF1F2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton1SensorF1F2.UseVisualStyleBackColor = false;
         this.radioButton1SensorF1F2.Click += new System.EventHandler(this.radioButton1SensorF1F2_Click);
         // 
         // radioButton1SensorRNS20
         // 
         this.radioButton1SensorRNS20.BackColor = System.Drawing.Color.Transparent;
         this.radioButton1SensorRNS20.Location = new System.Drawing.Point(8, 144);
         this.radioButton1SensorRNS20.Name = "radioButton1SensorRNS20";
         this.radioButton1SensorRNS20.Size = new System.Drawing.Size(99, 32);
         this.radioButton1SensorRNS20.TabIndex = 0;
         this.radioButton1SensorRNS20.TabStop = true;
         this.radioButton1SensorRNS20.Text = "Bosch RNS 2.0";
         this.radioButton1SensorRNS20.TextAlign = System.Drawing.ContentAlignment.TopLeft;
         this.radioButton1SensorRNS20.UseVisualStyleBackColor = false;
         this.radioButton1SensorRNS20.Click += new System.EventHandler(this.radioButton1SensorRNS20_Click);
         // 
         // tabSensorData
         // 
         this.tabSensorData.BackColor = System.Drawing.SystemColors.Menu;
         this.tabSensorData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
         this.tabSensorData.Controls.Add(this.labelDataSens1);
         this.tabSensorData.Controls.Add(this.labelDataSens3);
         this.tabSensorData.Controls.Add(this.labelDataSens2);
         this.tabSensorData.Controls.Add(this.labelDataSens4);
         this.tabSensorData.Controls.Add(this.buttonVerRead);
         this.tabSensorData.Controls.Add(this.pictureBox2);
         this.tabSensorData.Controls.Add(this.labelRouteInfo);
         this.tabSensorData.Controls.Add(this.groupBox9);
         this.tabSensorData.Controls.Add(this.groupBox8);
         this.tabSensorData.Controls.Add(this.groupBox1);
         this.tabSensorData.Controls.Add(this.groupBox2);
         this.tabSensorData.Location = new System.Drawing.Point(4, 22);
         this.tabSensorData.Name = "tabSensorData";
         this.tabSensorData.Padding = new System.Windows.Forms.Padding(3);
         this.tabSensorData.Size = new System.Drawing.Size(520, 366);
         this.tabSensorData.TabIndex = 1;
         this.tabSensorData.Text = "Sensor Data";
         // 
         // labelDataSens1
         // 
         this.labelDataSens1.BackColor = System.Drawing.Color.Gray;
         this.labelDataSens1.Location = new System.Drawing.Point(480, 328);
         this.labelDataSens1.Name = "labelDataSens1";
         this.labelDataSens1.Size = new System.Drawing.Size(7, 23);
         this.labelDataSens1.TabIndex = 17;
         // 
         // labelDataSens3
         // 
         this.labelDataSens3.BackColor = System.Drawing.Color.Gray;
         this.labelDataSens3.Location = new System.Drawing.Point(496, 328);
         this.labelDataSens3.Name = "labelDataSens3";
         this.labelDataSens3.Size = new System.Drawing.Size(7, 23);
         this.labelDataSens3.TabIndex = 18;
         // 
         // labelDataSens2
         // 
         this.labelDataSens2.BackColor = System.Drawing.Color.Gray;
         this.labelDataSens2.Location = new System.Drawing.Point(488, 328);
         this.labelDataSens2.Name = "labelDataSens2";
         this.labelDataSens2.Size = new System.Drawing.Size(7, 23);
         this.labelDataSens2.TabIndex = 15;
         // 
         // labelDataSens4
         // 
         this.labelDataSens4.BackColor = System.Drawing.Color.Gray;
         this.labelDataSens4.Location = new System.Drawing.Point(504, 328);
         this.labelDataSens4.Name = "labelDataSens4";
         this.labelDataSens4.Size = new System.Drawing.Size(7, 23);
         this.labelDataSens4.TabIndex = 16;
         // 
         // buttonVerRead
         // 
         this.buttonVerRead.Location = new System.Drawing.Point(200, 280);
         this.buttonVerRead.Name = "buttonVerRead";
         this.buttonVerRead.Size = new System.Drawing.Size(120, 32);
         this.buttonVerRead.TabIndex = 7;
         this.buttonVerRead.Text = "&Update";
         this.buttonVerRead.UseVisualStyleBackColor = true;
         this.buttonVerRead.Click += new System.EventHandler(this.buttonVerRead_Click);
         // 
         // pictureBox2
         // 
         this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
         this.pictureBox2.Location = new System.Drawing.Point(8, 320);
         this.pictureBox2.Name = "pictureBox2";
         this.pictureBox2.Size = new System.Drawing.Size(40, 40);
         this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox2.TabIndex = 6;
         this.pictureBox2.TabStop = false;
         // 
         // labelRouteInfo
         // 
         this.labelRouteInfo.BackColor = System.Drawing.Color.Transparent;
         this.labelRouteInfo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.labelRouteInfo.Location = new System.Drawing.Point(56, 320);
         this.labelRouteInfo.Name = "labelRouteInfo";
         this.labelRouteInfo.Size = new System.Drawing.Size(416, 40);
         this.labelRouteInfo.TabIndex = 5;
         this.labelRouteInfo.Text = "This page shows the data routing between the analogue inputs and the PSI5 sensors" +
             ". Press the button \'Update\' to get the settings from the hardware device.";
         // 
         // groupBox9
         // 
         this.groupBox9.Controls.Add(this.label7);
         this.groupBox9.Controls.Add(this.label6);
         this.groupBox9.Controls.Add(this.numericUpDownRoute4Chl2);
         this.groupBox9.Controls.Add(this.numericUpDownRoute4Chl1);
         this.groupBox9.Controls.Add(this.radioButtonRoute4Const);
         this.groupBox9.Controls.Add(this.radioButtonRoute4AIN67);
         this.groupBox9.Controls.Add(this.radioButtonRoute4AIN45);
         this.groupBox9.Controls.Add(this.radioButtonRoute4AIN23);
         this.groupBox9.Controls.Add(this.radioButtonRoute4GEN3);
         this.groupBox9.Controls.Add(this.radioButtonRoute4GEN2);
         this.groupBox9.Controls.Add(this.radioButtonRoute4GEN1);
         this.groupBox9.Controls.Add(this.radioButtonRoute4GEN0);
         this.groupBox9.Controls.Add(this.radioButtonRoute4AIN01);
         this.groupBox9.Location = new System.Drawing.Point(392, 16);
         this.groupBox9.Name = "groupBox9";
         this.groupBox9.Size = new System.Drawing.Size(120, 256);
         this.groupBox9.TabIndex = 0;
         this.groupBox9.TabStop = false;
         this.groupBox9.Text = "PSI5 Node #4";
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(8, 224);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(18, 13);
         this.label7.TabIndex = 2;
         this.label7.Text = "0x";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(8, 200);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(18, 13);
         this.label6.TabIndex = 2;
         this.label6.Text = "0x";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // numericUpDownRoute4Chl2
         // 
         this.numericUpDownRoute4Chl2.Hexadecimal = true;
         this.numericUpDownRoute4Chl2.Location = new System.Drawing.Point(32, 224);
         this.numericUpDownRoute4Chl2.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute4Chl2.Name = "numericUpDownRoute4Chl2";
         this.numericUpDownRoute4Chl2.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute4Chl2.TabIndex = 1;
         this.numericUpDownRoute4Chl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute4Chl2.ThousandsSeparator = true;
         this.numericUpDownRoute4Chl2.Click += new System.EventHandler(this.numericUpDownRoute4Chl2_Click);
         // 
         // numericUpDownRoute4Chl1
         // 
         this.numericUpDownRoute4Chl1.Hexadecimal = true;
         this.numericUpDownRoute4Chl1.Location = new System.Drawing.Point(32, 200);
         this.numericUpDownRoute4Chl1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute4Chl1.Name = "numericUpDownRoute4Chl1";
         this.numericUpDownRoute4Chl1.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute4Chl1.TabIndex = 1;
         this.numericUpDownRoute4Chl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute4Chl1.Click += new System.EventHandler(this.numericUpDownRoute4Chl1_Click);
         // 
         // radioButtonRoute4Const
         // 
         this.radioButtonRoute4Const.AutoSize = true;
         this.radioButtonRoute4Const.Location = new System.Drawing.Point(8, 168);
         this.radioButtonRoute4Const.Name = "radioButtonRoute4Const";
         this.radioButtonRoute4Const.Size = new System.Drawing.Size(72, 17);
         this.radioButtonRoute4Const.TabIndex = 0;
         this.radioButtonRoute4Const.TabStop = true;
         this.radioButtonRoute4Const.Text = "Constants";
         this.radioButtonRoute4Const.UseVisualStyleBackColor = true;
         this.radioButtonRoute4Const.Click += new System.EventHandler(this.radioButtonRoute4Const_Click);
         // 
         // radioButtonRoute4AIN67
         // 
         this.radioButtonRoute4AIN67.AutoSize = true;
         this.radioButtonRoute4AIN67.Location = new System.Drawing.Point(8, 72);
         this.radioButtonRoute4AIN67.Name = "radioButtonRoute4AIN67";
         this.radioButtonRoute4AIN67.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute4AIN67.TabIndex = 0;
         this.radioButtonRoute4AIN67.TabStop = true;
         this.radioButtonRoute4AIN67.Text = "AN Input 6/7";
         this.radioButtonRoute4AIN67.UseVisualStyleBackColor = true;
         this.radioButtonRoute4AIN67.Click += new System.EventHandler(this.radioButtonRoute4AIN67_Click);
         // 
         // radioButtonRoute4AIN45
         // 
         this.radioButtonRoute4AIN45.AutoSize = true;
         this.radioButtonRoute4AIN45.Location = new System.Drawing.Point(8, 56);
         this.radioButtonRoute4AIN45.Name = "radioButtonRoute4AIN45";
         this.radioButtonRoute4AIN45.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute4AIN45.TabIndex = 0;
         this.radioButtonRoute4AIN45.TabStop = true;
         this.radioButtonRoute4AIN45.Text = "AN Input 4/5";
         this.radioButtonRoute4AIN45.UseVisualStyleBackColor = true;
         this.radioButtonRoute4AIN45.Click += new System.EventHandler(this.radioButtonRoute4AIN45_Click);
         // 
         // radioButtonRoute4AIN23
         // 
         this.radioButtonRoute4AIN23.AutoSize = true;
         this.radioButtonRoute4AIN23.Location = new System.Drawing.Point(8, 40);
         this.radioButtonRoute4AIN23.Name = "radioButtonRoute4AIN23";
         this.radioButtonRoute4AIN23.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute4AIN23.TabIndex = 0;
         this.radioButtonRoute4AIN23.TabStop = true;
         this.radioButtonRoute4AIN23.Text = "AN Input 2/3";
         this.radioButtonRoute4AIN23.UseVisualStyleBackColor = true;
         this.radioButtonRoute4AIN23.Click += new System.EventHandler(this.radioButtonRoute4AIN23_Click);
         // 
         // radioButtonRoute4GEN3
         // 
         this.radioButtonRoute4GEN3.AutoSize = true;
         this.radioButtonRoute4GEN3.Enabled = false;
         this.radioButtonRoute4GEN3.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute4GEN3.Location = new System.Drawing.Point(8, 144);
         this.radioButtonRoute4GEN3.Name = "radioButtonRoute4GEN3";
         this.radioButtonRoute4GEN3.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute4GEN3.TabIndex = 0;
         this.radioButtonRoute4GEN3.TabStop = true;
         this.radioButtonRoute4GEN3.Text = "unused";
         this.radioButtonRoute4GEN3.UseVisualStyleBackColor = true;
         this.radioButtonRoute4GEN3.Click += new System.EventHandler(this.radioButtonRoute4GEN0_Click);
         // 
         // radioButtonRoute4GEN2
         // 
         this.radioButtonRoute4GEN2.AutoSize = true;
         this.radioButtonRoute4GEN2.Enabled = false;
         this.radioButtonRoute4GEN2.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute4GEN2.Location = new System.Drawing.Point(8, 128);
         this.radioButtonRoute4GEN2.Name = "radioButtonRoute4GEN2";
         this.radioButtonRoute4GEN2.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute4GEN2.TabIndex = 0;
         this.radioButtonRoute4GEN2.TabStop = true;
         this.radioButtonRoute4GEN2.Text = "unused";
         this.radioButtonRoute4GEN2.UseVisualStyleBackColor = true;
         this.radioButtonRoute4GEN2.Click += new System.EventHandler(this.radioButtonRoute4GEN0_Click);
         // 
         // radioButtonRoute4GEN1
         // 
         this.radioButtonRoute4GEN1.AutoSize = true;
         this.radioButtonRoute4GEN1.Enabled = false;
         this.radioButtonRoute4GEN1.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute4GEN1.Location = new System.Drawing.Point(8, 112);
         this.radioButtonRoute4GEN1.Name = "radioButtonRoute4GEN1";
         this.radioButtonRoute4GEN1.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute4GEN1.TabIndex = 0;
         this.radioButtonRoute4GEN1.TabStop = true;
         this.radioButtonRoute4GEN1.Text = "unused";
         this.radioButtonRoute4GEN1.UseVisualStyleBackColor = true;
         this.radioButtonRoute4GEN1.Click += new System.EventHandler(this.radioButtonRoute4GEN0_Click);
         // 
         // radioButtonRoute4GEN0
         // 
         this.radioButtonRoute4GEN0.AutoSize = true;
         this.radioButtonRoute4GEN0.Location = new System.Drawing.Point(8, 96);
         this.radioButtonRoute4GEN0.Name = "radioButtonRoute4GEN0";
         this.radioButtonRoute4GEN0.Size = new System.Drawing.Size(80, 17);
         this.radioButtonRoute4GEN0.TabIndex = 0;
         this.radioButtonRoute4GEN0.TabStop = true;
         this.radioButtonRoute4GEN0.Text = "Sine 100Hz";
         this.radioButtonRoute4GEN0.UseVisualStyleBackColor = true;
         this.radioButtonRoute4GEN0.Click += new System.EventHandler(this.radioButtonRoute4GEN0_Click);
         // 
         // radioButtonRoute4AIN01
         // 
         this.radioButtonRoute4AIN01.AutoSize = true;
         this.radioButtonRoute4AIN01.Location = new System.Drawing.Point(8, 24);
         this.radioButtonRoute4AIN01.Name = "radioButtonRoute4AIN01";
         this.radioButtonRoute4AIN01.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute4AIN01.TabIndex = 0;
         this.radioButtonRoute4AIN01.TabStop = true;
         this.radioButtonRoute4AIN01.Text = "AN Input 0/1";
         this.radioButtonRoute4AIN01.UseVisualStyleBackColor = true;
         this.radioButtonRoute4AIN01.Click += new System.EventHandler(this.radioButtonRoute4AIN01_Click);
         // 
         // groupBox8
         // 
         this.groupBox8.Controls.Add(this.numericUpDownRoute3Chl2);
         this.groupBox8.Controls.Add(this.label13);
         this.groupBox8.Controls.Add(this.label9);
         this.groupBox8.Controls.Add(this.numericUpDownRoute3Chl1);
         this.groupBox8.Controls.Add(this.radioButtonRoute3Const);
         this.groupBox8.Controls.Add(this.radioButtonRoute3AIN67);
         this.groupBox8.Controls.Add(this.radioButtonRoute3AIN45);
         this.groupBox8.Controls.Add(this.radioButtonRoute3AIN23);
         this.groupBox8.Controls.Add(this.radioButtonRoute3GEN3);
         this.groupBox8.Controls.Add(this.radioButtonRoute3GEN2);
         this.groupBox8.Controls.Add(this.radioButtonRoute3GEN1);
         this.groupBox8.Controls.Add(this.radioButtonRoute3GEN0);
         this.groupBox8.Controls.Add(this.radioButtonRoute3AIN01);
         this.groupBox8.Location = new System.Drawing.Point(264, 16);
         this.groupBox8.Name = "groupBox8";
         this.groupBox8.Size = new System.Drawing.Size(120, 256);
         this.groupBox8.TabIndex = 0;
         this.groupBox8.TabStop = false;
         this.groupBox8.Text = "PSI5 Node #3";
         // 
         // numericUpDownRoute3Chl2
         // 
         this.numericUpDownRoute3Chl2.Hexadecimal = true;
         this.numericUpDownRoute3Chl2.Location = new System.Drawing.Point(32, 224);
         this.numericUpDownRoute3Chl2.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute3Chl2.Name = "numericUpDownRoute3Chl2";
         this.numericUpDownRoute3Chl2.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute3Chl2.TabIndex = 1;
         this.numericUpDownRoute3Chl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute3Chl2.Click += new System.EventHandler(this.numericUpDownRoute3Chl2_Click);
         // 
         // label13
         // 
         this.label13.AutoSize = true;
         this.label13.Location = new System.Drawing.Point(8, 224);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(18, 13);
         this.label13.TabIndex = 2;
         this.label13.Text = "0x";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(8, 200);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(18, 13);
         this.label9.TabIndex = 2;
         this.label9.Text = "0x";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // numericUpDownRoute3Chl1
         // 
         this.numericUpDownRoute3Chl1.Hexadecimal = true;
         this.numericUpDownRoute3Chl1.Location = new System.Drawing.Point(32, 200);
         this.numericUpDownRoute3Chl1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute3Chl1.Name = "numericUpDownRoute3Chl1";
         this.numericUpDownRoute3Chl1.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute3Chl1.TabIndex = 1;
         this.numericUpDownRoute3Chl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute3Chl1.Click += new System.EventHandler(this.numericUpDownRoute3Chl1_Click);
         // 
         // radioButtonRoute3Const
         // 
         this.radioButtonRoute3Const.AutoSize = true;
         this.radioButtonRoute3Const.Location = new System.Drawing.Point(8, 168);
         this.radioButtonRoute3Const.Name = "radioButtonRoute3Const";
         this.radioButtonRoute3Const.Size = new System.Drawing.Size(72, 17);
         this.radioButtonRoute3Const.TabIndex = 0;
         this.radioButtonRoute3Const.TabStop = true;
         this.radioButtonRoute3Const.Text = "Constants";
         this.radioButtonRoute3Const.UseVisualStyleBackColor = true;
         this.radioButtonRoute3Const.Click += new System.EventHandler(this.radioButtonRoute3Const_Click);
         // 
         // radioButtonRoute3AIN67
         // 
         this.radioButtonRoute3AIN67.AutoSize = true;
         this.radioButtonRoute3AIN67.Location = new System.Drawing.Point(8, 72);
         this.radioButtonRoute3AIN67.Name = "radioButtonRoute3AIN67";
         this.radioButtonRoute3AIN67.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute3AIN67.TabIndex = 0;
         this.radioButtonRoute3AIN67.TabStop = true;
         this.radioButtonRoute3AIN67.Text = "AN Input 6/7";
         this.radioButtonRoute3AIN67.UseVisualStyleBackColor = true;
         this.radioButtonRoute3AIN67.Click += new System.EventHandler(this.radioButtonRoute3AIN67_Click);
         // 
         // radioButtonRoute3AIN45
         // 
         this.radioButtonRoute3AIN45.AutoSize = true;
         this.radioButtonRoute3AIN45.Location = new System.Drawing.Point(8, 56);
         this.radioButtonRoute3AIN45.Name = "radioButtonRoute3AIN45";
         this.radioButtonRoute3AIN45.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute3AIN45.TabIndex = 0;
         this.radioButtonRoute3AIN45.TabStop = true;
         this.radioButtonRoute3AIN45.Text = "AN Input 4/5";
         this.radioButtonRoute3AIN45.UseVisualStyleBackColor = true;
         this.radioButtonRoute3AIN45.Click += new System.EventHandler(this.radioButtonRoute3AIN45_Click);
         // 
         // radioButtonRoute3AIN23
         // 
         this.radioButtonRoute3AIN23.AutoSize = true;
         this.radioButtonRoute3AIN23.Location = new System.Drawing.Point(8, 40);
         this.radioButtonRoute3AIN23.Name = "radioButtonRoute3AIN23";
         this.radioButtonRoute3AIN23.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute3AIN23.TabIndex = 0;
         this.radioButtonRoute3AIN23.TabStop = true;
         this.radioButtonRoute3AIN23.Text = "AN Input 2/3";
         this.radioButtonRoute3AIN23.UseVisualStyleBackColor = true;
         this.radioButtonRoute3AIN23.Click += new System.EventHandler(this.radioButtonRoute3AIN23_Click);
         // 
         // radioButtonRoute3GEN3
         // 
         this.radioButtonRoute3GEN3.AutoSize = true;
         this.radioButtonRoute3GEN3.Enabled = false;
         this.radioButtonRoute3GEN3.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute3GEN3.Location = new System.Drawing.Point(8, 144);
         this.radioButtonRoute3GEN3.Name = "radioButtonRoute3GEN3";
         this.radioButtonRoute3GEN3.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute3GEN3.TabIndex = 0;
         this.radioButtonRoute3GEN3.TabStop = true;
         this.radioButtonRoute3GEN3.Text = "unused";
         this.radioButtonRoute3GEN3.UseVisualStyleBackColor = true;
         this.radioButtonRoute3GEN3.Click += new System.EventHandler(this.radioButtonRoute3GEN0_Click);
         // 
         // radioButtonRoute3GEN2
         // 
         this.radioButtonRoute3GEN2.AutoSize = true;
         this.radioButtonRoute3GEN2.Enabled = false;
         this.radioButtonRoute3GEN2.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute3GEN2.Location = new System.Drawing.Point(8, 128);
         this.radioButtonRoute3GEN2.Name = "radioButtonRoute3GEN2";
         this.radioButtonRoute3GEN2.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute3GEN2.TabIndex = 0;
         this.radioButtonRoute3GEN2.TabStop = true;
         this.radioButtonRoute3GEN2.Text = "unused";
         this.radioButtonRoute3GEN2.UseVisualStyleBackColor = true;
         this.radioButtonRoute3GEN2.Click += new System.EventHandler(this.radioButtonRoute3GEN0_Click);
         // 
         // radioButtonRoute3GEN1
         // 
         this.radioButtonRoute3GEN1.AutoSize = true;
         this.radioButtonRoute3GEN1.Enabled = false;
         this.radioButtonRoute3GEN1.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute3GEN1.Location = new System.Drawing.Point(8, 112);
         this.radioButtonRoute3GEN1.Name = "radioButtonRoute3GEN1";
         this.radioButtonRoute3GEN1.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute3GEN1.TabIndex = 0;
         this.radioButtonRoute3GEN1.TabStop = true;
         this.radioButtonRoute3GEN1.Text = "unused";
         this.radioButtonRoute3GEN1.UseVisualStyleBackColor = true;
         this.radioButtonRoute3GEN1.Click += new System.EventHandler(this.radioButtonRoute3GEN0_Click);
         // 
         // radioButtonRoute3GEN0
         // 
         this.radioButtonRoute3GEN0.AutoSize = true;
         this.radioButtonRoute3GEN0.Location = new System.Drawing.Point(8, 96);
         this.radioButtonRoute3GEN0.Name = "radioButtonRoute3GEN0";
         this.radioButtonRoute3GEN0.Size = new System.Drawing.Size(80, 17);
         this.radioButtonRoute3GEN0.TabIndex = 0;
         this.radioButtonRoute3GEN0.TabStop = true;
         this.radioButtonRoute3GEN0.Text = "Sine 100Hz";
         this.radioButtonRoute3GEN0.UseVisualStyleBackColor = true;
         this.radioButtonRoute3GEN0.Click += new System.EventHandler(this.radioButtonRoute3GEN0_Click);
         // 
         // radioButtonRoute3AIN01
         // 
         this.radioButtonRoute3AIN01.AutoSize = true;
         this.radioButtonRoute3AIN01.Location = new System.Drawing.Point(8, 24);
         this.radioButtonRoute3AIN01.Name = "radioButtonRoute3AIN01";
         this.radioButtonRoute3AIN01.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute3AIN01.TabIndex = 0;
         this.radioButtonRoute3AIN01.TabStop = true;
         this.radioButtonRoute3AIN01.Text = "AN Input 0/1";
         this.radioButtonRoute3AIN01.UseVisualStyleBackColor = true;
         this.radioButtonRoute3AIN01.Click += new System.EventHandler(this.radioButtonRoute3AIN01_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.numericUpDownRoute2Chl2);
         this.groupBox1.Controls.Add(this.numericUpDownRoute2Chl1);
         this.groupBox1.Controls.Add(this.label15);
         this.groupBox1.Controls.Add(this.label14);
         this.groupBox1.Controls.Add(this.radioButtonRoute2Const);
         this.groupBox1.Controls.Add(this.radioButtonRoute2AIN67);
         this.groupBox1.Controls.Add(this.radioButtonRoute2AIN45);
         this.groupBox1.Controls.Add(this.radioButtonRoute2AIN23);
         this.groupBox1.Controls.Add(this.radioButtonRoute2GEN3);
         this.groupBox1.Controls.Add(this.radioButtonRoute2GEN2);
         this.groupBox1.Controls.Add(this.radioButtonRoute2GEN1);
         this.groupBox1.Controls.Add(this.radioButtonRoute2GEN0);
         this.groupBox1.Controls.Add(this.radioButtonRoute2AIN01);
         this.groupBox1.Location = new System.Drawing.Point(136, 16);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(120, 256);
         this.groupBox1.TabIndex = 0;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "PSI5 Node #2";
         // 
         // numericUpDownRoute2Chl2
         // 
         this.numericUpDownRoute2Chl2.Hexadecimal = true;
         this.numericUpDownRoute2Chl2.Location = new System.Drawing.Point(32, 224);
         this.numericUpDownRoute2Chl2.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute2Chl2.Name = "numericUpDownRoute2Chl2";
         this.numericUpDownRoute2Chl2.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute2Chl2.TabIndex = 1;
         this.numericUpDownRoute2Chl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute2Chl2.Click += new System.EventHandler(this.numericUpDownRoute2Chl2_Click);
         // 
         // numericUpDownRoute2Chl1
         // 
         this.numericUpDownRoute2Chl1.Hexadecimal = true;
         this.numericUpDownRoute2Chl1.Location = new System.Drawing.Point(32, 200);
         this.numericUpDownRoute2Chl1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute2Chl1.Name = "numericUpDownRoute2Chl1";
         this.numericUpDownRoute2Chl1.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute2Chl1.TabIndex = 1;
         this.numericUpDownRoute2Chl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute2Chl1.Click += new System.EventHandler(this.numericUpDownRoute2Chl1_Click);
         // 
         // label15
         // 
         this.label15.AutoSize = true;
         this.label15.Location = new System.Drawing.Point(8, 224);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(18, 13);
         this.label15.TabIndex = 2;
         this.label15.Text = "0x";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label14
         // 
         this.label14.AutoSize = true;
         this.label14.Location = new System.Drawing.Point(8, 200);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(18, 13);
         this.label14.TabIndex = 2;
         this.label14.Text = "0x";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // radioButtonRoute2Const
         // 
         this.radioButtonRoute2Const.AutoSize = true;
         this.radioButtonRoute2Const.Location = new System.Drawing.Point(8, 168);
         this.radioButtonRoute2Const.Name = "radioButtonRoute2Const";
         this.radioButtonRoute2Const.Size = new System.Drawing.Size(72, 17);
         this.radioButtonRoute2Const.TabIndex = 0;
         this.radioButtonRoute2Const.TabStop = true;
         this.radioButtonRoute2Const.Text = "Constants";
         this.radioButtonRoute2Const.UseVisualStyleBackColor = true;
         this.radioButtonRoute2Const.Click += new System.EventHandler(this.radioButtonRoute2Const_Click);
         // 
         // radioButtonRoute2AIN67
         // 
         this.radioButtonRoute2AIN67.AutoSize = true;
         this.radioButtonRoute2AIN67.Location = new System.Drawing.Point(8, 72);
         this.radioButtonRoute2AIN67.Name = "radioButtonRoute2AIN67";
         this.radioButtonRoute2AIN67.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute2AIN67.TabIndex = 0;
         this.radioButtonRoute2AIN67.TabStop = true;
         this.radioButtonRoute2AIN67.Text = "AN Input 6/7";
         this.radioButtonRoute2AIN67.UseVisualStyleBackColor = true;
         this.radioButtonRoute2AIN67.Click += new System.EventHandler(this.radioButtonRoute2AIN67_Click);
         // 
         // radioButtonRoute2AIN45
         // 
         this.radioButtonRoute2AIN45.AutoSize = true;
         this.radioButtonRoute2AIN45.Location = new System.Drawing.Point(8, 56);
         this.radioButtonRoute2AIN45.Name = "radioButtonRoute2AIN45";
         this.radioButtonRoute2AIN45.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute2AIN45.TabIndex = 0;
         this.radioButtonRoute2AIN45.TabStop = true;
         this.radioButtonRoute2AIN45.Text = "AN Input 4/5";
         this.radioButtonRoute2AIN45.UseVisualStyleBackColor = true;
         this.radioButtonRoute2AIN45.Click += new System.EventHandler(this.radioButtonRoute2AIN45_Click);
         // 
         // radioButtonRoute2AIN23
         // 
         this.radioButtonRoute2AIN23.AutoSize = true;
         this.radioButtonRoute2AIN23.Location = new System.Drawing.Point(8, 40);
         this.radioButtonRoute2AIN23.Name = "radioButtonRoute2AIN23";
         this.radioButtonRoute2AIN23.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute2AIN23.TabIndex = 0;
         this.radioButtonRoute2AIN23.TabStop = true;
         this.radioButtonRoute2AIN23.Text = "AN Input 2/3";
         this.radioButtonRoute2AIN23.UseVisualStyleBackColor = true;
         this.radioButtonRoute2AIN23.Click += new System.EventHandler(this.radioButtonRoute2AIN23_Click);
         // 
         // radioButtonRoute2GEN3
         // 
         this.radioButtonRoute2GEN3.AutoSize = true;
         this.radioButtonRoute2GEN3.Enabled = false;
         this.radioButtonRoute2GEN3.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute2GEN3.Location = new System.Drawing.Point(8, 144);
         this.radioButtonRoute2GEN3.Name = "radioButtonRoute2GEN3";
         this.radioButtonRoute2GEN3.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute2GEN3.TabIndex = 0;
         this.radioButtonRoute2GEN3.TabStop = true;
         this.radioButtonRoute2GEN3.Text = "unused";
         this.radioButtonRoute2GEN3.UseVisualStyleBackColor = true;
         this.radioButtonRoute2GEN3.Click += new System.EventHandler(this.radioButtonRoute2GEN0_Click);
         // 
         // radioButtonRoute2GEN2
         // 
         this.radioButtonRoute2GEN2.AutoSize = true;
         this.radioButtonRoute2GEN2.Enabled = false;
         this.radioButtonRoute2GEN2.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute2GEN2.Location = new System.Drawing.Point(8, 128);
         this.radioButtonRoute2GEN2.Name = "radioButtonRoute2GEN2";
         this.radioButtonRoute2GEN2.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute2GEN2.TabIndex = 0;
         this.radioButtonRoute2GEN2.TabStop = true;
         this.radioButtonRoute2GEN2.Text = "unused";
         this.radioButtonRoute2GEN2.UseVisualStyleBackColor = true;
         this.radioButtonRoute2GEN2.Click += new System.EventHandler(this.radioButtonRoute2GEN0_Click);
         // 
         // radioButtonRoute2GEN1
         // 
         this.radioButtonRoute2GEN1.AutoSize = true;
         this.radioButtonRoute2GEN1.Enabled = false;
         this.radioButtonRoute2GEN1.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute2GEN1.Location = new System.Drawing.Point(8, 112);
         this.radioButtonRoute2GEN1.Name = "radioButtonRoute2GEN1";
         this.radioButtonRoute2GEN1.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute2GEN1.TabIndex = 0;
         this.radioButtonRoute2GEN1.TabStop = true;
         this.radioButtonRoute2GEN1.Text = "unused";
         this.radioButtonRoute2GEN1.UseVisualStyleBackColor = true;
         this.radioButtonRoute2GEN1.Click += new System.EventHandler(this.radioButtonRoute2GEN0_Click);
         // 
         // radioButtonRoute2GEN0
         // 
         this.radioButtonRoute2GEN0.AutoSize = true;
         this.radioButtonRoute2GEN0.Location = new System.Drawing.Point(8, 96);
         this.radioButtonRoute2GEN0.Name = "radioButtonRoute2GEN0";
         this.radioButtonRoute2GEN0.Size = new System.Drawing.Size(80, 17);
         this.radioButtonRoute2GEN0.TabIndex = 0;
         this.radioButtonRoute2GEN0.TabStop = true;
         this.radioButtonRoute2GEN0.Text = "Sine 100Hz";
         this.radioButtonRoute2GEN0.UseVisualStyleBackColor = true;
         this.radioButtonRoute2GEN0.Click += new System.EventHandler(this.radioButtonRoute2GEN0_Click);
         // 
         // radioButtonRoute2AIN01
         // 
         this.radioButtonRoute2AIN01.AutoSize = true;
         this.radioButtonRoute2AIN01.Location = new System.Drawing.Point(8, 24);
         this.radioButtonRoute2AIN01.Name = "radioButtonRoute2AIN01";
         this.radioButtonRoute2AIN01.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute2AIN01.TabIndex = 0;
         this.radioButtonRoute2AIN01.TabStop = true;
         this.radioButtonRoute2AIN01.Text = "AN Input 0/1";
         this.radioButtonRoute2AIN01.UseVisualStyleBackColor = true;
         this.radioButtonRoute2AIN01.Click += new System.EventHandler(this.radioButtonRoute2AIN01_Click);
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.numericUpDownRoute1Chl2);
         this.groupBox2.Controls.Add(this.numericUpDownRoute1Chl1);
         this.groupBox2.Controls.Add(this.label17);
         this.groupBox2.Controls.Add(this.label16);
         this.groupBox2.Controls.Add(this.radioButtonRoute1Const);
         this.groupBox2.Controls.Add(this.radioButtonRoute1AIN67);
         this.groupBox2.Controls.Add(this.radioButtonRoute1AIN45);
         this.groupBox2.Controls.Add(this.radioButtonRoute1AIN23);
         this.groupBox2.Controls.Add(this.radioButtonRoute1GEN3);
         this.groupBox2.Controls.Add(this.radioButtonRoute1GEN2);
         this.groupBox2.Controls.Add(this.radioButtonRoute1GEN1);
         this.groupBox2.Controls.Add(this.radioButtonRoute1GEN0);
         this.groupBox2.Controls.Add(this.radioButtonRoute1AIN01);
         this.groupBox2.Location = new System.Drawing.Point(8, 16);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(120, 256);
         this.groupBox2.TabIndex = 0;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "PSI5 Node #1";
         // 
         // numericUpDownRoute1Chl2
         // 
         this.numericUpDownRoute1Chl2.Hexadecimal = true;
         this.numericUpDownRoute1Chl2.Location = new System.Drawing.Point(32, 224);
         this.numericUpDownRoute1Chl2.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute1Chl2.Name = "numericUpDownRoute1Chl2";
         this.numericUpDownRoute1Chl2.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute1Chl2.TabIndex = 1;
         this.numericUpDownRoute1Chl2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute1Chl2.Click += new System.EventHandler(this.numericUpDownRoute1Chl2_Click);
         // 
         // numericUpDownRoute1Chl1
         // 
         this.numericUpDownRoute1Chl1.Hexadecimal = true;
         this.numericUpDownRoute1Chl1.Location = new System.Drawing.Point(32, 200);
         this.numericUpDownRoute1Chl1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
         this.numericUpDownRoute1Chl1.Name = "numericUpDownRoute1Chl1";
         this.numericUpDownRoute1Chl1.Size = new System.Drawing.Size(64, 20);
         this.numericUpDownRoute1Chl1.TabIndex = 1;
         this.numericUpDownRoute1Chl1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         this.numericUpDownRoute1Chl1.Click += new System.EventHandler(this.numericUpDownRoute1Chl1_Click);
         // 
         // label17
         // 
         this.label17.AutoSize = true;
         this.label17.Location = new System.Drawing.Point(8, 224);
         this.label17.Name = "label17";
         this.label17.Size = new System.Drawing.Size(18, 13);
         this.label17.TabIndex = 2;
         this.label17.Text = "0x";
         this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label16
         // 
         this.label16.AutoSize = true;
         this.label16.Location = new System.Drawing.Point(8, 200);
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size(18, 13);
         this.label16.TabIndex = 2;
         this.label16.Text = "0x";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // radioButtonRoute1Const
         // 
         this.radioButtonRoute1Const.AutoSize = true;
         this.radioButtonRoute1Const.Location = new System.Drawing.Point(8, 168);
         this.radioButtonRoute1Const.Name = "radioButtonRoute1Const";
         this.radioButtonRoute1Const.Size = new System.Drawing.Size(72, 17);
         this.radioButtonRoute1Const.TabIndex = 0;
         this.radioButtonRoute1Const.TabStop = true;
         this.radioButtonRoute1Const.Text = "Constants";
         this.radioButtonRoute1Const.UseVisualStyleBackColor = true;
         this.radioButtonRoute1Const.Click += new System.EventHandler(this.radioButtonRoute1Const_Click);
         // 
         // radioButtonRoute1AIN67
         // 
         this.radioButtonRoute1AIN67.AutoSize = true;
         this.radioButtonRoute1AIN67.Location = new System.Drawing.Point(8, 72);
         this.radioButtonRoute1AIN67.Name = "radioButtonRoute1AIN67";
         this.radioButtonRoute1AIN67.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute1AIN67.TabIndex = 0;
         this.radioButtonRoute1AIN67.TabStop = true;
         this.radioButtonRoute1AIN67.Text = "AN Input 6/7";
         this.radioButtonRoute1AIN67.UseVisualStyleBackColor = true;
         this.radioButtonRoute1AIN67.Click += new System.EventHandler(this.radioButtonRoute1AIN67_Click);
         // 
         // radioButtonRoute1AIN45
         // 
         this.radioButtonRoute1AIN45.AutoSize = true;
         this.radioButtonRoute1AIN45.Location = new System.Drawing.Point(8, 56);
         this.radioButtonRoute1AIN45.Name = "radioButtonRoute1AIN45";
         this.radioButtonRoute1AIN45.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute1AIN45.TabIndex = 0;
         this.radioButtonRoute1AIN45.TabStop = true;
         this.radioButtonRoute1AIN45.Text = "AN Input 4/5";
         this.radioButtonRoute1AIN45.UseVisualStyleBackColor = true;
         this.radioButtonRoute1AIN45.Click += new System.EventHandler(this.radioButtonRoute1AIN45_Click);
         // 
         // radioButtonRoute1AIN23
         // 
         this.radioButtonRoute1AIN23.AutoSize = true;
         this.radioButtonRoute1AIN23.Location = new System.Drawing.Point(8, 40);
         this.radioButtonRoute1AIN23.Name = "radioButtonRoute1AIN23";
         this.radioButtonRoute1AIN23.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute1AIN23.TabIndex = 0;
         this.radioButtonRoute1AIN23.TabStop = true;
         this.radioButtonRoute1AIN23.Text = "AN Input 2/3";
         this.radioButtonRoute1AIN23.UseVisualStyleBackColor = true;
         this.radioButtonRoute1AIN23.Click += new System.EventHandler(this.radioButtonRoute1AIN23_Click);
         // 
         // radioButtonRoute1GEN3
         // 
         this.radioButtonRoute1GEN3.AutoSize = true;
         this.radioButtonRoute1GEN3.Enabled = false;
         this.radioButtonRoute1GEN3.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute1GEN3.Location = new System.Drawing.Point(8, 144);
         this.radioButtonRoute1GEN3.Name = "radioButtonRoute1GEN3";
         this.radioButtonRoute1GEN3.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute1GEN3.TabIndex = 0;
         this.radioButtonRoute1GEN3.TabStop = true;
         this.radioButtonRoute1GEN3.Text = "unused";
         this.radioButtonRoute1GEN3.UseVisualStyleBackColor = true;
         this.radioButtonRoute1GEN3.Click += new System.EventHandler(this.radioButtonRoute1GEN0_Click);
         // 
         // radioButtonRoute1GEN2
         // 
         this.radioButtonRoute1GEN2.AutoSize = true;
         this.radioButtonRoute1GEN2.Enabled = false;
         this.radioButtonRoute1GEN2.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute1GEN2.Location = new System.Drawing.Point(8, 128);
         this.radioButtonRoute1GEN2.Name = "radioButtonRoute1GEN2";
         this.radioButtonRoute1GEN2.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute1GEN2.TabIndex = 0;
         this.radioButtonRoute1GEN2.TabStop = true;
         this.radioButtonRoute1GEN2.Text = "unused";
         this.radioButtonRoute1GEN2.UseVisualStyleBackColor = true;
         this.radioButtonRoute1GEN2.Click += new System.EventHandler(this.radioButtonRoute1GEN0_Click);
         // 
         // radioButtonRoute1GEN1
         // 
         this.radioButtonRoute1GEN1.AutoSize = true;
         this.radioButtonRoute1GEN1.Enabled = false;
         this.radioButtonRoute1GEN1.ForeColor = System.Drawing.SystemColors.ControlLight;
         this.radioButtonRoute1GEN1.Location = new System.Drawing.Point(8, 112);
         this.radioButtonRoute1GEN1.Name = "radioButtonRoute1GEN1";
         this.radioButtonRoute1GEN1.Size = new System.Drawing.Size(60, 17);
         this.radioButtonRoute1GEN1.TabIndex = 0;
         this.radioButtonRoute1GEN1.TabStop = true;
         this.radioButtonRoute1GEN1.Text = "unused";
         this.radioButtonRoute1GEN1.UseVisualStyleBackColor = true;
         this.radioButtonRoute1GEN1.Click += new System.EventHandler(this.radioButtonRoute1GEN0_Click);
         // 
         // radioButtonRoute1GEN0
         // 
         this.radioButtonRoute1GEN0.AutoSize = true;
         this.radioButtonRoute1GEN0.Location = new System.Drawing.Point(8, 96);
         this.radioButtonRoute1GEN0.Name = "radioButtonRoute1GEN0";
         this.radioButtonRoute1GEN0.Size = new System.Drawing.Size(80, 17);
         this.radioButtonRoute1GEN0.TabIndex = 0;
         this.radioButtonRoute1GEN0.TabStop = true;
         this.radioButtonRoute1GEN0.Text = "Sine 100Hz";
         this.radioButtonRoute1GEN0.UseVisualStyleBackColor = true;
         this.radioButtonRoute1GEN0.Click += new System.EventHandler(this.radioButtonRoute1GEN0_Click);
         // 
         // radioButtonRoute1AIN01
         // 
         this.radioButtonRoute1AIN01.AutoSize = true;
         this.radioButtonRoute1AIN01.Location = new System.Drawing.Point(8, 24);
         this.radioButtonRoute1AIN01.Name = "radioButtonRoute1AIN01";
         this.radioButtonRoute1AIN01.Size = new System.Drawing.Size(87, 17);
         this.radioButtonRoute1AIN01.TabIndex = 0;
         this.radioButtonRoute1AIN01.TabStop = true;
         this.radioButtonRoute1AIN01.Text = "AN Input 0/1";
         this.radioButtonRoute1AIN01.UseVisualStyleBackColor = true;
         this.radioButtonRoute1AIN01.Click += new System.EventHandler(this.radioButtonRoute1AIN01_Click);
         // 
         // tabSetup
         // 
         this.tabSetup.BackColor = System.Drawing.SystemColors.Menu;
         this.tabSetup.Controls.Add(this.label29);
         this.tabSetup.Controls.Add(this.listBoxSetFiles);
         this.tabSetup.Controls.Add(this.buttonSetSave);
         this.tabSetup.Controls.Add(this.buttonSetLoad);
         this.tabSetup.Controls.Add(this.label26);
         this.tabSetup.Controls.Add(this.pictureBox6);
         this.tabSetup.Location = new System.Drawing.Point(4, 22);
         this.tabSetup.Name = "tabSetup";
         this.tabSetup.Padding = new System.Windows.Forms.Padding(3);
         this.tabSetup.Size = new System.Drawing.Size(520, 366);
         this.tabSetup.TabIndex = 4;
         this.tabSetup.Text = "Setups";
         // 
         // label29
         // 
         this.label29.Location = new System.Drawing.Point(16, 16);
         this.label29.Name = "label29";
         this.label29.Size = new System.Drawing.Size(488, 23);
         this.label29.TabIndex = 11;
         this.label29.Text = "Previous used setup file:";
         this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // listBoxSetFiles
         // 
         this.listBoxSetFiles.FormattingEnabled = true;
         this.listBoxSetFiles.HorizontalScrollbar = true;
         this.listBoxSetFiles.Location = new System.Drawing.Point(16, 40);
         this.listBoxSetFiles.Name = "listBoxSetFiles";
         this.listBoxSetFiles.Size = new System.Drawing.Size(488, 212);
         this.listBoxSetFiles.TabIndex = 10;
         this.listBoxSetFiles.DoubleClick += new System.EventHandler(this.listBoxSetFiles_DoubleClick);
         // 
         // buttonSetSave
         // 
         this.buttonSetSave.Location = new System.Drawing.Point(384, 264);
         this.buttonSetSave.Name = "buttonSetSave";
         this.buttonSetSave.RightToLeft = System.Windows.Forms.RightToLeft.No;
         this.buttonSetSave.Size = new System.Drawing.Size(120, 40);
         this.buttonSetSave.TabIndex = 9;
         this.buttonSetSave.Text = "Save Setup File";
         this.buttonSetSave.UseVisualStyleBackColor = true;
         this.buttonSetSave.Click += new System.EventHandler(this.buttonSetSave_Click);
         // 
         // buttonSetLoad
         // 
         this.buttonSetLoad.Location = new System.Drawing.Point(16, 264);
         this.buttonSetLoad.Name = "buttonSetLoad";
         this.buttonSetLoad.Size = new System.Drawing.Size(120, 40);
         this.buttonSetLoad.TabIndex = 9;
         this.buttonSetLoad.Text = "Load Setup File";
         this.buttonSetLoad.UseVisualStyleBackColor = true;
         this.buttonSetLoad.Click += new System.EventHandler(this.buttonSetLoad_Click);
         // 
         // label26
         // 
         this.label26.BackColor = System.Drawing.Color.Transparent;
         this.label26.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label26.Location = new System.Drawing.Point(48, 320);
         this.label26.Name = "label26";
         this.label26.Size = new System.Drawing.Size(416, 40);
         this.label26.TabIndex = 8;
         this.label26.Text = "Here you can store or load the settings of the GUI. Double click on a previous se" +
             "tup file to load it.";
         this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // pictureBox6
         // 
         this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
         this.pictureBox6.Location = new System.Drawing.Point(8, 320);
         this.pictureBox6.Name = "pictureBox6";
         this.pictureBox6.Size = new System.Drawing.Size(40, 40);
         this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox6.TabIndex = 7;
         this.pictureBox6.TabStop = false;
         // 
         // tabDebug
         // 
         this.tabDebug.BackColor = System.Drawing.SystemColors.Menu;
         this.tabDebug.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
         this.tabDebug.Controls.Add(this.buttonDebugClear);
         this.tabDebug.Controls.Add(this.label4);
         this.tabDebug.Controls.Add(this.pictureBox3);
         this.tabDebug.Controls.Add(this.richTextBoxDebug);
         this.tabDebug.Location = new System.Drawing.Point(4, 22);
         this.tabDebug.Name = "tabDebug";
         this.tabDebug.Padding = new System.Windows.Forms.Padding(3);
         this.tabDebug.Size = new System.Drawing.Size(520, 366);
         this.tabDebug.TabIndex = 2;
         this.tabDebug.Text = "Debug";
         // 
         // buttonDebugClear
         // 
         this.buttonDebugClear.Location = new System.Drawing.Point(440, 320);
         this.buttonDebugClear.Name = "buttonDebugClear";
         this.buttonDebugClear.Size = new System.Drawing.Size(72, 40);
         this.buttonDebugClear.TabIndex = 7;
         this.buttonDebugClear.Text = "&Clear View";
         this.buttonDebugClear.UseVisualStyleBackColor = true;
         this.buttonDebugClear.Click += new System.EventHandler(this.buttonDebugClear_Click);
         // 
         // label4
         // 
         this.label4.BackColor = System.Drawing.Color.Transparent;
         this.label4.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
         this.label4.Location = new System.Drawing.Point(56, 320);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(376, 40);
         this.label4.TabIndex = 6;
         this.label4.Text = "You can see the communication between the GUI and the connected device here. This" +
             " window is very helpful for developers to find communication issues.";
         // 
         // pictureBox3
         // 
         this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
         this.pictureBox3.Location = new System.Drawing.Point(8, 320);
         this.pictureBox3.Name = "pictureBox3";
         this.pictureBox3.Size = new System.Drawing.Size(40, 40);
         this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox3.TabIndex = 5;
         this.pictureBox3.TabStop = false;
         // 
         // richTextBoxDebug
         // 
         this.richTextBoxDebug.BackColor = System.Drawing.Color.Gray;
         this.richTextBoxDebug.ForeColor = System.Drawing.Color.White;
         this.richTextBoxDebug.Location = new System.Drawing.Point(8, 8);
         this.richTextBoxDebug.Name = "richTextBoxDebug";
         this.richTextBoxDebug.ReadOnly = true;
         this.richTextBoxDebug.Size = new System.Drawing.Size(504, 304);
         this.richTextBoxDebug.TabIndex = 0;
         this.richTextBoxDebug.Text = "";
         // 
         // serialPort
         // 
         this.serialPort.BaudRate = 921600;
         this.serialPort.ReadBufferSize = 1;
         this.serialPort.WriteBufferSize = 64;
         this.serialPort.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(this.serialPort_ErrorReceived);
         this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
         // 
         // tabDevInfo
         // 
         this.tabDevInfo.BackColor = System.Drawing.SystemColors.Menu;
         this.tabDevInfo.Controls.Add(this.groupBox10);
         this.tabDevInfo.Controls.Add(this.groupBox4);
         this.tabDevInfo.Controls.Add(this.buttonDevUpdate);
         this.tabDevInfo.Controls.Add(this.labelDevSens1);
         this.tabDevInfo.Controls.Add(this.labelDevSens3);
         this.tabDevInfo.Controls.Add(this.labelDevSens2);
         this.tabDevInfo.Controls.Add(this.labelDevSens4);
         this.tabDevInfo.Controls.Add(this.label30);
         this.tabDevInfo.Controls.Add(this.pictureBox7);
         this.tabDevInfo.Location = new System.Drawing.Point(4, 22);
         this.tabDevInfo.Name = "tabDevInfo";
         this.tabDevInfo.Size = new System.Drawing.Size(520, 366);
         this.tabDevInfo.TabIndex = 5;
         this.tabDevInfo.Text = "Device";
         // 
         // pictureBox7
         // 
         this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
         this.pictureBox7.Location = new System.Drawing.Point(8, 320);
         this.pictureBox7.Name = "pictureBox7";
         this.pictureBox7.Size = new System.Drawing.Size(40, 40);
         this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.pictureBox7.TabIndex = 8;
         this.pictureBox7.TabStop = false;
         // 
         // label30
         // 
         this.label30.BackColor = System.Drawing.Color.Transparent;
         this.label30.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label30.Location = new System.Drawing.Point(48, 320);
         this.label30.Name = "label30";
         this.label30.Size = new System.Drawing.Size(416, 40);
         this.label30.TabIndex = 9;
         this.label30.Text = "In this tab, you can trigger or read certain device functionality. For example re" +
             "set the device.";
         this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // labelDevSens1
         // 
         this.labelDevSens1.BackColor = System.Drawing.Color.Gray;
         this.labelDevSens1.Location = new System.Drawing.Point(480, 328);
         this.labelDevSens1.Name = "labelDevSens1";
         this.labelDevSens1.Size = new System.Drawing.Size(7, 23);
         this.labelDevSens1.TabIndex = 17;
         // 
         // labelDevSens3
         // 
         this.labelDevSens3.BackColor = System.Drawing.Color.Gray;
         this.labelDevSens3.Location = new System.Drawing.Point(496, 328);
         this.labelDevSens3.Name = "labelDevSens3";
         this.labelDevSens3.Size = new System.Drawing.Size(7, 23);
         this.labelDevSens3.TabIndex = 18;
         // 
         // labelDevSens2
         // 
         this.labelDevSens2.BackColor = System.Drawing.Color.Gray;
         this.labelDevSens2.Location = new System.Drawing.Point(488, 328);
         this.labelDevSens2.Name = "labelDevSens2";
         this.labelDevSens2.Size = new System.Drawing.Size(7, 23);
         this.labelDevSens2.TabIndex = 15;
         // 
         // labelDevSens4
         // 
         this.labelDevSens4.BackColor = System.Drawing.Color.Gray;
         this.labelDevSens4.Location = new System.Drawing.Point(504, 328);
         this.labelDevSens4.Name = "labelDevSens4";
         this.labelDevSens4.Size = new System.Drawing.Size(7, 23);
         this.labelDevSens4.TabIndex = 16;
         // 
         // buttonDevUpdate
         // 
         this.buttonDevUpdate.Location = new System.Drawing.Point(200, 264);
         this.buttonDevUpdate.Name = "buttonDevUpdate";
         this.buttonDevUpdate.Size = new System.Drawing.Size(120, 32);
         this.buttonDevUpdate.TabIndex = 19;
         this.buttonDevUpdate.Text = "&Update";
         this.buttonDevUpdate.UseVisualStyleBackColor = true;
         this.buttonDevUpdate.Click += new System.EventHandler(this.buttonDevUpdate_Click);
         // 
         // buttonDevReset
         // 
         this.buttonDevReset.Location = new System.Drawing.Point(368, 16);
         this.buttonDevReset.Name = "buttonDevReset";
         this.buttonDevReset.Size = new System.Drawing.Size(112, 64);
         this.buttonDevReset.TabIndex = 19;
         this.buttonDevReset.Text = "&Reset the device and all its components";
         this.buttonDevReset.UseVisualStyleBackColor = true;
         this.buttonDevReset.Click += new System.EventHandler(this.buttonDevReset_Click);
         // 
         // groupBox4
         // 
         this.groupBox4.Controls.Add(this.label31);
         this.groupBox4.Controls.Add(this.buttonDevReset);
         this.groupBox4.Location = new System.Drawing.Point(16, 72);
         this.groupBox4.Name = "groupBox4";
         this.groupBox4.Size = new System.Drawing.Size(488, 88);
         this.groupBox4.TabIndex = 20;
         this.groupBox4.TabStop = false;
         this.groupBox4.Text = "Reset the Device";
         // 
         // label31
         // 
         this.label31.BackColor = System.Drawing.Color.Transparent;
         this.label31.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
         this.label31.Location = new System.Drawing.Point(8, 24);
         this.label31.Name = "label31";
         this.label31.Size = new System.Drawing.Size(336, 40);
         this.label31.TabIndex = 21;
         this.label31.Text = "After you press the reset button on the right, the device will be reset within 2 " +
             "seconds.";
         this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
         // 
         // groupBox10
         // 
         this.groupBox10.Controls.Add(this.labelDeviceVersion);
         this.groupBox10.Location = new System.Drawing.Point(16, 8);
         this.groupBox10.Name = "groupBox10";
         this.groupBox10.Size = new System.Drawing.Size(488, 56);
         this.groupBox10.TabIndex = 21;
         this.groupBox10.TabStop = false;
         this.groupBox10.Text = "Version Information";
         // 
         // labelDeviceVersion
         // 
         this.labelDeviceVersion.Location = new System.Drawing.Point(8, 24);
         this.labelDeviceVersion.Name = "labelDeviceVersion";
         this.labelDeviceVersion.Size = new System.Drawing.Size(464, 24);
         this.labelDeviceVersion.TabIndex = 0;
         this.labelDeviceVersion.Text = "-";
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.BackColor = System.Drawing.Color.White;
         this.ClientSize = new System.Drawing.Size(527, 392);
         this.Controls.Add(this.tabApp);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.Name = "Form1";
         this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
         this.Text = "Sensor Simulator GUI";
         this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
         this.tabApp.ResumeLayout(false);
         this.tabConn.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
         this.tabSensorType.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
         this.groupBox7.ResumeLayout(false);
         this.groupBox7.PerformLayout();
         this.groupBox6.ResumeLayout(false);
         this.groupBox6.PerformLayout();
         this.groupBox3.ResumeLayout(false);
         this.groupBox3.PerformLayout();
         this.groupBox5.ResumeLayout(false);
         this.groupBox5.PerformLayout();
         this.tabSensorData.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
         this.groupBox9.ResumeLayout(false);
         this.groupBox9.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute4Chl2)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute4Chl1)).EndInit();
         this.groupBox8.ResumeLayout(false);
         this.groupBox8.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute3Chl2)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute3Chl1)).EndInit();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute2Chl2)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute2Chl1)).EndInit();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute1Chl2)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRoute1Chl1)).EndInit();
         this.tabSetup.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
         this.tabDebug.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
         this.tabDevInfo.ResumeLayout(false);
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
         this.groupBox4.ResumeLayout(false);
         this.groupBox10.ResumeLayout(false);
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.TabControl tabApp;
      private System.Windows.Forms.TabPage tabConn;
      private System.Windows.Forms.TabPage tabSensorData;
      private System.Windows.Forms.ListBox listBoxConnInterface;
      private System.Windows.Forms.Button buttonConn;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.Button buttonConnUpdate;
      private System.IO.Ports.SerialPort serialPort;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.PictureBox pictureBox2;
      private System.Windows.Forms.Label labelRouteInfo;
      private System.Windows.Forms.Button buttonVerRead;
      private System.Windows.Forms.TabPage tabDebug;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.PictureBox pictureBox3;
      private System.Windows.Forms.RichTextBox richTextBoxDebug;
      private System.Windows.Forms.Button buttonDebugClear;
      private System.Windows.Forms.TabPage tabSensorType;
      private System.Windows.Forms.Label labelNode1Info;
      private System.Windows.Forms.PictureBox pictureBox4;
      private System.Windows.Forms.GroupBox groupBox5;
      private System.Windows.Forms.Button button1Update;
      private System.Windows.Forms.RadioButton radioButton1SensorRNS20;
      private System.Windows.Forms.RadioButton radioButton1SensorF3;
      private System.Windows.Forms.RadioButton radioButton1SensorF1F2;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute1Chl2;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute1Chl1;
      private System.Windows.Forms.RadioButton radioButtonRoute1Const;
      private System.Windows.Forms.RadioButton radioButtonRoute1AIN67;
      private System.Windows.Forms.RadioButton radioButtonRoute1AIN45;
      private System.Windows.Forms.RadioButton radioButtonRoute1AIN23;
      private System.Windows.Forms.RadioButton radioButtonRoute1AIN01;
      private System.Windows.Forms.GroupBox groupBox9;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute4Chl2;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute4Chl1;
      private System.Windows.Forms.RadioButton radioButtonRoute4Const;
      private System.Windows.Forms.RadioButton radioButtonRoute4AIN67;
      private System.Windows.Forms.RadioButton radioButtonRoute4AIN45;
      private System.Windows.Forms.RadioButton radioButtonRoute4AIN23;
      private System.Windows.Forms.RadioButton radioButtonRoute4AIN01;
      private System.Windows.Forms.GroupBox groupBox8;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute3Chl2;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute3Chl1;
      private System.Windows.Forms.RadioButton radioButtonRoute3Const;
      private System.Windows.Forms.RadioButton radioButtonRoute3AIN67;
      private System.Windows.Forms.RadioButton radioButtonRoute3AIN45;
      private System.Windows.Forms.RadioButton radioButtonRoute3AIN23;
      private System.Windows.Forms.RadioButton radioButtonRoute3AIN01;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute2Chl2;
      private System.Windows.Forms.NumericUpDown numericUpDownRoute2Chl1;
      private System.Windows.Forms.RadioButton radioButtonRoute2Const;
      private System.Windows.Forms.RadioButton radioButtonRoute2AIN67;
      private System.Windows.Forms.RadioButton radioButtonRoute2AIN45;
      private System.Windows.Forms.RadioButton radioButtonRoute2AIN23;
      private System.Windows.Forms.RadioButton radioButtonRoute2AIN01;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.GroupBox groupBox7;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.RadioButton radioButton4SensorRNS20;
      private System.Windows.Forms.RadioButton radioButton4SensorF3;
      private System.Windows.Forms.RadioButton radioButton4SensorF1F2;
      private System.Windows.Forms.GroupBox groupBox6;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.RadioButton radioButton3SensorRNS20;
      private System.Windows.Forms.RadioButton radioButton3SensorF3;
      private System.Windows.Forms.RadioButton radioButton3SensorF1F2;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.RadioButton radioButton2SensorRNS20;
      private System.Windows.Forms.RadioButton radioButton2SensorF3;
      private System.Windows.Forms.RadioButton radioButton2SensorF1F2;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.RadioButton radioButton1SensorOFF;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.RadioButton radioButton4SensorOFF;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.RadioButton radioButton3SensorOFF;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.RadioButton radioButton2SensorOFF;
      private System.Windows.Forms.PictureBox pictureBox5;
      private System.Windows.Forms.Label labelTypeSens1;
      private System.Windows.Forms.Label labelTypeSens3;
      private System.Windows.Forms.Label labelTypeSens2;
      private System.Windows.Forms.Label labelTypeSens4;
      private System.Windows.Forms.Label labelDataSens1;
      private System.Windows.Forms.Label labelDataSens3;
      private System.Windows.Forms.Label labelDataSens2;
      private System.Windows.Forms.Label labelDataSens4;
      private System.Windows.Forms.TabPage tabSetup;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.PictureBox pictureBox6;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.ListBox listBoxSetFiles;
      private System.Windows.Forms.Button buttonSetSave;
      private System.Windows.Forms.Button buttonSetLoad;
      private System.Windows.Forms.RadioButton radioButtonRoute4GEN0;
      private System.Windows.Forms.RadioButton radioButtonRoute3GEN0;
      private System.Windows.Forms.RadioButton radioButtonRoute2GEN0;
      private System.Windows.Forms.RadioButton radioButtonRoute1GEN0;
      private System.Windows.Forms.RadioButton radioButtonRoute4GEN3;
      private System.Windows.Forms.RadioButton radioButtonRoute4GEN2;
      private System.Windows.Forms.RadioButton radioButtonRoute4GEN1;
      private System.Windows.Forms.RadioButton radioButtonRoute3GEN3;
      private System.Windows.Forms.RadioButton radioButtonRoute3GEN2;
      private System.Windows.Forms.RadioButton radioButtonRoute3GEN1;
      private System.Windows.Forms.RadioButton radioButtonRoute2GEN3;
      private System.Windows.Forms.RadioButton radioButtonRoute2GEN2;
      private System.Windows.Forms.RadioButton radioButtonRoute2GEN1;
      private System.Windows.Forms.RadioButton radioButtonRoute1GEN3;
      private System.Windows.Forms.RadioButton radioButtonRoute1GEN2;
      private System.Windows.Forms.RadioButton radioButtonRoute1GEN1;
      private System.Windows.Forms.TabPage tabDevInfo;
      private System.Windows.Forms.Button buttonDevUpdate;
      private System.Windows.Forms.Label labelDevSens1;
      private System.Windows.Forms.Label labelDevSens3;
      private System.Windows.Forms.Label labelDevSens2;
      private System.Windows.Forms.Label labelDevSens4;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.PictureBox pictureBox7;
      private System.Windows.Forms.Button buttonDevReset;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.GroupBox groupBox10;
      private System.Windows.Forms.Label labelDeviceVersion;
   }
}

