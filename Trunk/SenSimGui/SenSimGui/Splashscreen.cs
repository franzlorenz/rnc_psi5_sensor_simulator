﻿/******************************************************************************
 * Project        SENSOR SIMULATION GUI
 * (c) copyright  2017
 * Company        Harman/Becker Automotive Systems GmbH
 *                All rights reserved
 * Secrecy Level  STRICTLY CONFIDENTIAL
 *****************************************************************************/
/**
 * @file          Splashscreen.cs
 * @ingroup       test
 * @author        Franz Lorenz
 *
 * This class handles the splashscreen.
 */

/*----------------------------------------------------------
 *  INCLUDES
 *--------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/*----------------------------------------------------------
 *  CLASSES
 *--------------------------------------------------------*/
namespace SenSimGui
{
   public partial class Splashscreen : Form
   {
      public Splashscreen()
      {
         InitializeComponent();
      }

      /**
       * This function is called, if the timer
       * expires.
       */
      private void timerSplashscreen_Tick(object sender, EventArgs e)
      {
         Close();
      }
   }
}
